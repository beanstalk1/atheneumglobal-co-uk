/*===================================
          Number Count Up
=====================================*/
if ($(".count-number").length > 0) {
  $(".count-number").rCounter({
    duration: 20
  });
}

/*===================================
          Search Input Popup
=====================================*/
function sidebarSearch() {
  var searchTrigger = $(".search-active"),
    endTriggersearch = $(".search-close"),
    container = $(".main-search-active");

  searchTrigger.on("click", function(e) {
    e.preventDefault();
    container.toggleClass("search-visible");
  });

  endTriggersearch.on("click", function() {
    container.removeClass("search-visible");
  });
}
sidebarSearch();

/*===================================
            ScrollTop
=====================================*/

$("#scroll-top").click(function() {
  $("html").animate(
    {
      scrollTop: 0
    },
    "slow"
  );
});

/*===================================
            ScrollBottom
=====================================*/
$(".scroll-next").click(function() {
  $("html, body").animate(
    {
      scrollTop: $(window).height()
    },
    "linear"
  );
});

// Scroll then fixed
$(window).scroll(function() {
  if ($(window).scrollTop() > 800) {
    $("#scroll-top").addClass("active");
  } else {
    $("#scroll-top").removeClass("active");
  }
});

// Mobile Bottom popup toggler
if ($(window).width() < 769) {
  var scrollPos = 0;

  window.addEventListener("scroll", function() {
    if (document.body.getBoundingClientRect().top > scrollPos) {
      $(".side-form-icons").removeClass("show-up-form-icons");
    } else {
      $(".side-form-icons").addClass("show-up-form-icons");
    }
    scrollPos = document.body.getBoundingClientRect().top;
  });
}

// Show popup on Btn Click;

if ($("#popup-toggle").length > 0) {
  $("#popup-toggle").click(function() {
    $(".popover-right").toggleClass("show");
  });
}

// Disable Input
if ($(".toggle-switch").length > 0) {
  var ts = $("#job_type");

  if (ts[0].disabled) {
    alert("disabled");
  }
}

// Initialize Nice Select
if ($(".select-box").length > 0) {
  $(".select-box").niceSelect();
}
$(document).ready(function() {});

// Preloader
$(window).on("load", function() {
  var preLoder = $("#preloader");
  preLoder.fadeOut(1000);

  $(document).on("click", ".cancel-preloader a", function(e) {
    e.preventDefault();
    $("#preloader").fadeOut(2000);
  });
});
//accreditations crousal

$('.accred').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
})


//news feeds
const mainContainer = document.getElementById("myData");
const RSS_URL = `https://www.ei-ie.org/en/latestnews/rss`;
const proxyurl = "https://cors-anywhere.herokuapp.com/";
fetch(proxyurl + RSS_URL)
  .then(response => response.text())
  .then(str => new window.DOMParser().parseFromString(str, "text/xml"))
  .then(data => {

  console.log(data);
  const items = data.querySelectorAll("item");
  let html = ``;
  items.forEach(el => {
    html += `
         <article>
           <h2>
             <a href="${el.querySelector("link").innerHTML}" target="_blank" rel="noopener">
               ${el.querySelector("title").innerHTML}
             </a>
           </h2>
           <p>
        ${el.querySelector("description").innerHTML}
           </p>
         </article>
       `;
  });
  mainContainer.insertAdjacentHTML("afterend", html);
  })
