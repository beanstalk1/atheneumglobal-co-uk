<title>Contact for online admission in UK</title>
<meta name="keywords" content="Online Teacher Training Course">
<meta name="description" content="Contact UK's best teacher education provider. Become anaccredited teacher to pursue a global teaching career. Contact us now.">
<?php include("_menu.php");?>
<link rel="canonical" href="<?php echo $url; ?>" async/>
<style>
 .breadcrub-style-3 .bg-img{
   background-image: url('/assets/img/study/partnership.jpeg');
 }
 h6{
   color:#000;
   padding: 20px 0px 0px
 }
 strong{
   color:#000;
 }
 .awardList{
   position: relative;
 }
 .awardList li {
    position: relative;
    padding-left: 32px;
}
.awardList .check-arrow::before {
    position: absolute;
    left: 0;
    top: 20px;
}
.degree span {
    font-size: 40px;
    color: #fff;
}
.degree .btn-primary {
    background-color: #811b18 !important;
    border: transparent;
}
.degree {
    width: 100%;
    background: url(./assets/img/study/level3_final.jpeg);
    background-size: cover;
    background-position: bottom center;
    position: relative;
    background-attachment: fixed;
    padding: 60px 0;
}
.degree strong{
  color:#fff;
}
 </style>
<!-- Breadcrumb -->
  <div class="breadcrub breadcrub-style-3 section allcourse-title">
    <div class="bg-img d-flex flex-column justify-content-center">
<div class="overlay"></div>
      <div class="container">
        <div class="heading">
          <h1 class="page-heading">Accreditations & Partnerships</h1>
        </div>
      </div>
      <!-- <div class="overlay"></div> -->
    </div>
  </div>
