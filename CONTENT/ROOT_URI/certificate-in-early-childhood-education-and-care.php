<title>Certificate in Early Childhood Care and Education in UK</title>
<meta name="keywords" content="Certificate in Early Childhood Care and Education, certificate in ECCE">
<meta name="description" content="We offer UK level 5 qualification for ECCE to shape the teaching careers of global educators.">
<?php include("_menu.php");?>
<link rel="canonical" href="<?php echo $url; ?>" async/>
<style>
/* .allcourse-title .page-heading {
  font-size: 30px !important;
  padding-bottom: 14px;
} */
.page-heading~p {
    font-size: 16px;
}
p,li{
  font-size: 15px !important;
}
h4{
  color:#777;
}
.breadcrub-style-3 .bg-img{
  background-image: url('/assets/img/study/early_certificate.jpg');
}
</style>
<!-- Breadcrumb -->
  <div class="breadcrub breadcrub-style-3 section allcourse-title">
    <div class="bg-img d-flex flex-column justify-content-center">
<div class="overlay"></div>
      <div class="container">
        <div class="heading">
          <h1 class="page-heading">Certificate in Early Childhood Care and Education</h1>
          <p>"Your attitude, not your aptitude, will determine your altitude."<br>
                                                               -<strong>Zig Ziglar</strong></p>
        </div>
      </div>
      <!-- <div class="overlay"></div> -->
    </div>
  </div>

<!-- Course Detail -->
<div class="course-detail section" id="single-coursepage">
    <div class="container">
        <div class="row">
            	<div class="col-12 col-lg-9 content">
                <h2 class="h4 pb-2">Online ECCE Certificate Course</h2>
               <p>Atheneum Global Teacher Training College’s Certificate in Early Childhood Care and Education Teacher Training Program is an abridged course with a focus on teaching and learning associated with early childhood. The course has been designed to acquaint the aspiring early childhood educators with the effective teaching methods using a student-centered approach. Today’s classrooms are more about encouraging movements and stimulating discussions and early childhood teachers are expected to understand the individual needs of the learners.</p>
					<div class="divider"></div>
					<h3 class="text-center pt-1 pb-2">ECCE Certificate Course Details</h3>
					<div class="tabs tabs-vertical" id="courses-tab">
					    <div class="row">
					        <div class="col-md-4">
					            <ul class="nav flex-column nav-tabs" id="course-faq" role="tablist" aria-orientation="vertical">
					                <li class="nav-item">
					                    <a class="nav-link active" href="#courseduartion" data-toggle="tab">COURSE DURATION</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#eligibility" data-toggle="tab">ELIGIBILITY</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#coursefee" data-toggle="tab">COURSE FEE</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#curriculum" data-toggle="tab">CURRICULUM</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#diploma" data-toggle="tab">DIPLOMA</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#enrollment" data-toggle="tab">ENROLLMENT PROCESS</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#placements" data-toggle="tab">PLACEMENTS</a>
					                </li>
					            </ul>
					        </div>
					        <div class="col-md-8">
					            <div class="tab-content">
					                <div class="tab-pane active" id="courseduartion">
					                    <ul class="list">
					                        <li class="main-li check-arrow">4 months in total.</li>
					                        <li class="main-li check-arrow">Flexibility to access from all around the space.</li>
					                        <li class="main-li check-arrow">Self paced.</li>
					                        <li class="main-li check-arrow">Permits early completion.</li>
					                    </ul>
                              <p>The duration of the average course will vary 4-6 months depending on how many hours you spend each week. However, the time spent is well-recognized as it gives teachers the requisite information to continue with trust in their career in this field.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="eligibility">
					                    <ul class="list">
					                        <li class="main-li check-arrow">Possession of a high school degree is sufficient.</li>
					                        <li class="main-li check-arrow">Existing teachers are free to apply.</li>
					                    </ul>
                              <p>A high school degree from any recognized institution is the minimum eligibility criteria for our Early Childhood Care and Education Certificate program. Following admission, documents must be submitted.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="coursefee">
					                    <ul class="list">
                                <li class="main-li check-arrow">We are available only available online.</li>
                                <li class="main-li check-arrow">Installment options with affordable fees.</li>
                                <li class="main-li check-arrow">Scholarship for excellent students.</li>
                              </ul>
                              <p>Our Early Childhood Care and Education Certificate is planned to be open to anyone. We work for the comfort of the applicant online.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="curriculum">
					                    <p>Early Childhood care and Education works for young minds up to eight years. The Ministry of Women and Child Development of India is engraving the guidelines. It worked for the children's overall growth. A keen view of the syllabus is provided herewith,</p>
					                    <ul class="list">
					                        <li class="main-li check-arrow">Objectives and Principles of ECCE.</li>
					                        <li class="main-li check-arrow">Early Years Development.</li>
					                        <li class="main-li check-arrow">Child Development.</li>
					                        <li class="main-li check-arrow">Developmentally Appropriate Practices</li>
					                        <li class="main-li check-arrow">Early Learning Environment.</li>
					                    </ul>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="diploma">
					                    <p>Some very prestigious international affiliations are affiliated with Atheneum World Teacher Training Certificate.
After completion of the course, the candidates will receive the ECCE Certificate.</p>
                              <p>Indian citizens receive a free certificate shipment, but international applicants will pay an extra <strong>$35</strong> for certificate shipping. We send a courier to meet the candidates provided address for a minimum of 3 weeks.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="enrollment">
					                    <ul class="list">
					                        <li class="main-li check-arrow">Simplest enrollment process.</li>
					                        <li class="main-li check-arrow">Online application and payment.</li>
					                        <li class="main-li check-arrow">Self paced.</li>
					                    </ul>
					                    <p>Regular discussion session with experienced faculty intervention.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="placements">
					                    <p>
					                       Atheneum Global Teacher Training College is an online pioneering platform for dream work. Each candidate receives a customized internship letter to help complete the course.
					                    </p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					            </div>
					        </div>
					    </div>
					</div>
            	</div>

            	<div class="col-12 col-lg-3 hide-md-and-down" id="single-course-sidebar">
                    <h5 class="pt-4">Related Courses</h5>
                    <ul class="list">
                        <li class="sub-li">
                        	<div class="post-image">
                                <div class="d-block img-thumbnail img-thumbnail-no-borders">
                                    <img src="/assets/img/course/1.jpg" width="50" height="50" alt="">
                                </div>
                            </div>
                            <div class="post-info">
                                <a href="/graduate-in-early-childhood-education-and-care">Graduate in Early Childhood Care and Education</a>
                            </div>
                        </li>
                        <li class="sub-li">
                        	<div class="post-image">
                                <div class="d-block img-thumbnail img-thumbnail-no-borders">
                                    <img src="/assets/img/course/3.jpg" width="50" height="50" alt="">
                                </div>
                            </div>
                            <div class="post-info">
                                <a href="/global-qualification-in-early-childhood-education-and-care">Post Graduate in Early Childhood Care and Education</a>
                            </div>
                        </li>
                    </ul>
                    <?php include("_common-sidebar.php");?>
                </div>
        </div>
    </div>
</div>
