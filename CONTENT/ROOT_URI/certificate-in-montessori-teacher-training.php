<title>Atheneum Global Teacher Training College UK</title>
<meta name="description" content="">
<?php include("_menu.php");?>
<link rel="canonical" href="<?php echo $url; ?>" async/>
<style>
/* .allcourse-title .page-heading {
  font-size: 30px !important;
  padding-bottom: 14px;
} */
.page-heading~p {
    font-size: 16px;
}
p,li{
  font-size: 15px !important;
}
h4{
  color:#777;
}
.breadcrub-style-3 .bg-img{
  background-image: url('/assets/img/study/mtt-certificate.jpg');
}

</style>
<!-- Breadcrumb -->
  <div class="breadcrub breadcrub-style-3 section allcourse-title">
    <div class="bg-img d-flex flex-column justify-content-center">
<div class="overlay"></div>
      <div class="container">
        <div class="heading">
          <h1 class="page-heading">Certificate in Montessori teacher training course</h1>
          <p>"From the moment the child enters the classroom, every step of his or her education
is seen as a progressive building block, ultimately forming the whole person from
childhood to adulthood. It&#39;s all about the needs of the child.”</p>
        </div>
      </div>
      <!-- <div class="overlay"></div> -->
    </div>
  </div>

<!-- Course Detail -->
<div class="course-detail section" id="single-coursepage">
    <div class="container">
        <div class="row">
            	<div class="col-12 col-lg-9 content">
                <h2 class="h4 pb-2">Certificate in Montessori teacher training course</h2>
               <p>The Montessori Teacher Training Certificate Program educates students about the core ideas of <strong>Dr. Maria Montessori</strong> and equips students to become effective Montessori teachers looking to start a career as Montessori teachers. The Montessori Education Certification program trains educators in an instructional environment using the globally accepted Montessori system.</p>
               <p>The duration of the average course will vary 4-6 months depending on how many hours you spend each week. However, the time spent is well-recognized as it gives teachers the requisite information to continue with trust in their career in this field.</p>
					<div class="divider"></div>
					<h3 class="text-center pt-1 pb-2">Montessori Certificate Course Details</h3>
					<div class="tabs tabs-vertical" id="courses-tab">
					    <div class="row">
					        <div class="col-md-4">
					            <ul class="nav flex-column nav-tabs" id="course-faq" role="tablist" aria-orientation="vertical">
					                <li class="nav-item">
					                    <a class="nav-link active" href="#courseduartion" data-toggle="tab">COURSE DURATION</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#eligibility" data-toggle="tab">ELIGIBILITY</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#coursefee" data-toggle="tab">COURSE FEE</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#curriculum" data-toggle="tab">CURRICULUM</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#diploma" data-toggle="tab">DIPLOMA</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#enrollment" data-toggle="tab">ENROLLMENT PROCESS</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#academicsupport" data-toggle="tab">ACADEMIC SUPPORT</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#placements" data-toggle="tab">PLACEMENTS</a>
					                </li>
					            </ul>
					        </div>
					        <div class="col-md-8">
					            <div class="tab-content">
					                <div class="tab-pane active" id="courseduartion">
					                    <ul class="list">
					                        <li class="">The Maximum duration of the course is four months.</li>
					                        <li>Can be accessed from any corner of the Earth because of its flexibility.</li>
					                        <li>Self-directed.</li>
					                        <li>Permits early completion.</li>
					                    </ul>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="eligibility">
					                    <ul>
					                        <li>High School Degree from recognized Institution.</li>
					                        <li>Existing teachers can apply.</li>
                                  <li>Minimum eligibility criteria for the Certificate in Montessori Education is a high school degree from a recognized Institute. After enrollment into the course, the submission of documents is mandatory for our records.</li>
					                    </ul>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="coursefee">
					                    <p>
                                <!-- The Certificate in Montessori Education is priced at Rs 9,500 for Indian Citizens and $150 International  Candidates. This is a complete online course hence very  flexible . Graduating from  Atheneum  Global Teachers Training College will open doors for a global career in teaching that too in very minimum investment. -->
                                The Certificate in Montessori Education is priced at $150. This is a complete online course hence very  flexible . Graduating from  Atheneum  Global Teachers Training College will open doors for a global career in teaching that too in very minimum investment.
					                    </p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="curriculum">
					                    <p>For those who want to work with or be in responsibility for the Montessori House of children in the pre-primary education space, we offer a concise certificate program in Montessori Teacher Training . The course contains core concepts from Montessori. The certificate has been awarded for the course to be successfully completed.</p>

					                    <h4 class="pt-1 pb-1">Theory: 4 modules</h4>
					                    <p>The break up of these modules is as below &ndash;</p>
					                    <ul class="list">
					                        <li class="check-arrow">Theory</li>
                                  <li class="check-arrow">Life History Of Maria Montessori</li>
					                        <li class="check-arrow">Life History Of Maria Montessori</li>
					                        <li class="check-arrow">Montessori Method And History</li>
					                        <li class="check-arrow">Exercises of Practical Life</li>
					                        <li class="check-arrow">Sensory Education in Montessori</li>
					                    </ul>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="diploma">
					                    <p>The candidates receive a certificate from the Atheneum Global Teacher Training College after completing the Post-graduates course. The awarded certificate has the additional benefit of not using the term 'online' in the certificates. Candidates resident abroad must pay an extra <strong>$25 (South East Asian & Middle Eastern countries) and $35 (the rest of the world)</strong> as a certificate distribution fee in Indian cards. The certificates are free of charge.
You have to pay the delivery fee usually after you have submitted all your tasks and completed the course. After that we will take the certificate to your shipping address , contact number and mail. The candidate address will take a maximum of three weeks for certificates to arrive.
					                    </p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="enrollment">
					                    <ul>
					                        <li>Simple and easy enrollment process.</li>
					                        <li>Application and payment online.</li>
					                        <li>Go through our FAQs and Blogs and you would be ready to enroll yourself into the course.</li>
					                        <li>You can also give us a call to help you with the onboarding process or to help clarify some aspects of the course.</li>
					                    </ul>
					                    <p>Once you&rsquo;ve made the online payment, you have to wait between 24-48 hours before we can enable your student account and get you started on your journey to academic success.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="academicsupport">
					                    <ul>
					                        <li>Personalized tutor support.</li>
					                        <li>Tons of reference material and videos</li>
					                        <li>Self Paced</li>
					                    </ul>
					                    <p>
					                       Our professional instructors at Atheneum Global Teacher Training College will directly mentor trainees who participate in the program. As part of this personalized tutoring, regular question clarification sessions and other academic support are extended. Learning takes place at your own speed and the service is completely tailored and has a high degree of expertise.
					                    </p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="placements">
					                    <p>
					                        Atheneum Global Teacher Training College trains global professionals to teach in an early childhood worldwide. Your application is credible with our industry connections and international credentials. An individual letter to help him/her complete his or her internship of your choice is issued to each candidate. This internship is an important step to complete your course and to begin your journey as a teacher.
					                    </p>
					                    <p>
					                        For teachers, a nationwide platform for teachers for all kinds of teaching assignments, the candidates are also privilegedly accessible. In essence, Atheneum is committed to providing you with the highest level of education and the best possible access to teaching opportunities.
					                    </p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					            </div>
					        </div>
					    </div>
					</div>
            	</div>

            	<div class="col-12 col-lg-3 hide-md-and-down" id="single-course-sidebar">
                    <h5 class="pt-4">Related Courses</h5>
                    <ul class="list">
                        <li class="sub-li">
                        	<div class="post-image">
                                <div class="d-block img-thumbnail img-thumbnail-no-borders">
                                    <img src="/assets/img/course/1.jpg" width="50" height="50" alt="">
                                </div>
                            </div>
                            <div class="post-info">
                                <a href="/graduate-in-montessori-teacher-training">Graduate in Montessori Teacher Training</a>
                            </div>
                        </li>
                        <li class="sub-li">
                        	<div class="post-image">
                                <div class="d-block img-thumbnail img-thumbnail-no-borders">
                                    <img src="/assets/img/course/3.jpg" width="50" height="50" alt="">
                                </div>
                            </div>
                            <div class="post-info">
                                <a href="/global-qualification-in-montessori-teacher-training">Post Graduate in Montessori Teacher Training</a>
                            </div>
                        </li>
                    </ul>
                    <?php include("_common-sidebar.php");?>
                </div>
        </div>
    </div>
</div>
