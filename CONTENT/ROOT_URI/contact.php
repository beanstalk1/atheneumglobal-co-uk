<title>Contact for online admission in UK</title>
<meta name="keywords" content="Online Teacher Training Course">
<meta name="description" content="Contact UK's best teacher education provider. Become anaccredited teacher to pursue a global teaching career. Contact us now.">
<?php include("_menu.php");?>
<link rel="canonical" href="<?php echo $url; ?>" async/>
<style>
h1.page-heading {
  color: var(--dark-grey);
}
h1.page-heading ~ p {
    color: var(--dark-grey) !important;
    padding-top: 12px;
}
</style>
<!-- Contact Breadcrub -->
<div class="contact-breadcrumb section">
    <div class="row">
        <div class="col-12 col-lg-7 content">
            <div class="inner-content">
                <h1 class="page-heading">Contact us</h1>
                <p class="d-none d-xl-block">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In rhoncus augue nibh, at ullamcorper orci ullamcorper ut. Integer vehicula iaculis risus, non consequat eros tincidunt ac.</p>
            </div>
        </div>
        <div class="d-none d-lg-block col-5 map">
            <!-- Map img goes here -->
            <!-- <div class="zoom-in-out">
                <div class="zoom-in">
                    <i class="fas fa-plus"></i>
                </div>
                <div class="zoom-out">
                    <i class="fas fa-minus"></i>
                </div>
            </div> -->
        </div>
    </div>
</div>

<!-- Contact Details -->
<section class="contact-details-section section">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="contact-info">
                    <div class="openings">
                        <h2 class="margin-bottom-30">Opening hours</h2>
                        <div class="opening opening-days">
                            <p class="day">Monday</p>
                            <div class="divider"></div>
                            <p class="day">Saturday</p>
                        </div>
                        <div class="opening opening-time">
                            <p class="time">09.00 am</p>
                            <div class="divider"></div>
                            <p class="time">17.00 pm</p>
                        </div>
                    </div>

                    <div class="contact-links">
                        <p class="mail"><a href="mailto:enquiry@atheneumglobal.co.uk">enquiry@atheneumglobal.co.uk</a></p>
                        <p class="phone"><a href="tel:+442038077020">+442038077020</a></p>
                        <div class="social-icons">
                            <a target="_blank" href="https://www.facebook.com/AtheneumGlobalTeacherTrainingCollege/"><i class="fab fa-facebook-square"></i></a>
                            <a target="_blank" href="https://www.instagram.com/atheneum.global/"><i class="fab fa-instagram"></i></a>
                            <a target="_blank" href="https://twitter.com/atheneum_global"><i class="fab fa-twitter"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 col-xl-5 message-from">
                <h2>Leave us a message</h2>

                <form class="text-left form-style-2">
                    <div class="form-group required">
                        <label for="username">Username</label>
                        <input type="text" id="username" class="form-control" name="username" placeholder="Enter Full Name" />
                    </div>
                    <div class="form-group required">
                        <label for="email">Email address</label>
                        <input type="email" id="email" class="form-control" name="email" placeholder="Put an valid Email ID" />
                    </div>
                    <div class="form-group required">
                        <label for="message">Message</label>
                        <input type="message" id="message" class="form-control" name="message" placeholder="I wanted to enquire about…" />
                        <!-- <span class="small-text">120 characters left</span> -->
                    </div>
                </form>
                <div class="main-btn">Send message</div>
            </div>
        </div>
    </div>
</section>
