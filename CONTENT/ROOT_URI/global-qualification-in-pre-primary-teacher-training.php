<title>Level5 Global Qualificationin Pre-primary Teacher Training</title>
<meta name="keywords" content="Pre and Primary teacher training course">
<meta name="description" content="Our international Level 5 Qualification in Pre- and Primary School Teacher Education trains aspiring teachers for a fantastic start to a teaching career.">
<?php include("_menu.php");?>
<link rel="canonical" href="<?php echo $url; ?>" async/>
<style>
/* .allcourse-title .page-heading {
  font-size: 30px !important;
  padding-bottom: 14px;
} */
.page-heading~p {
    font-size: 16px;
}
p,li{
  font-size: 15px !important;
}
h4{
  color:#777;
}
.breadcrub-style-3 .bg-img{
  background-image: url('/assets/img/study/post_pre.jpg');
}
#more {display: none;}
</style>
<!-- Breadcrumb -->
  <div class="breadcrub breadcrub-style-3 section allcourse-title">
    <div class="bg-img d-flex flex-column justify-content-center">
<div class="overlay"></div>
      <div class="container">
        <div class="heading">
          <h1 class="page-heading">Global Qualification in Pre and Primary Teacher Training</h1>
          <p>Logic will get you from A to Z:'imagination' will get you everywhere.
                                       <br>-<strong>Albert Einstein</strong></p>
        </div>
      </div>
      <!-- <div class="overlay"></div> -->
    </div>
  </div>

<!-- Course Detail -->
<div class="course-detail section" id="single-coursepage">
    <div class="container">
        <div class="row">
            	<div class="col-12 col-lg-9 content">
                <h2 class="h4 pb-2">Global Qualification in Pre and Primary Teacher Training</h2>
               <p>If you're planning a career as a pre-and primary school teacher, the Level 5 Global Qualification for Pre-and Primary Teacher Training (PPTT) is the most appropriate and immersive course available to students today. The course, which addresses the core developmental frameworks identified in early childhood, best equips course participants to become versatile practitioners in the schools and preschools of their choice.<span id="dots">...</span><span id="more"><br><br>
               Early childhood is a period from conception to age 8, a period that presents a developmental continuum in line with the theoretical framework of developmental psychology and learning theories. The reason for extending the period of early childhood from 6 to 8 years is to ensure a gradual and smooth transition from pre-primary to primary education, which is a structured and formal learning system requiring an effective interface. It is in this context that the Global Level 5 Qualification of Pre-and Primary Teacher Training is relevant, as it takes advantage of the structural framework established for early years and builds on it to further enhance the scope of teacher education  the post formative early childhood stage.<br></br>
               The course is perfect for those students who seek a career in the teaching profession while not compromising on existing career or life choices that require them to pursue a Level 5 Global Qualification in the field of their choice.</p>
               <a class="small-btn" onclick="myFunction()" id="myBtn" style="color:#fff; margin:2rem 0 !important;">Read More</a>
          <div class="divider"></div>
					<h3 class="text-center pt-1 pb-2">Pre-Primary Post Graduate Course Details</h3>
					<div class="tabs tabs-vertical" id="courses-tab">
					    <div class="row">
					        <div class="col-md-4">
					            <ul class="nav flex-column nav-tabs" id="course-faq" role="tablist" aria-orientation="vertical">
					                <li class="nav-item">
					                    <a class="nav-link active" href="#courseduartion" data-toggle="tab">COURSE DURATION</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#eligibility" data-toggle="tab">ELIGIBILITY</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#coursefee" data-toggle="tab">COURSE FEE</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#curriculum" data-toggle="tab">CURRICULUM</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#diploma" data-toggle="tab">DIPLOMA</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#enrollment" data-toggle="tab">ENROLLMENT PROCESS</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#academicsupport" data-toggle="tab">ACADEMIC SUPPORT</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#placements" data-toggle="tab">PLACEMENTS</a>
					                </li>
					            </ul>
					        </div>
					        <div class="col-md-8">
					            <div class="tab-content">
					                <div class="tab-pane active" id="courseduartion">
					                    <ul class="list">
					                        <li class="main-li check-arrow">15 months max.</li>
					                        <li class="main-li check-arrow">Can be accessed from any part of the sphere.</li>
                                  <li class="main-li check-arrow">Self-Paced.</li>
                                  <li class="main-li check-arrow">Permits Early Completion.</li>
					                    </ul>
                              <P>The Post Graduate Diploma in Pre and Primary Teacher Training prepares the candidates with the latest set of curriculum standards and technologies needed and practiced used in Pre-Primary Schools.</P>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="eligibility">
					                    <ul>
					                        <li>A Bachelors Degree is the minimum requirement.</li>
					                        <li>Existing and eager to become teachers can apply.</li>
					                    </ul>
                              <P>The minimum eligibility for the Post Graduate Diploma in Pre-Primary Education is a bachelor's degree from a recognized institute. After, enrollment the submission of documents for our records is required.</P>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="coursefee">
					                    <p>
<!-- The Post Graduate Diploma in Pre-Primary Education is priced very affordably at Rs 25,500 for Indian citizens and $400 for International Candidates. The courses are 100% online and do not interrupt anyone's present profession. Therefore Atheneum Global Teacher Training is offering a world-class degree with minimum investment.</p> -->
The Post Graduate Diploma in Pre-Primary Education is priced very affordably at $400. The courses are 100% online and do not interrupt anyone's present profession. Therefore Atheneum Global Teacher Training is offering a world-class degree with minimum investment.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="curriculum">
                              <P>We have prepared a very authentic Post Graduation Program in Pre-Primary Teachers Training and the norms which we follow are based on the National ECCE Curriculum Framework set by the Ministry of Women and Child Development in concurrence with the HRD Ministry. This course incorporates advanced teacher training concepts in addition to the ones covered in the Graduation Program.</P>
                              <P>After successful completion of the course, the Diploma is awarded</P>
                              <P>The assessment is done through various methods. It includes files on the practice material, making of language and cultural teaching materials..</P>
					                    <P>Keeping the guidelines in mind the course has:-</P>
					                    <ul class="list">
					                        <li class="main-li check-arrow">Objectives and principles of PPTT</li>
                                  <li class="main-li check-arrow">Pre-Primary administration</li>
                                  <li class="main-li check-arrow">Child Development</li>
                                  <li class="main-li check-arrow">Developmentally Appropriate Practices in Pre-Primary Education</li>
                                  <li class="main-li check-arrow">Guidance and Discipline.</li>
                                  <li class="main-li check-arrow">Early Learning Theories.</li>
                                  <li class="main-li check-arrow">Classroom Management and Areas of Learning in Early Years.</li>
                                  <li class="main-li check-arrow">Planning in Early Years.</li>
                                  <li class="main-li check-arrow">Play and it's benefits.</li>
                                  <li class="main-li check-arrow">Evaluation in Early Years.</li>
                                  <li class="main-li check-arrow">Child Psychology.</li>
                                  <li class="main-li check-arrow">Curriculum.</li>
                                  <li class="main-li check-arrow">Integration of Children with Special Needs.</li>
                                  <li class="main-li check-arrow">Safety, Health, and Nutrition in Early Years.</li>
                                  <li class="main-li check-arrow">Technology and Media.</li>
                                  <li class="main-li check-arrow">EVS</li>
					                    </ul>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="diploma">
					                    <p>
                                The Diploma is awarded after the successful completion of the course. Online has not mentioned anywhere it adds an advantage for the candidates. Indian Certificates are shipped free of cost but International Candidates need to pay standard fee of <strong>US $35</strong> as dispatch fee for certificates. After all the curriculum submission and examination the dispatch fee is asked for. Then we ask for the shipping address and contact number for the courier. It takes a maximum of 3 weeks for the certificates to reach the destination.
					                    </p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="enrollment">
					                    <ul>
					                        <li>Easy enrollment process.</li>
					                        <li>Online payment and application.</li>
					                    </ul>
					                    <p>All the courses are clearly defined on our website which gives you a better understanding of Atheneum Global Teacher Training College and their work. Umpteen number of FAQs and Blogs are provided.</p>
                              <p>Feel free to ring regarding any queries or during On boarding Process.</p>
                              <p>Once the payment has been done, please wait for 24-48 hours, for us to process and create your ID and student account. And start towards the path of excellence.</p>
                              <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="academicsupport">
					                    <ul>
					                        <li>Personalised guide.</li>
					                        <li>Dozens of references and videos.</li>
					                        <li>Self-paced.</li>
					                    </ul>
					                    <p>
                                Candidates who enroll with Atheneum Global Teacher Training College will be guided personally by our highly esteemed faculties.
					                    </p>
                              <p>
                                Daily doubt clearing sessions and all academic support are extended as a part of this course. The learning is at your pace but we are the support system for technicalities.
                              </p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="placements">
					                    <p>
                                Atheneum Global Teacher Training College educates global professionals to pursue a teaching profession anywhere in the sphere. Our esteemed linkages and International credential lay a strong foundation in the career of our students. Each candidate is issued a personalized letter to help land up with an internship of their choice. This internship letter will let you finish the course and start a new journey to become an Educator.
					                    </p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					            </div>
					        </div>
					    </div>
					</div>
            	</div>

            	<div class="col-12 col-lg-3 hide-md-and-down" id="single-course-sidebar">
                    <h5>Related Courses</h5>
                    <ul class="list">
                      <li class="sub-li">
                        <div class="post-image">
                              <div class="d-block img-thumbnail img-thumbnail-no-borders">
                                  <img src="/assets/img/course/3.jpg" width="50" height="50" alt="">
                              </div>
                          </div>
                          <div class="post-info">
                              <a href="/certificate-in-pre-primary-teacher-training">Certificate in Pre-Primary teacher training course</a>
                          </div>
                      </li>
                        <li class="sub-li">
                        	<div class="post-image">
                                <div class="d-block img-thumbnail img-thumbnail-no-borders">
                                    <img src="/assets/img/course/1.jpg" width="50" height="50" alt="">
                                </div>
                            </div>
                            <div class="post-info">
                                <a href="/graduate-in-pre-primary-teacher-training">Graduate in Pre-Primary Teacher Training</a>
                            </div>
                        </li>
                    </ul>
                    <?php include("_common-sidebar.php");?>
                </div>
        </div>
    </div>
</div>
<script>
function myFunction() {
  var dots = document.getElementById("dots");
  var moreText = document.getElementById("more");
  var btnText = document.getElementById("myBtn");

  if (dots.style.display === "none") {
    dots.style.display = "inline";
    btnText.innerHTML = "Read more";
    moreText.style.display = "none";
  } else {
    dots.style.display = "none";
    btnText.innerHTML = "Read less";
    moreText.style.display = "inline";
  }
}
</script>
