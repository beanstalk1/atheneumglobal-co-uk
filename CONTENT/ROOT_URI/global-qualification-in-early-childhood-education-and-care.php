<title> Level 5 Global Qualification in Early Childhood Care and Education</title>
<meta name="keywords" content="early childhood education courses">
<meta name="description" content="Level 5 Global Qualification in ECCE awarded by Ofqual regulated AO develops well-informed teachers qualified for a highly distinguished career in teaching.">
<?php include("_menu.php");?>
<link rel="canonical" href="<?php echo $url; ?>" async/>
<style>
/* .allcourse-title .page-heading {
  font-size: 30px !important;
  padding-bottom: 14px;
} */
.page-heading~p {
    font-size: 16px;
}
p,li{
  font-size: 15px !important;
}
h4{
  color:#777;
}
.breadcrub-style-3 .bg-img{
  background-image: url('/assets/img/study/early_post_certificate.jpg');
}
</style>
<!-- Breadcrumb -->
  <div class="breadcrub breadcrub-style-3 section allcourse-title">
    <div class="bg-img d-flex flex-column justify-content-center">
<div class="overlay"></div>
      <div class="container">
        <div class="heading">
          <h1 class="page-heading">Post Graduate Diploma in ECCE</h1>
          <p>"You are educating a man; a man you are educating. a woman; a generation you are educating."
<br>-<strong>Bringham Young.</strong></p>
        </div>
      </div>
      <!-- <div class="overlay"></div> -->
    </div>
  </div>

<!-- Course Detail -->
<div class="course-detail section" id="single-coursepage">
    <div class="container">
        <div class="row">
            	<div class="col-12 col-lg-9 content">
                <h2 class="h4 pb-2">Post Graduate Diploma in Early Childhood Education and Care Course</h2>
               <p>The Post Graduate Diploma in Early Childhood Care and Education Teacher Training Program is an advanced course designed for both aspiring and presently working early childhood educators who can gain a deeper understanding of teaching young learners aged 0-5 years. The course provides the newbie and experienced teachers with a broad range of knowledge coupled with teaching skills and methods to motivate children and generate interest in learning in their formative years.</p>
					<div class="divider"></div>
					<h3 class="text-center pt-1 pb-2">ECCE Post Graduate Course Details</h3>
					<div class="tabs tabs-vertical" id="courses-tab">
					    <div class="row">
					        <div class="col-md-4">
					            <ul class="nav flex-column nav-tabs" id="course-faq" role="tablist" aria-orientation="vertical">
					                <li class="nav-item">
					                    <a class="nav-link active" href="#courseduartion" data-toggle="tab">COURSE DURATION</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#eligibility" data-toggle="tab">ELIGIBILITY</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#coursefee" data-toggle="tab">COURSE FEE</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#curriculum" data-toggle="tab">CURRICULUM</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#diploma" data-toggle="tab">DIPLOMA</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#enrollment" data-toggle="tab">ENROLLMENT PROCESS</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#academicsupport" data-toggle="tab">ACADEMIC SUPPORT</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#placements" data-toggle="tab">PLACEMENTS</a>
					                </li>
					            </ul>
					        </div>
					        <div class="col-md-8">
					            <div class="tab-content">
					                <div class="tab-pane active" id="courseduartion">
					                    <ul class="list">
					                        <li class="main-li check-arrow">15 months max.</li>
					                        <li class="main-li check-arrow">Can be accessed from any part of the sphere.</li>
					                        <li class="main-li check-arrow">Permits Early Completion.</li>
					                    </ul>
                              <p>The Post Graduate Diploma in Early Childhood Care and Education prepares the candidates with the latest set of curriculum standards and technologies.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="eligibility">
					                    <ul class="list">
					                        <li class="main-li check-arrow">A Bachelors Degree is the minimum requirement.</li>
                                  <li class="main-li check-arrow">Existing and eager to become teachers can apply.</li>
					                    </ul>
                              <p>The minimum eligibility for the Post Graduate is a bachelor's degree from a recognized institute. After, enrollment the submission of documents for our records is required.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="coursefee">
                              <!-- <p>The Post Graduate Diploma in ECCE  is priced very affordably at Rs 25,500 for Indian citizens and $400 for International Candidates. The courses are 100% online and do not interrupt anyone's present profession. Therefore Atheneum Global Teacher Training is offering a world-class degree with minimum investment.</p> -->
                              <p>The Post Graduate Diploma in ECCE  is priced very affordably at $400. The courses are 100% online and do not interrupt anyone's present profession. Therefore Atheneum Global Teacher Training is offering a world-class degree with minimum investment.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="curriculum">
                              <p>We have prepared a very aesthetic Post Graduation Program in ECCE. It spans over for 15 months given the vast syllabus. This course incorporates advanced teacher training concepts in addition to the ones covered in the Graduation Program. The curriculum is based on the National ECCE curriculum set by the Ministry of Women and Child Development and HRD Ministry.</p>
					                    <p class="pt-1 pb-1"><strong>The Curriculum has three broad sections:-</strong></p>
					                    <ul class="list">
					                        <li class="main-li check-arrow">Introduction and Pedagogy principles of early childhood.</li>
					                        <li class="main-li check-arrow">Developmentally Appropriate Practices.</li>
                                  <li class="main-li check-arrow">Implementation of the program, role of parent and teacher.</li>
					                    </ul>
                              <p>After successful completion of the course, the Diploma is awarded.</p>
                              <p>The assessment is done through various methods. It includes files on the practice material, making of language and cultural teaching materials.</p>
                              <p><strong>Keeping the guidelines in mind the course has:-</strong></p>
                              <ul class="list">
					                        <li class="main-li check-arrow">Objectives and principles of ECCE</li>
					                        <li class="main-li check-arrow">Early Years Development</li>
                                  <li class="main-li check-arrow">Developmentally Appropriate Practices - Early Years.</li>
                                  <li class="main-li check-arrow">Transition from Early Years.</li>
                                  <li class="main-li check-arrow">Learning Theories.</li>
                                  <li class="main-li check-arrow">Early Learning Environment.</li>
                                  <li class="main-li check-arrow">Early Learning Planning.</li>
                                  <li class="main-li check-arrow">Importance of Play.</li>
                                  <li class="main-li check-arrow">Pedagogical Techniques and Strategies.</li>
                                  <li class="main-li check-arrow">Assessment In Early Years.</li>
                                  <li class="main-li check-arrow">Early Childhood Curriculum Development.</li>
                                  <li class="main-li check-arrow">Integration of Children with Special Needs.</li>
                                  <li class="main-li check-arrow">Sociology.</li>
                                  <li class="main-li check-arrow">Exposure to Technology in Early Childhood.</li>
                                  <li class="main-li check-arrow">Environmental Science(EVS).</li>
                                  <li class="main-li check-arrow">Basics of Lesson Planning.</li>
                                  <li class="main-li check-arrow">Literacy and Numeracy in Early Childhood.</li>
					                    </ul>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="diploma">
                              <p>The Diploma is awarded after the successful completion of the course. Online has not mentioned anywhere it adds an advantage for the candidates. Indian Certificates are shipped free of cost. After all the curriculum submission and examination the dispatch fee is asked for. Then we ask for the shipping address and contact number for the courier. It takes a maximum of 3 weeks for the certificates to reach the destination.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="enrollment">
					                    <ul class="list">
					                        <li class="main-li check-arrow">Easy enrollment process.</li>
					                        <li class="main-li check-arrow">Online payment and application.</li>
					                    </ul>
                              <p>All the courses are clearly defined on our website which gives you a better understanding of Atheneum Global Teacher Training College and their work. Umpteen number of FAQs and Blogs are provided.
Feel free to ring regarding any queries or during On boarding Process.</p>
                              <p>Once the payment has been done, please wait for 24-48 hours, for us to process and create your ID and student account. And start towards the path of excellence.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="academicsupport">
					                    <ul class="list">
					                        <li class="main-li check-arrow">Personalised guide.</li>
					                        <li class="main-li check-arrow">Dozens of references and videos.</li>
					                        <li class="main-li check-arrow">Self-paced.</li>
					                    </ul>
					                    <p>
                               Candidates who enroll with Atheneum Global Teacher Training College will be guided personally by our highly esteemed faculties.
                             </p>
                             <p>Daily doubt clearing sessions and all academic support are extended as a part of this course. The learning is at your pace but we are the support system for technicalities.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="placements">
					                    <p>Atheneum Global Teacher Training College educates global professionals to pursue a teaching profession anywhere in the sphere. Our esteemed linkages and International credential lay a strong foundation in the career of our students. Each candidate is issued a personalized letter to help land up with an internship of their choice. This internship letter will let you finish the course and start a new journey to become an Educator.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					            </div>
					        </div>
					    </div>
					</div>
            	</div>

            	<div class="col-12 col-lg-3 hide-md-and-down" id="single-course-sidebar">
                    <h5>Related Courses</h5>
                    <ul class="list">
                        <li class="sub-li">
                        	<div class="post-image">
                                <div class="d-block img-thumbnail img-thumbnail-no-borders">
                                    <img src="/assets/img/course/1.jpg" width="50" height="50" alt="">
                                </div>
                            </div>
                            <div class="post-info">
                                <a href="/certificate-in-early-childhood-education-and-care">Certificate in Early Childhood Education and Care Course</a>
                            </div>
                        </li>
                        <li class="sub-li">
                        	<div class="post-image">
                                <div class="d-block img-thumbnail img-thumbnail-no-borders">
                                    <img src="/assets/img/course/3.jpg" width="50" height="50" alt="">
                                </div>
                            </div>
                            <div class="post-info">
                                <a href="/graduate-in-early-childhood-education-and-care">Graduate in Early Childhood Education and Care Course</a>
                            </div>
                        </li>
                    </ul>
                    <?php include("_common-sidebar.php");?>
                </div>
        </div>
    </div>
</div>
