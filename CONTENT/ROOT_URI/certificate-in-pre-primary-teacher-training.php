<title>Certificate in pre-primary teacher training coursein UK</title>
<meta name="keywords" content="pre & primary teacher training course">
<meta name="description" content="Our Certificate Program in pre and primary teacher training course presents international learners a unique learning and qualification opportunity to be trained for a global teaching career.">
<?php include("_menu.php");?>
<link rel="canonical" href="<?php echo $url; ?>" async/>
<style>
/* .allcourse-title .page-heading {
  font-size: 30px !important;
  padding-bottom: 14px;
} */
.page-heading~p {
    font-size: 16px;
}
p,li{
  font-size: 15px !important;
}
h4{
  color:#777;
}
.breadcrub-style-3 .bg-img{
  background-image: url('/assets/img/study/certificate_pre.jpg');
}

</style>
<!-- Breadcrumb -->
  <div class="breadcrub breadcrub-style-3 section allcourse-title">
    <div class="bg-img d-flex flex-column justify-content-center">
<div class="overlay"></div>
      <div class="container">
        <div class="heading">
          <h1 class="page-heading">Certificate in Pre-Primary teacher training course</h1>
          <p>"A great teacher takes a hand, opens a mind, and touches a heart."</p>
        </div>
      </div>
      <!-- <div class="overlay"></div> -->
    </div>
  </div>

<!-- Course Detail -->
<div class="course-detail section" id="single-coursepage">
    <div class="container">
        <div class="row">
            	<div class="col-12 col-lg-9 content">
                <h2 class="h4 pb-2">Online Pre-Primary certificate course</h2>
               <p>The Certificate Program for Pre-and Primary Teacher Training (PPTT) is an appropriate course available to students seeking to meet the requirements of pre-and primary-teachers in the shortest possible timeframe. The National Curriculum Framework Guidelines provided by the Ministry of Women and Child Development have been incorporated into the program structure and equip course participants with the knowledge to be pre-and primary school teachers in any early childhood setting.</p>
               <p>The course is perfect for those students who seek to fulfill the role of educator in a pre-and primary setting with a quick course that gives them basic skills without compromising on existing career or life choices that require them to pursue certification in the field of their choice.</p>
					<div class="divider"></div>
					<h3 class="text-center pt-1 pb-2">Pre-Primary Certificate Course Details</h3>
					<div class="tabs tabs-vertical" id="courses-tab">
					    <div class="row">
					        <div class="col-md-4">
					            <ul class="nav flex-column nav-tabs" id="course-faq" role="tablist" aria-orientation="vertical">
					                <li class="nav-item">
					                    <a class="nav-link active" href="#courseduartion" data-toggle="tab">COURSE DURATION</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#eligibility" data-toggle="tab">ELIGIBILITY</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#coursefee" data-toggle="tab">COURSE FEE</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#curriculum" data-toggle="tab">CURRICULUM</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#diploma" data-toggle="tab">DIPLOMA</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#enrollment" data-toggle="tab">ENROLLMENT PROCESS</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#academicsupport" data-toggle="tab">ACADEMIC SUPPORT</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#placements" data-toggle="tab">PLACEMENTS</a>
					                </li>
					            </ul>
					        </div>
					        <div class="col-md-8">
					            <div class="tab-content">
					                <div class="tab-pane active" id="courseduartion">
					                    <ul class="list">
					                        <li class="main-li check-arrow">The Maximum duration of the course is four months.</li>
					                        <li class="main-li check-arrow">Can be accessed from any corner of the Earth because of its flexibility.</li>
					                        <li class="main-li check-arrow">Self-directed.</li>
					                        <li class="main-li check-arrow">Permits early completion.</li>
					                    </ul>
                              <p>The candidate can apply and join an education institution in the nation and abroad (internationally). After completing the certificate program of Pre-primary Teacher Training (PPTT) The course provides a comprehensive curriculum but can be completed in advance (2-3 months) if you have more time available.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="eligibility">
					                    <ul class="list">
					                        <li class="main-li check-arrow">High School Degree from recognized Institution.</li>
					                        <li class="main-li check-arrow">Existing teachers can apply.</li>
					                    </ul>
                              <p>The minimum eligibility for the Certificate Program in Pre and Primary Teacher Training is a high school degree from an authorized institution. After enrollment, the educational qualification needs to be submitted for our requirements.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="coursefee">
					                    <ul class="list">
                                <li class="main-li check-arrow">The Program will be conducted in online mode.</li>
                                <li class="main-li check-arrow">Reasonably priced with installment options.</li>
                                <li class="main-li check-arrow">Scholarships for deserving students.</li>
                              </ul>
                              <p>The Certificate Program in Pre-Primary Teacher Training is worth at $150.</p>
                              <p>The courses are online, so they will not pose any Hindrance in the existing format of life. With a Graduation from Atheneum Global Teacher Training College, we assure you a fruitful career.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="curriculum">
					                    <p>The Atheneum Pre-Primary Teacher Training Credential Program prepares aspirants to be ready for the industry. In collaboration with the Minister of Women and Child Development (HRD) the course offers a comprehensive curriculum strongly linked to the National Early Childhood Care and Education Circle Framework. The course includes a number of compulsory practices in the framework of national and international pre-primary education in India and across borders.</p>
                              <p>Keeping the guidelines in mind, the course covers the following topics that span in 9 months:</p>
					                    <h4 class="pt-1 pb-1">Theory: 4 modules</h4>
					                    <ul class="list">
					                        <li class="main-li check-arrow">Objectives and Principles of Pre-Primary Teacher Training.</li>
					                        <li class="main-li check-arrow">Pre-Primary Administration.</li>
					                        <li class="main-li check-arrow">Child Development.</li>
					                        <li class="main-li check-arrow">Developmentally Appropriate Practices in Pre-Primary Education.</li>
					                        <li class="main-li check-arrow">Guidance and Discipline.</li>
                                  <li class="main-li check-arrow">Early Learning Theories.</li>
                                  <li class="main-li check-arrow">Classroom Management and Areas of Learning in Early Years.</li>
                                  <li class="main-li check-arrow">Planning in Early Years.</li>
                                  <li class="main-li check-arrow">Play and its Benefits.</li>
                                  <li class="main-li check-arrow">Evaluation in Early Years.</li>
					                    </ul>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="diploma">
					                    <p>A Globally respected diploma from Atheneum Global Teacher Training College will be granted to the candidates upon successful completion of the course.</p>
                              <!-- <p>The diploma has an added advantage as the word 'Online' is nowhere mentioned.
Shipment of the certificate is free within the boundaries but candidates of Abroad(International)need to pay an additional <strong>US$25 (South East Asian & Middle Eastern Countries) and US$35(REST OF THE WORLD)</strong> as dispatch fee for the certificate. After submitting all the assignments and completing the course, we ask for the dispatch fee. Correct contact number and address is needed for the courier of the certificate. It takes a maximum of 3 weeks for the certificates to reach the address.</p> -->
<p>The diploma has an added advantage as the word 'Online' is nowhere mentioned.
Shipment of the certificate is free within the boundaries but candidates of Abroad(International)need to pay standard fee of <strong>US$35</strong> as dispatch fee for the certificate. After submitting all the assignments and completing the course, we ask for the dispatch fee. Correct contact number and address is needed for the courier of the certificate. It takes a maximum of 3 weeks for the certificates to reach the address.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="enrollment">
					                    <ul class="list">
					                        <li class="main-li check-arrow">Easy Enrollment Process.</li>
					                        <li class="main-li check-arrow">Online applications and payments.</li>
					                        <li class="main-li check-arrow">You can go through the Blogs and FAQs before enrolling.</li>
					                        <li class="main-li check-arrow">We are also there to help you through the onboarding process.</li>
					                    </ul>
					                    <p>After the payment done by you, please wait for 48 hours before we can enable you to log in and start the journey of success.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="academicsupport">
					                    <ul>
					                        <li>Personalized tutor support.</li>
					                        <li>Tons of reference material and videos</li>
					                        <li>Self Paced</li>
					                    </ul>
					                    <p>
                                Candidates enrolled with us will be guided personally with our experienced faculties. Regular doubt clearing sessions and academic support will be extended for the smooth going of the course. Learn according to your own pace and enjoy the success.
					                    </p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="placements">
					                    <p>
					                       We train our candidates to pursue the teaching profession worldwide. Our connections to the industry give your certificate credibility. We issue a custom letter helping you to complete an internship. Connection to our Career Site is also open to applicants. This is a unique platform for taking a step ahead of your career.
					                    </p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					            </div>
					        </div>
					    </div>
					</div>
            	</div>

            	<div class="col-12 col-lg-3 hide-md-and-down" id="single-course-sidebar">
                    <h5 class="pt-4">Related Courses</h5>
                    <ul class="list">
                        <li class="sub-li">
                        	<div class="post-image">
                                <div class="d-block img-thumbnail img-thumbnail-no-borders">
                                    <img src="/assets/img/course/1.jpg" width="50" height="50" alt="">
                                </div>
                            </div>
                            <div class="post-info">
                                <a href="/graduate-in-pre-primary-teacher-training">Graduate in Pre-Primary Teacher Training</a>
                            </div>
                        </li>
                        <li class="sub-li">
                        	<div class="post-image">
                                <div class="d-block img-thumbnail img-thumbnail-no-borders">
                                    <img src="/assets/img/course/3.jpg" width="50" height="50" alt="">
                                </div>
                            </div>
                            <div class="post-info">
                                <a href="/global-qualification-in-pre-primary-teacher-training">Post Graduate in Pre-Primary Teacher Training</a>
                            </div>
                        </li>
                    </ul>
                    <?php include("_common-sidebar.php");?>
                </div>
        </div>
    </div>
</div>
