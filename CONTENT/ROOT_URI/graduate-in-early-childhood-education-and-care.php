<title>Graduate Diploma in Early Childhood Care and Education in UK</title>
<meta name="keywords" content="early childhood education courses">
<meta name="description" content="Our ECCE diploma course is intended for aspiring teachers who are looking for a globally relevant qualification to explore overseas opportunities.">
<?php include("_menu.php");?>
<link rel="canonical" href="<?php echo $url; ?>" async/>
<style>
/* .allcourse-title .page-heading {
  font-size: 30px !important;
  padding-bottom: 14px;
} */
.page-heading~p {
    font-size: 16px;
}
p,li{
  font-size: 15px !important;
}
h4{
  color:#777;
}
.breadcrub-style-3 .bg-img{
  background-image: url('/assets/img/study/early_graduate.jpg');
}
</style>
<!-- Breadcrumb -->
  <div class="breadcrub breadcrub-style-3 section allcourse-title">
    <div class="bg-img d-flex flex-column justify-content-center">
<div class="overlay"></div>
      <div class="container">
        <div class="heading">
          <h1 class="page-heading">Graduate in Early Childhood Care and Education</h1>
          <p>"You cannot make people learn. You can only provide the right conditions for learning."<br>
                                   -<strong>Vince Gowman</strong></p>
        </div>
      </div>
      <!-- <div class="overlay"></div> -->
    </div>
  </div>

<!-- Course Detail -->
<div class="course-detail section" id="single-coursepage">
    <div class="container">
        <div class="row">
            	<div class="col-12 col-lg-9 content">
                <h2 class="h4 pb-2">Graduate in Early Childhood Care and Education</h2>
               <p>The Diploma in Early Childhood Care and Education Teacher Training Program is a comprehensive course designed for both new and existing early childhood educators incorporating the latest methodologies in early childhood education. The course takes stock of the emerging knowledge related to early childhood learning in an attempt to provide candidates with all essential skills that will enable early childhood teachers to successfully work with children and understand children’s individual preferences.</p>
					<div class="divider"></div>
					<h3 class="text-center pt-1 pb-2">ECCE Graduate Course Details</h3>
					<div class="tabs tabs-vertical" id="courses-tab">
					    <div class="row">
					        <div class="col-md-4">
					            <ul class="nav flex-column nav-tabs" id="course-faq" role="tablist" aria-orientation="vertical">
					                <li class="nav-item">
					                    <a class="nav-link active" href="#courseduartion" data-toggle="tab">COURSE DURATION</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#eligibility" data-toggle="tab">ELIGIBILITY</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#coursefee" data-toggle="tab">COURSE FEE</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#curriculum" data-toggle="tab">CURRICULUM</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#diploma" data-toggle="tab">DIPLOMA</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#enrollment" data-toggle="tab">ENROLLMENT PROCESS</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#academicsupport" data-toggle="tab">ACADEMIC SUPPORT</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#placements" data-toggle="tab">PLACEMENTS</a>
					                </li>
					            </ul>
					        </div>
					        <div class="col-md-8">
					            <div class="tab-content">
					                <div class="tab-pane active" id="courseduartion">
					                    <ul class="list">
					                        <li class="main-li check-arrow">9 months.</li>
					                        <li class="main-li check-arrow">Accessible around the Earth.</li>
					                        <li class="main-li check-arrow"> Scope of early completion.</li>
					                    </ul>
                              <p>The more time devoted, more quickly to get finished.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="eligibility">
					                    <ul class="list">
					                        <li class="main-li check-arrow">High School Degree from a recognized institution.</li>
                                  <li class="main-li check-arrow">Any Profession can apply.</li>
                                  <li class="main-li check-arrow">Present teachers are welcome to join our course.</li>
					                    </ul>
                              <p>Everyone who has cleared high school from an institution can attend our Early Childhood Education Diploma. Documents are required after registration.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="coursefee">
                              <ul class="list">
                                <li class="main-li check-arrow">Online Platform.</li>
                                <li class="main-li check-arrow">Priced modestly with installment option.</li>
                                <li class="main-li check-arrow">Scholarship for merit posing students.</li>
                              </ul>
                              <!-- <p>The course can be availed for a price of Rs 15,500 for Nationals. International candidates can grasp at $250. Payment option is flexible because of the online front.</p> -->
                              <p>The course can be availed for a price of $250. Payment option is flexible because of the online front.</p>

					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="curriculum">
                              <p>This course will build knowledge of how to nurture children for early education. The course is spread under the following heads:-</p>
					                    <p class="pt-1 pb-1"><strong>Theory: 8 modules.</strong><br>
                                                    <strong>Practicals: 4 modules</strong><br>
                                                    <strong>Albums: 5 modules.</strong></p>
					                    <p><strong>The Theory comprises of:-</strong></p>
					                    <ul class="list">
					                        <li class="main-li check-arrow">Life History of Dr. Maria Montessori.</li>
					                        <li class="main-li check-arrow">Montessori Methods And History.</li>
                                  <li class="main-li check-arrow">Exercise of Practical Life.</li>
                                  <li class="main-li check-arrow">Early Childhood Education.</li>
                                  <li class="main-li check-arrow">Montessori School Administration.</li>
                                  <li class="main-li check-arrow">Sensory Education in Montessori.</li>
                                  <li class="main-li check-arrow">Early Childhood Language.</li>
                                  <li class="main-li check-arrow">Methods of Teaching, Writing, and Reading.</li>
					                    </ul>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="diploma">
					                    <p>
                                 The certificate will be issued, where the term 'Online' is not mentioned anywhere after the course is finished successfully.
					                    </p>
                              <!-- <p>The Indian Candidate Certificate shall be issued free of charge. An additional <strong>US$ 25 (South-east Asia and Middle-east) and US$ 35 (Rest of the World)</strong> must be charged as shipping fees by applicants residing outside the country. We ask for a shipping address and contact number after submitting all of the documents.
The courier takes up to 3 weeks to reach the address of the applicant.</p> -->
<p>The Indian Candidate Certificate shall be issued free of charge. A standard fee of <strong>US$ 35</strong> must be charged as shipping fees by applicants residing outside the country. We ask for a shipping address and contact number after submitting all of the documents.
The courier takes up to 3 weeks to reach the address of the applicant.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="enrollment">
					                    <ul class="list">
					                        <li class="main-li check-arrow">Easy enrollment process.</li>
					                        <li class="main-li check-arrow">Payment and Application online.</li>
					                    </ul>
                              <p>Our course is demonstrated on our website, FAQs, and Blog Posts. We are always there with you during the on boarding process.</p>
                              <p>After the payment, please wait between 24-48 hours before we enable the student account and get you started towards success.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="academicsupport">
					                    <ul class="list">
					                        <li class="main-li check-arrow">Tutor support in case of academic queries.</li>
					                        <li class="main-li check-arrow">Unlimited references, videos.</li>
					                        <li class="main-li check-arrow">Self-paced</li>
					                    </ul>
					                    <p>
                               Candidates enrolled would be personally guided by our experienced faculties.
                             </p>
                             <p>Regular Doubt Clearing Session is an integral part of our program. Learning is at your own pace.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="placements">
					                    <p>Atheneum Global Teacher Training College issues a personalized letter to help the student with an internship of their choice, which helps in career success.
We also Give privileged access to Jobs for Teachers, a national portal for teachers of all kinds of teaching assignments.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					            </div>
					        </div>
					    </div>
					</div>
            	</div>

            	<div class="col-12 col-lg-3 hide-md-and-down" id="single-course-sidebar">
                    <h5>Related Courses</h5>
                    <ul class="list">
                        <li class="sub-li">
                        	<div class="post-image">
                                <div class="d-block img-thumbnail img-thumbnail-no-borders">
                                    <img src="/assets/img/course/1.jpg" width="50" height="50" alt="">
                                </div>
                            </div>
                            <div class="post-info">
                                <a href="/certificate-in-early-childhood-education-and-care">Certificate in Early Childhood Care and Education</a>
                            </div>
                        </li>
                        <li class="sub-li">
                        	<div class="post-image">
                                <div class="d-block img-thumbnail img-thumbnail-no-borders">
                                    <img src="/assets/img/course/3.jpg" width="50" height="50" alt="">
                                </div>
                            </div>
                            <div class="post-info">
                                <a href="/global-qualification-in-early-childhood-education-and-care">Post Graduate in Early Childhood Care and Education</a>
                            </div>
                        </li>
                    </ul>
                    <?php include("_common-sidebar.php");?>
                </div>
        </div>
    </div>
</div>
