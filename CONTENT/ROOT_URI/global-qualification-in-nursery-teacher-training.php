<title> Level 5global qualification in Nursery Teacher Training</title>
<meta name="keywords" content="online ntt course, nursery teacher training course online">
<meta name="description" content="Level 5 Global Qualification in NTT awarded by Ofqual regulated AO develops well-informed teachers qualified for a highly distinguished career in teaching.">
<?php include("_menu.php");?>
<link rel="canonical" href="<?php echo $url; ?>" async/>
<style>
/* .allcourse-title .page-heading {
  font-size: 30px !important;
  padding-bottom: 14px;
} */
.page-heading~p {
    font-size: 16px;
}
p,li{
  font-size: 15px !important;
}
h4{
  color:#777;
}
.breadcrub-style-3 .bg-img{
  background-image: url('/assets/img/study/graduate_ntt.jpg');
}
#more {display: none;}
</style>
<!-- Breadcrumb -->
  <div class="breadcrub breadcrub-style-3 section allcourse-title">
    <div class="bg-img d-flex flex-column justify-content-center">
<div class="overlay"></div>
      <div class="container">
        <div class="heading">
          <h1 class="page-heading">Level 5 Global Qualification In Nursery Teacher Training</h1>
        </div>
      </div>
      <!-- <div class="overlay"></div> -->
    </div>
  </div>

<!-- Course Detail -->
<div class="course-detail section" id="single-coursepage">
    <div class="container">
        <div class="row">
            	<div class="col-12 col-lg-9 content">
                <h2 class="h4 pb-2">Level 5 Global Qualification In Nursery Teacher Training</h2>
               <p>The Level 5 Global Qualification in Nursery Teacher Training focuses on the development of appropriate early childhood strategies and describes the vital role of pre-school teachers and parents in linking core concepts / skills, objectives, pedagogical processes and practices and ultimately leading to early learning outcomes. The program focuses on identifying the developmental stages in which children examine, explore and discover a great deal about themselves and develop attitudes and skills related to learning that stays with them for life. It also seeks to align the different developmental contexts with the expected learning outcomes in ways that are consistent with the normal learning activities of young children in this age group.<span id="dots">...</span><span id="more"><br><br>
               Level 5 Global Qualification in Nursery Teacher Education is the most appropriate course open to students today if you consider a career as a nursery teacher. The course prepares nursery teachers for positions of responsibility in the schools and pre-schools of their choice.<br><br>
               The course is designed to assist nursery teachers and candidates in their careers as nursery teachers, while not sacrificing themselves to the current profession or life choices that enable them to obtain a global qualification in the field of choice.</p>
               <a class="small-btn" onclick="myFunction()" id="myBtn" style="color:#fff; margin:2rem 0 !important;">Read More</a>

					<div class="divider"></div>
					<h3 class="text-center pt-1 pb-2">NTT Level 5 Global Qualification Course Details</h3>
					<div class="tabs tabs-vertical" id="courses-tab">
					    <div class="row">
					        <div class="col-md-4">
					            <ul class="nav flex-column nav-tabs" id="course-faq" role="tablist" aria-orientation="vertical">
					                <li class="nav-item">
					                    <a class="nav-link active" href="#courseduartion" data-toggle="tab">COURSE DURATION</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#eligibility" data-toggle="tab">ELIGIBILITY</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#coursefee" data-toggle="tab">COURSE FEE</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#curriculum" data-toggle="tab">CURRICULUM</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#diploma" data-toggle="tab">DIPLOMA</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#enrollment" data-toggle="tab">ENROLLMENT PROCESS</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#academicsupport" data-toggle="tab">ACADEMIC SUPPORT</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#placements" data-toggle="tab">PLACEMENTS</a>
					                </li>
					            </ul>
					        </div>
					        <div class="col-md-8">
					            <div class="tab-content">
					                <div class="tab-pane active" id="courseduartion">
					                    <ul class="list">
					                        <li class="main-li check-arrow">15 months max.</li>
					                        <li class="main-li check-arrow">It is a flexible program that can be pursued from any corner of the globe.</li>
					                        <li class="main-li check-arrow">Self-paced.</li>
                                  <li class="main-li check-arrow">Fast track mode permits early completion</li>
					                    </ul>
                              <p>Average course completion time varies between 9-15 months depending on the number of hours per week that you devote to this course. The time spent though, is well accounted for as it equips teachers with the essential knowledge base to further his or her career in this field with confidence.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="eligibility">
					                    <ul class="list">
					                        <li class="main-li check-arrow">A Bachelors Degree is the minimum requirement.</li>
                                  <li class="main-li check-arrow">Aspiring and existing teachers are free to apply.</li>
					                    </ul>
                              <p>The minimum eligibility requirement for the Level 5 Global Qualification in Nursery Teacher Training is a bachelor’s degree from any recognized institute. After you’ve enrolled yourself into the course, you’d have to submit your documentation for our records.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="coursefee">
                            <ul class="list">
                                <li class="main-li check-arrow">Program is available in online mode.</li>
                                <li class="main-li check-arrow">Very reasonably priced with installment options available.</li>
                                <li class="main-li check-arrow">Scholarships available for meritorious students.</li>
                            </ul>
                              <!-- <p>The Level 5 Global Qualification in Nursery Teacher Training is priced very affordably at Rs 25,500 for Indian citizens and $400 for international students. The course is administered completely online and hence extremely conveniently administered for students pursuing existing careers or those who require the flexibility of self paced online education. With a Level 5 Global Qualification from Atheneum Global Teacher Training College, be best placed for a global teaching career with a very affordable investment.</p> -->
                              <p>The Level 5 Global Qualification in Nursery Teacher Training is priced very affordably at $400. The course is administered completely online and hence extremely conveniently administered for students pursuing existing careers or those who require the flexibility of self paced online education. With a Level 5 Global Qualification from Atheneum Global Teacher Training College, be best placed for a global teaching career with a very affordable investment.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="curriculum">
                              <p>The curriculum has been designed for three years of nursery education before class I, which highlights goals, key concepts/ skills, pedagogical processes and early learning outcomes for nursery I, II and III. It also suggests ways of planning a nursery education programme, classroom organizations and management, assessment and building partnership with parents and community.</p>
					                    <p class="pt-1 pb-1"><strong>Keeping the NCERT guidelines in mind, the Level 5 Global Qualification course covers the following topics in depth in a course than spans 15 months.</strong></p>
                              <ul class="list">
					                        <li class="main-li check-arrow">Objectives and principles of NTT</li>
					                        <li class="main-li check-arrow">School Organization & Management</li>
                                  <li class="main-li check-arrow">Developmentally Appropriate Practices - Young Learners.</li>
                                  <li class="main-li check-arrow">Development of Young Learners</li>
                                  <li class="main-li check-arrow">Home, School and Community</li>
                                  <li class="main-li check-arrow">Education in Early Years</li>
                                  <li class="main-li check-arrow">Classroom Management Strategies</li>
                                  <li class="main-li check-arrow">Learning Plan</li>
                                  <li class="main-li check-arrow">Magic of Play</li>
                                  <li class="main-li check-arrow">Assessment of Young Learners</li>
                                  <li class="main-li check-arrow">Psychology</li>
                                  <li class="main-li check-arrow">Curriculum Development</li>
                                  <li class="main-li check-arrow">Special Education</li>
                                  <li class="main-li check-arrow">Safety, Health & Nutrition in Early Years</li>
                                  <li class="main-li check-arrow">Role of Technology in Early Years</li>
                                  <li class="main-li check-arrow">EVS</li>
                                  <li class="main-li check-arrow">Effective Lesson Planning</li>
                                  <li class="main-li check-arrow">Literacy and Numeracy in Early Years</li>
					                    </ul>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="diploma">
                              <!-- <p>Applicants are awarded the Atheneum Global Teacher Training College diploma after completing the PG  Course. The awarded diploma has an added advantage, as the word 'Online' does not appear in the certificates. Delivery of certificates within India is free of charge, but applicants residing outside India are required to pay an additional US$ 25 (South East Asian & Middle East) and US$ 35 (the rest of the world) as a certificate dispatch fee. Usually, upon submission of all assignments and completion of the course, you will have to pay the dispatch fee. After that, we will take your shipping address and contact number and send the certificate to the same address. The certificate will take a maximum of 3 weeks to reach the address of the candidate.</p> -->
                              <p>Applicants are awarded the Atheneum Global Teacher Training College diploma after completing the PG  Course. The awarded diploma has an added advantage, as the word 'Online' does not appear in the certificates. Delivery of certificates within India is free of charge, but applicants residing outside India are required to pay an standard fee of <strong>US$ 35</strong> as a certificate dispatch fee. Usually, upon submission of all assignments and completion of the course, you will have to pay the dispatch fee. After that, we will take your shipping address and contact number and send the certificate to the same address. The certificate will take a maximum of 3 weeks to reach the address of the candidate.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="enrollment">
					                    <ul class="list">
					                        <li class="main-li check-arrow">Simple and easy enrollment process.</li>
					                        <li class="main-li check-arrow">Application and payment online.</li>
					                    </ul>
                              <p>All our course requirements are clarified on our website, FAQs and Blog posts. You can also give us a call to help you with the onboarding process or to help clarify some aspects of the course.
Once you’ve made the online payment, you have to wait between 24-48 hours before we can enable your student account and get you started on your journey to academic success.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="academicsupport">
					                    <ul class="list">
					                        <li class="main-li check-arrow">Personalized tutor support.</li>
					                        <li class="main-li check-arrow">Tons of reference material and videos</li>
					                        <li class="main-li check-arrow">Self Paced Program at a mouse click away from you.</li>
					                    </ul>
                             <p>Trainees enrolled into the program would be personally tutored by our experienced faculties at Atheneum Global Teacher Training College. Regular doubt clearing sessions and other academic support is extended as part of this personalized tutoring. The learning is at your own pace and the support provided is completely personalized and carries high level of expertise.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="placements">
					                    <p>Atheneum Global Teacher Training College’s industry linkages and international credentials lends credibility to your candidature. Every candidate is issued a personalized letter to help him or her land up with an internship of their choice. This internship is an important step towards finishing your course as well as to start your journey as an educator.</p>
                              <p>In addition to this, candidates are also given privileged access to Jobs For Teachers, a national portal for teachers for all kinds of teaching assignments. In essence, what we wish to communicate is that Atheneum is committed to offer you the very best education and the very best access to teaching opportunities.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					            </div>
					        </div>
					    </div>
					</div>
            	</div>

            	<div class="col-12 col-lg-3 hide-md-and-down" id="single-course-sidebar">
                    <h5>Related Courses</h5>
                    <ul class="list">
                        <li class="sub-li">
                        	<div class="post-image">
                                <div class="d-block img-thumbnail img-thumbnail-no-borders">
                                    <img src="/assets/img/course/1.jpg" width="50" height="50" alt="">
                                </div>
                            </div>
                            <div class="post-info">
                                <a href="/certificate-in-nursery-teacher-training">Certificate in Nursery Teacher training</a>
                            </div>
                        </li>
                        <li class="sub-li">
                        	<div class="post-image">
                                <div class="d-block img-thumbnail img-thumbnail-no-borders">
                                    <img src="/assets/img/course/3.jpg" width="50" height="50" alt="">
                                </div>
                            </div>
                            <div class="post-info">
                                <a href="/graduate-in-nursery-teacher-training">Graduate in Nursery Teacher training</a>
                            </div>
                        </li>
                    </ul>
                    <?php include("_common-sidebar.php");?>
                </div>
        </div>
    </div>
</div>
<script>
function myFunction() {
  var dots = document.getElementById("dots");
  var moreText = document.getElementById("more");
  var btnText = document.getElementById("myBtn");

  if (dots.style.display === "none") {
    dots.style.display = "inline";
    btnText.innerHTML = "Read more";
    moreText.style.display = "none";
  } else {
    dots.style.display = "none";
    btnText.innerHTML = "Read less";
    moreText.style.display = "inline";
  }
}
</script>
