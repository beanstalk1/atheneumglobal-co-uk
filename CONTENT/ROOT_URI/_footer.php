<!-- Footer Section -->
<footer class="footer-section">
        <div class="container-fluid">
            <div class="divider"></div>
            <div class="row footer-content">
                <div class="col-12 col-md-6 col-lg-3">
                    <img class="brand-logo" src="assets/img/logos/atheneum.png" alt="University Logo">
                    <div class="contact-links">
                        <div class="link">
                            <i class="fas fa-envelope"></i>
                            <p><a mailto="enquiry@atheneumglobal.co.uk">enquiry@atheneumglobal.co.uk</a></p>
                        </div>
                        <div class="link">
                            <i class="fas fa-phone"></i>
                            <p>+442038077020</p>
                        </div>
                        <div class="link" style="display: flex;
    align-items: baseline;
    padding-top: 6px;">
                            <i class="fas fa-map-marker-alt"></i>
                            <p>Atheneum Global Ltd<br>
                              Kemp House 160 City Road<br>
                              London<br>
                              Ec1v2nx</p>
                        </div>
                        <div class="link social-icons">
                            <a target="_blank" href="https://www.facebook.com/AtheneumGlobalTeacherTrainingCollege/"><i class="fab fa-facebook-square"></i></a>
                            <a target="_blank" href="https://www.instagram.com/atheneum.global/"><i class="fab fa-instagram"></i></a>
                            <a target="_blank" href="https://twitter.com/atheneum_global"><i class="fab fa-twitter"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-2">
                    <h5>Links</h5>
                    <ul>
                        <li><a href="/global-teaching-opportunities">Global Teaching Opportunities</a></li>
                        <li><a href="/faq">FAQ</a></li>
                        <li><a href="/news">News</a></li>
                        <!-- <li><a href="#">Events</a></li> -->
                    </ul>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <!-- <div class="ins-feed">
                      <h5>Instagram feed</h5>
                      <ul>
                        <li>
                          <img src="assets/img/University/Homepage_5/ins-1.png" alt="insta img" class="img-fluid">
                        </li>
                        <li>
                          <img src="assets/img/University/Homepage_5/ins-2.png" alt="insta img" class="img-fluid">
                        </li>
                        <li>
                          <img src="assets/img/University/Homepage_5/ins-3.png" alt="insta img" class="img-fluid">
                        </li>
                        <li>
                            <img src="assets/img/University/Homepage_5/ins-4.png" alt="insta img" class="img-fluid">
                        </li>
                        <li>
                            <img src="assets/img/University/Homepage_5/ins-5.png" alt="insta img" class="img-fluid">
                        </li>
                        <li>
                            <img src="assets/img/University/Homepage_5/ins-6.png" alt="insta img" class="img-fluid">
                        </li>
                      </ul>
                    </div> -->
                    <h5>Information</h5>
                    <ul>
                        <li><a href="/apply-now">Apply Now</a></li>
                        <li><a href="/about">About us</a></li>
                    </ul>
                </div>
                <div class="col-12 col-md-6 col-lg-4 newsletter">
                    <h5>Download Brochure</h5>
                    <p>We bring the right people together to challenge established thinking and drive transformation. We work with our students to build the capabilities that enable organizations to achieve sustainable advantage.</p>
                    <form class="d-flex justify-content-between">
                        <input type="email" placeholder="Enter your email address here">
                        <button type="submit" class="main-btn">Download</button>
                    </form>
                </div>
            </div>


        </div>

        <div class="copyright">
            <div class="container-fluid">
                <p>© Atheneum Global 2020 | All rights reserved.</p>
            </div>
            <!-- Scroll Top Start -->
            <a id="scroll-top" class="d-flex justify-content-center align-items-center">
                <i class="flaticon-up-arrow"></i>
            </a>
        </div>
</footer>

    <!-- <div class="preloader" id="preloader">
        <div class="preloader-inner">
            <div class="spinner">
                <div class="dot1"></div>
                <div class="dot2"></div>
            </div>
        </div>
    </div> -->

    <!-- JQuery Link -->
    <script src="/assets/js/vander/jquery-3.4.1.min.js"></script>
    <script src="/assets/js/vander/jquery-migrate-1.4.1.min.js"></script>
    <script src="/assets/js/vander/popper.min.js"></script>
    <script src="/assets/js/vander/bootstrap.min.js"></script>
    <script src="/assets/js/vander/owl.carousel.min.js"></script>
    <script src="/assets/js/vander/slick.min.js"></script>
    <script src="/assets/js/vander/waypoints.min.js"></script>
    <script src="/assets/js/vander/jQuery.rcounter.js"></script>

    <!-- Google maps geolocation -->
    <!-- <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC4OJOOGMWDxoQiUbe7FfaPK83dGlCmB2g&amp;callback=initMap">
    </script> -->
    <!-- Custom Script -->
    <script src="/assets/js/main.js"></script>
    <script src="/assets/js/slider.js"></script>
    <script src="/assets/js/toggler.js"></script>
    <script src="/assets/js/vPlayer.js"></script>
    <script src="/assets/js/map.js"></script>
    <script async id="slcLiveChat" src="https://widget.sonetel.com/SonetelWidget.min.js" data-account-id="207313412"></script>
    <!-- codingeek -->
    <!-- <script src="/codingeek-link.js"></script>
    <script src="/../../codingeek-js/codingeek.js"></script> -->
</body>
</html>
