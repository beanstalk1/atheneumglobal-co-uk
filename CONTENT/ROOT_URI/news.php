<title>Informative articles on global teacher education</title>
<meta name="keywords" content="teacher training blogs">
<meta name="description" content="The right repository if you wanted to go overseas as a teacher. Read on to know what to expect as a TEFL graduate or as a professional teaching with a global teaching diploma.">
<?php include("_menu.php");?>
<link rel="canonical" href="<?php echo $url; ?>" async/>
<style>
 .breadcrub-style-3 .bg-img{
   background-image: url('/assets/img/blogs/blog.jpg');
 }
 article {
    width: calc(33.33% - 32px);
    margin: 16px;
    text-align: center;
    background-color: #f7f7f7;
    padding: 20px;
    border-radius: 5px;
    transition: 0.3s;
}
h2{
  text-align: center;
  padding-bottom: 30px;
}
article:hover {
    box-shadow: 0 10px 24px 0 rgba(98, 98, 98, 0.1);
}
h2 a {
    color: #9c2e38;
    font-size: 17px;
    padding-bottom: 15px;
    line-height: 1.3;
}

@media only screen and (max-width:992px){
  article {
     width: calc(50% - 32px) !important;
   }
   h2{
     font-size: 30px;
   }
}

@media only screen and (max-width:768px){
  article {
     width: 100% !important;
   }
   h2{
     font-size: 25px;
   }
}
</style>
<!-- Breadcrumb -->
<div class="breadcrub breadcrub-style-3 section allcourse-title">
    <div class="bg-img d-flex flex-column justify-content-center">
<div class="overlay"></div>
        <div class="container">
            <div class="heading">
                <h1 class="page-heading">News</h1>
            </div>
        </div>
        <!-- <div class="overlay"></div> -->
    </div>
</div>
<section>
  <div class="container">
    <h2>Education International - Latest News about Education Unions Worldwide</h2>
    <div class="row">
<div id="myData">
<!-- <script src="//rss.bloople.net/?url=https%3A%2F%2Fwww.ei-ie.org%2Fen%2Flatestnews%2Frss&detail=300&showicon=true&type=js"></script> -->
</div>
</div>
  </div>
</section>
