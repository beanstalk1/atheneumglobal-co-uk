<title>About Atheneum Global Teacher Training College</title>
<meta name="keywords" content="Online Teacher Training Course">
<meta name="description" content="Global exposure of our training courses for teachers. Apply to begin your teaching career with the role of a dream teacher in UK.">
<?php include("_menu.php");?>
<link rel="canonical" href="<?php echo $url; ?>" async/>
<style>
 .breadcrub-style-3 .bg-img{
   background-image: url('/assets/img/about/about.jpg');
 }
 h6{
   color:#000;
   padding: 20px 0px 0px
 }
 strong{
   color:#000;
 }
 .awardList{
   position: relative;
 }
 .awardList li {
    position: relative;
    padding-left: 32px;
}
.awardList .check-arrow::before {
    position: absolute;
    left: 0;
    top: 20px;
}
.degree span {
    font-size: 40px;
    color: #fff;
}
.degree .btn-primary {
    background-color: #811b18 !important;
    border: transparent;
}
.degree {
    width: 100%;
    background: url(./assets/img/study/level3_final.jpeg);
    background-size: cover;
    background-position: bottom center;
    position: relative;
    background-attachment: fixed;
    padding: 60px 0;
}
.degree strong{
  color:#fff;
}
.tab-content {
    padding: 30px;
    text-align: center;
}
.nav {
    justify-content: center;
}
.nav-pills .nav-link.active {
    color: #fff;
    background-color: #6c171b;
    position: relative;
}
.nav-link.active:before {
    position: absolute;
    content: "";
    border-top: 10px solid #6c171b;
    border-left: 10px solid transparent;
    border-right: 10px solid transparent;
    bottom: -10px;
    left: 43%;
}
li.nav-item .nav-link h4 {
    margin-bottom: 0px;
    font-size: 18px;
}
.nav-pills .nav-link {
    color: #000;
    border-radius: 10px;
    border:1px solid #eacccd;
    padding: 12px 16px;
}
.tab-content {
    padding: 26px 17px 1px;
    border: 1px solid #eacccd;
    border-radius: 7px;
}
.nav-pills .nav-link {
    margin: 22px 15px 8px;
    transition: 0.3s;
}
.nav-pills .nav-link:hover{
  background-color: #6C171C;
  color: #fff;
}
.commonImg {
    height: 200px;
    object-fit: scale-down;
}
</style>
<!-- Breadcrumb -->
<div class="breadcrub breadcrub-style-3 section allcourse-title">
    <div class="bg-img d-flex flex-column justify-content-center">
<div class="overlay"></div>
        <div class="container">
            <div class="heading">
                <h1 class="page-heading">About Us</h1>
            </div>
        </div>
        <!-- <div class="overlay"></div> -->
    </div>
</div>

<div class="mission-vision section">

    <!-- mantra -->
    <div class="mantra section">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8 content">
                    <h2>Who we are?</h2>
                    <p>Atheneum Global Teacher Training College is an international teacher trainer based in the UK (registration number 12604851) with students and partners all across the world. We have been training teachers since 2012 with centres across the UK, Kenya, Mauritius, Malaysia, Vietnam, and India.</p>
                </div>
                <div class="col-12 col-lg-4 media">
                    <!-- Media BG Goes Here -->
                    <img class="img-fluid" src="assets/img/University/About-us-1/video_bg.png" alt="martra media" />
                    <!-- video popup toggler -->
                    <button id="open-video-popup" class="d-flex justify-content-center align-items-center halfway-btn">
                        <i class="flaticon-play-button"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- <section id="events" class="events-section section">
        <div class="container">
            <h2 class="margin-bottom-50">Our Departments</h2>
            <div class="row odd-event">
                <div class="col-12">
                    <h3 style="text-transform: capitalize;">ENGLISH STUDIES</h3>
                    <p>
                      Atheneum Global is professionally managed and run by trained educators with global qualifications. We have very stringent norms for trainers associated with us. The Department of English Studies has trained teacher educators globally who have been assessed by our trainers who have had either a Celta or a Delta qualification. Our Internal Qualification Assurance is delivered by IQA’s with a minimum of Level 4 qualification in Quality Assurance.
                    </p>
                    <button class="main-btn" onclick="window.location.href = '/english-studies';" style="margin:2rem 0px;">Learn more</button>
                </div>
            </div>
            <div class="row even-event">
                <div class="col-12">
                    <h3 style="text-transform: capitalize;">Career Studies</h3>
                    <p>
                        The Department of Professional Studies hosts several teacher education programs like Montessori Education, Nursery Education, Early Childhood Care, and Education, Pre and Primary Teacher Training and School Organization and Management. All our trainers have globally accomplished teacher educators with degrees in Early Childhood or equivalent and professional accomplishments vetted through references.
                    </p>
                    <button class="main-btn" onclick="window.location.href = '/professional-development';" style="margin:2rem 0px;">Learn more</button>
                </div>
            </div>
            <div class="row odd-event">
                <div class="col-12">
                    <h3>BUSINESS STUDIES</h3>
                    <p>
                       The Department of Business Studies offers professional programs for aspirants looking to secure their future as Business Managers across a diverse range of industries. Our tutors have professionally accomplished teacher trainers with professional accomplishments and a history of training business management trainees.
                    </p>
                    <button class="main-btn" onclick="window.location.href = '/business-studies';" style="margin:2rem 0px;">Learn more</button>
                </div>
            </div>
        </div>
    </section> -->

<section class='pils pt-4 pb-4'>
<div class="container">
  <h2 class="text-center pb-3">Our Departments</h2>
  <div class="row">
    <div class="col-12">
      <!-- Nav pills -->
  <ul class="nav nav-pills">
    <li class="nav-item">
      <a class="nav-link active" data-toggle="pill" href="#home"><h4>English Studies</h4></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="pill" href="#menu1"><h4>Career Studies</h4></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="pill" href="#menu2"><h4>Business Studies</h4></a>
    </li>
  </ul>
<!-- Tab panes -->
<div class="tab-content">
  <div class="tab-pane container active" id="home">
    <p>
      Atheneum Global is professionally managed and run by trained educators with global qualifications. We have very stringent norms for trainers associated with us. The Department of English Studies has trained teacher educators globally who have been assessed by our trainers who have had either a Celta or a Delta qualification. Our Internal Qualification Assurance is delivered by IQA’s with a minimum of Level 4 qualification in Quality Assurance.
    </p>
    <button class="main-btn" onclick="window.location.href = '/english-studies';">Learn more</button>
  </div>
  <div class="tab-pane container fade" id="menu1">
    <p>
        The Department of Professional Studies hosts several teacher education programs like Montessori Education, Nursery Education, Early Childhood Care, and Education, Pre and Primary Teacher Training and School Organization and Management. All our trainers have globally accomplished teacher educators with degrees in Early Childhood or equivalent and professional accomplishments vetted through references.
    </p>
    <button class="main-btn" onclick="window.location.href = '/professional-development';">Learn more</button>
  </div>
  <div class="tab-pane container fade" id="menu2">
    <p>
       The Department of Business Studies offers professional programs for aspirants looking to secure their future as Business Managers across a diverse range of industries. Our tutors have professionally accomplished teacher trainers with professional accomplishments and a history of training business management trainees.
    </p>
    <button class="main-btn" onclick="window.location.href = '/business-studies';">Learn more</button>
  </div>
</div>
</div>
</div>
</div>
</section>
    <!-- video popup -->
    <div class="video-popup">
        <div class="video-popup-wrapper">
          <div class="v-container">
            <div class="close-popup">
              <i class="fas fa-times"></i>
            </div>
            <!-- <iframe src="https://player.vimeo.com/video/107999509" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe> -->
            <!-- <video src="assets/img/videos/university.mp4" id="myVideo"></video> -->
            <video src="/assets/video/aboutus.mp4" id="myVideo"></video>
            <div class="video-bar">
              <div class="passed"></div>
            </div>
            <div class="buttons">
              <button id="play-pause">
                <i class="fas fa-play"></i>
              </button>
              <button id="sound-toggler">
                <i class="fas fa-volume-up"></i>
              </button>
              <button id="share-toggler">
                <i class="fas fa-share-alt"></i>
              </button>
            </div>
          </div>
        </div>
      </div>

    <!-- About text -->
    <!-- <div class="mission-section section pt-5 pb-5" style="background-color: #f7f7f7;margin-bottom:0px !important;">
            <h2 class="margin-bottom-50 text-center">Inside looks at our offerings</h2>
            <div class="content">
                <p>Atheneum Global is professionally managed and run by trained educators with global qualifications. We have very stringent norms for trainers associated with us. The Department of English Studies has trained teacher educators globally who have been assessed by our trainers who have had either a Celta or a Delta qualification. Our Internal Qualification Assurance is delivered by IQA&rsquo;s with a minimum of Level 4 qualification in Quality Assurance.</p>
                <p>The Department of Professional Studies hosts several teacher education programs like Montessori Education, Nursery Education, Early Childhood Care, and Education, Pre and Primary Teacher Training and School Organization and Management. All our trainers have globally accomplished teacher educators with degrees in Early Childhood or equivalent and professional accomplishments vetted through references.</p>
                <p>The Department of Business Studies offers professional programs for aspirants looking to secure their future as Business Managers across a diverse range of industries. Our tutors have professionally accomplished teacher trainers with professional accomplishments and a history of training business management trainees.</p>
                <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
           </div>
    </div> -->
    <section class="degree" style="background-image:url(/assets/img/study/cda.jpg)">
      <div class="overlay"></div>
       <div class="container">
         <div class="row">
           <div class="col-auto mx-auto text-center my-auto">
              <span class="pb-4" style="display:block;">Get <strong>Yourself</strong> <strong>Enrolled</strong></span>
              <a href="/apply-now" class="btn btn-primary btn-lg" style="margin:2rem 0px;">Apply Now</a>
            </div>
         </div>
       </div>
    </section>

<section>
  <!-- <div class="container">
    <h2 class="text-center pb-4">Accreditations & Partnerships</h2>
    <div class="row">
      <div class="col-12">
      <h3>OFQUAL REGULATED COURSES</h3>
      <p>Ofqual is the regulator of qualifications, examinations and assessments in England. Ofqual is a non-ministerial government department which reports directly to the UK Parliament.</p>
      <p>It was set up in 2010 and its main objectives are:</p>
      <ul class="list">
         <li class="check-arrow">To secure that qualifications give a reliable indication of skills, knowledge and understanding and that the attainment level is consistent over time.</li>
         <li class="check-arrow">To secure that assessments give a reliable indication of skills, knowledge and understanding and that the attainment level is consistent over time.</li>
         <li class="check-arrow">To promote public confidence in regulated qualifications and assessments.</li>
         <li class="check-arrow">To promote awareness of the range of qualifications and the benefits of regulated qualifications to the public.</li>
         <li class="check-arrow">To secure efficiency and value for money in regulated qualifications.</li>
      </ul>
      <p>Ofqual recognizes awarding organizations and only recognized awarding organizations can offer regulated qualifications. In this way Ofqual makes sure only those organizations that have the appropriate resources, capability and expertise can award regulated qualifications.</p>
      <p>Focus Awards complies with Ofqual's recognition criteria and recognizes Atheneum Global Teacher Training College as a qualification provider in the field of education for select qualifications as indicated on this website.
For more information about our recognition and the regulated qualifications we offer visit.</p>
<p>For more information about our recognition and the regulated qualifications we offer visit  <a href="http://register.ofqual.gov.uk/Qualification?recognitionNumber=RN5221" href="_blank">The Register of Regulated Awarding Organisations and Qualifications.</a>
</p>
<div class="large-btn" onclick="window.location.href = '/apply-now';" style="margin:2rem 0px;">Apply Now</div> -->
 <!--
<h3></h3>
    </div>
    <ul class="list awardList">
      <li class="check-arrow">

      </li>
      <li class="check-arrow">
        <h6>UK REGISTER OF LEARNING PROVIDERS</h6>
          <p>Atheneum Global Teacher Training College is listed on the UK Government Register of Learning Providers (the UKRLP) as a verified UK learning provider.  The UKRLP is used by government departments and agencies such as the Learning and Skills Council, Careers Advice Service, HESA, HEFCE and UCAS; learners and employers.</p>
          <p>Atheneum Global Teacher Training College is registered under the UKRLP (UK Register of Learning Providers), UKPRN:10086062.</p>
      </li>
      <li class="check-arrow">
        <h6>Westminster College UK</h6>
        <p>Westminster College is one of the most reputable TEFL learning institute in the UK. Recognized by UK government as a Learning Provider Internationally and as a recognized professional TEFL educator. It has a Global presence with more than 36 locations across the globe.</p>
        <p>Atheneum Global Teacher Training College has a strong partnership program with Westminster College UK to offer dual qualifications to learners for them to benefit from the cumulative goodwill of two strong academic institutions of the United Kingdom.</p>
      </li>
      <li class="check-arrow">
        <h6>International Montessori Council</h6>
        <p>The International Montessori Council (IMC) is a professional association designed to provide specific services and support to Montessori educators and schools. Its mission is to protect the vision and legacy of Dr. Maria Montessori and the Montessori Foundation by making policy designed to promote the essential principles of best practices and authentic Montessori education. Atheneum Global Teacher Training College is proudly associated with the IMC to take Montessori principles and the methodology far and wide.</p>
      </li>
      <li class="check-arrow">
        <h6>International Montessori Society</h6>
        <p>IMS recognizes Montessori institution based on their commitment and practice to follow laws of nature with children as set forth in specific recognition criteria. IMS seeks to maintain and support this commitment through workshops, teacher education, and consultation services to assure the highest quality of program operation possible.</p>
      </li>
      <li class="check-arrow">
        <h6>IATEFL - International Association of Teachers of English as a Foreign Language</h6>
        <p>ITTT is an institutional member of IATEFL. IATEFL is one of the most thriving communities of ELT teachers in the world. Its mission is to "Link, develop and support English Language Teaching professionals worldwide". With its global reach, IATEFL encourages teachers of English around the globe to share ideas and philosophies via regular newsletters and conferences. For more information on IATEFL, please visit: www.iatefl.org.</p>
      </li>
    </ul> -->
  </div>
  </div>
</section>
<section class="pb-5">
<div class="container">
  <h2 class="text-center pb-4">Accreditations & Partnerships</h2>
  <div class="row">
    <div class="col-12">
          <div class="accordion-wrapper">
              <div id="accordion">
                  <div class="card">
                      <div class="card-header" id="headingOne">
                          <h5 class="mb-0">
                              <a role="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                  OFQUAL REGULATED COURSES
                              </a>
                          </h5>
                      </div>
                      <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                          <div class="card-body">
                            <p>Ofqual is the regulator of qualifications, examinations and assessments in England. Ofqual is a non-ministerial government department which reports directly to the UK Parliament.</p>
                            <p>It was set up in 2010 and its main objectives are:</p>
                            <ul class="list">
                               <li class="check-arrow">To secure that qualifications give a reliable indication of skills, knowledge and understanding and that the attainment level is consistent over time.</li>
                               <li class="check-arrow">To secure that assessments give a reliable indication of skills, knowledge and understanding and that the attainment level is consistent over time.</li>
                               <li class="check-arrow">To promote public confidence in regulated qualifications and assessments.</li>
                               <li class="check-arrow">To promote awareness of the range of qualifications and the benefits of regulated qualifications to the public.</li>
                               <li class="check-arrow">To secure efficiency and value for money in regulated qualifications.</li>
                            </ul>
                            <p>Ofqual recognizes awarding organizations and only recognized awarding organizations can offer regulated qualifications. In this way Ofqual makes sure only those organizations that have the appropriate resources, capability and expertise can award regulated qualifications.</p>
                            <p>Focus Awards complies with Ofqual's recognition criteria and recognizes Atheneum Global Teacher Training College as a qualification provider in the field of education for select qualifications as indicated on this website.
                      For more information about our recognition and the regulated qualifications we offer visit.</p>
                      <p>For more information about our recognition and the regulated qualifications we offer visit  <a href="http://register.ofqual.gov.uk/Qualification?recognitionNumber=RN5221" href="_blank">The Register of Regulated Awarding Organisations and Qualifications.</a>
                      </p>
                      <!-- <div class="large-btn" onclick="window.location.href = '/apply-now';" style="margin:2rem 0px;">Apply Now</div> -->
                      <img src="/assets/img/slider/Ofqual_Regulated.png" class="img-fluid commonImg">
                          </div>
                      </div>
                  </div>
                  <div class="card">
                      <div class="card-header" id="headingzero">
                          <h5 class="mb-0">
                              <a class="collapsed" role="button" data-toggle="collapse" data-target="#collapsezero" aria-expanded="false" aria-controls="collapsezero">
                                  UK REGISTER OF LEARNING PROVIDERS
                              </a>
                          </h5>
                      </div>
                      <div id="collapsezero" class="collapse" aria-labelledby="headingzero" data-parent="#accordion">
                          <div class="card-body">
                            <p>Atheneum Global Teacher Training College is listed on the UK Government Register of Learning Providers (the UKRLP) as a verified UK learning provider.  The UKRLP is used by government departments and agencies such as the Learning and Skills Council, Careers Advice Service, HESA, HEFCE and UCAS; learners and employers.</p>
                            <p>Atheneum Global Teacher Training College is registered under the UKRLP (UK Register of Learning Providers), UKPRN:10086062.</p>
                            <!-- <div class="large-btn" onclick="window.location.href = '/apply-now';" style="margin:2rem 0px;">Apply Now</div> -->
                          </div>
                      </div>
                  </div>
                  <div class="card">
                      <div class="card-header" id="headingTwo">
                          <h5 class="mb-0">
                              <a class="collapsed" role="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                  Westminster College UK
                              </a>
                          </h5>
                      </div>
                      <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                          <div class="card-body">
                            <p>Westminster College is one of the most reputable TEFL learning institute in the UK. Recognized by UK government as a Learning Provider Internationally and as a recognized professional TEFL educator. It has a Global presence with more than 36 locations across the globe.</p>
                            <p>Atheneum Global Teacher Training College has a strong partnership program with Westminster College UK to offer dual qualifications to learners for them to benefit from the cumulative goodwill of two strong academic institutions of the United Kingdom.</p>
                            <!-- <div class="large-btn" onclick="window.location.href = '/apply-now';" style="margin:2rem 0px;">Apply Now</div> -->
                          </div>
                      </div>
                  </div>
                  <div class="card">
                      <div class="card-header" id="headingThree">
                          <h5 class="mb-0">
                              <a class="collapsed" role="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                  International Montessori Council
                              </a>
                          </h5>
                      </div>
                      <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                          <div class="card-body">
                            <p>The International Montessori Council (IMC) is a professional association designed to provide specific services and support to Montessori educators and schools. Its mission is to protect the vision and legacy of Dr. Maria Montessori and the Montessori Foundation by making policy designed to promote the essential principles of best practices and authentic Montessori education. Atheneum Global Teacher Training College is proudly associated with the IMC to take Montessori principles and the methodology far and wide.</p>
                            <!-- <div class="large-btn" onclick="window.location.href = '/apply-now';" style="margin:2rem 0px;">Apply Now</div>                           -->
                          <img src="/assets/img/study/IMC.png" alt="IMC" class="img-fluid commonImg">
                        </div>
                      </div>
                  </div>
                  <div class="card">
                      <div class="card-header" id="headingFour">
                          <h5 class="mb-0">
                              <a class="collapsed" role="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                   International Montessori Society
                              </a>
                          </h5>
                      </div>
                      <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                          <div class="card-body">
                            <p>IMS recognizes Montessori institution based on their commitment and practice to follow laws of nature with children as set forth in specific recognition criteria. IMS seeks to maintain and support this commitment through workshops, teacher education, and consultation services to assure the highest quality of program operation possible.</p>
                            <!-- <div class="large-btn" onclick="window.location.href = '/apply-now';" style="margin:2rem 0px;">Apply Now</div> -->
                            <img src="/assets/img/study/IMS.png" alt="IMS" class="img-fluid commonImg">
                          </div>
                      </div>
                  </div>
                  <div class="card">
                      <div class="card-header" id="headingFive">
                          <h5 class="mb-0">
                              <a class="collapsed" role="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                  IATEFL - International Association of Teachers of English as a Foreign Language
                              </a>
                          </h5>
                      </div>
                      <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                          <div class="card-body">
                            <p>The International Montessori Council (IMC) is a professional association designed to provide specific services and support to Montessori educators and schools. Its mission is to protect the vision and legacy of Dr. Maria Montessori and the Montessori Foundation by making policy designed to promote the essential principles of best practices and authentic Montessori education. Atheneum Global Teacher Training College is proudly associated with the IMC to take Montessori principles and the methodology far and wide.</p>
                            <!-- <div class="large-btn" onclick="window.location.href = '/apply-now';" style="margin:2rem 0px;">Apply Now</div> -->
                            <img src="/assets/img/slider/IATEFL.png" class="img-fluid commonImg" alt="IATEFL">
                          </div>
                      </div>
                  </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </section>
<!-- <section id="events" class="events-section section">
    <div class="container">
        <h2 class="margin-bottom-50">The Register of Regulated Awarding Organisations and Qualifications.</h2>
        <div class="row event-item-style-1 odd-event">
            <div class="col-7 col-md-6 col-lg-3 media">
                <img src="assets/img/course/mtt.jpg" alt="events image" class="img-fluid" />
            </div>
            <div class="col-12 col-lg-7 details">
                <h3 style="text-transform: capitalize;">UK REGISTER OF LEARNING PROVIDERS</h3>
                <p>Atheneum Global Teacher Training College is listed on the UK Government Register of Learning Providers (the UKRLP) as a verified UK learning provider.  The UKRLP is used by government departments and agencies such as the Learning and Skills Council, Careers Advice Service, HESA, HEFCE and UCAS; learners and employers.</p>
                <p>Atheneum Global Teacher Training College is registered under the UKRLP (UK Register of Learning Providers), UKPRN:10086062.</p>
                <div class="large-btn" onclick="window.location.href = '/apply-now';" style="margin:2rem 0px;">Apply Now</div>
            </div>
        </div>
        <div class="row event-item-style-1 even-event">
            <div class="col-7 col-md-6 col-lg-3 media">
                <img src="assets/img/course/pptt.jpg" alt="events image" class="img-fluid" />
            </div>
            <div class="col-12 col-lg-7 details">
                <h3 style="text-transform: capitalize;">Westminster College UK</h3>
                <p>Westminster College is one of the most reputable TEFL learning institute in the UK. Recognized by UK government as a Learning Provider Internationally and as a recognized professional TEFL educator. It has a Global presence with more than 36 locations across the globe.</p>
                <p>Atheneum Global Teacher Training College has a strong partnership program with Westminster College UK to offer dual qualifications to learners for them to benefit from the cumulative goodwill of two strong academic institutions of the United Kingdom.</p>
                <div class="large-btn" onclick="window.location.href = '/apply-now';" style="margin:2rem 0px;">Apply Now</div>
            </div>
        </div>
        <div class="row event-item-style-1 odd-event">
            <div class="col-7 col-md-6 col-lg-3 media">
                <img src="assets/img/course/ntt.jpg" alt="events image" class="img-fluid" />
            </div>
            <div class="col-12 col-lg-7 details">
                <h3>International Montessori Council</h3>
                <p>The International Montessori Council (IMC) is a professional association designed to provide specific services and support to Montessori educators and schools. Its mission is to protect the vision and legacy of Dr. Maria Montessori and the Montessori Foundation by making policy designed to promote the essential principles of best practices and authentic Montessori education. Atheneum Global Teacher Training College is proudly associated with the IMC to take Montessori principles and the methodology far and wide.</p>
                <div class="large-btn" onclick="window.location.href = '/apply-now';" style="margin:2rem 0px;">Apply Now</div>
            </div>
        </div>
        <div class="row event-item-style-1 even-event">
            <div class="col-7 col-md-6 col-lg-3 media">
                <img src="assets/img/course/pptt.jpg" alt="events image" class="img-fluid" />
            </div>
            <div class="col-12 col-lg-7 details">
                <h3 style="text-transform: capitalize;">International Montessori Society</h3>
                <p>IMS recognizes Montessori institution based on their commitment and practice to follow laws of nature with children as set forth in specific recognition criteria. IMS seeks to maintain and support this commitment through workshops, teacher education, and consultation services to assure the highest quality of program operation possible.</p>
                <div class="large-btn" onclick="window.location.href = '/apply-now';" style="margin:2rem 0px;">Apply Now</div>
            </div>
        </div>
        <div class="row event-item-style-1 odd-event">
            <div class="col-7 col-md-6 col-lg-3 media">
                <img src="assets/img/course/ntt.jpg" alt="events image" class="img-fluid" />
            </div>
            <div class="col-12 col-lg-7 details">
                <h3>IATEFL - International Association of Teachers of English as a Foreign Language</h3>
                <p>The International Montessori Council (IMC) is a professional association designed to provide specific services and support to Montessori educators and schools. Its mission is to protect the vision and legacy of Dr. Maria Montessori and the Montessori Foundation by making policy designed to promote the essential principles of best practices and authentic Montessori education. Atheneum Global Teacher Training College is proudly associated with the IMC to take Montessori principles and the methodology far and wide.</p>
                <div class="large-btn" onclick="window.location.href = '/apply-now';" style="margin:2rem 0px;">Apply Now</div>
            </div>
        </div>
    </div>
</section> -->
<section class="degree">
   <div class="container">
     <div class="row">
       <div class="col-auto mx-auto text-center my-auto">
          <span class="pb-4" style="display:block;">Get <strong>Yourself</strong> <strong>Enrolled</strong></span>
          <a href="/apply-now" class="btn btn-primary btn-lg" style="margin:2rem 0px;">Apply Now</a>
        </div>
     </div>
   </div>
</section>
