<title>TESOL Certification course offered by Atheneum</title>
<meta name="keywords" content="tesol certification, tesol course">
<meta name="description" content="100% online TESOL certificate course for those who want to progress their ESL career internationally. Apply now for this 150 hrs of TESOL course.">
<?php include("_menu.php");?>
<link rel="canonical" href="<?php echo $url; ?>" async/>
<style>
 .breadcrub-style-3 .bg-img{
   background-image: url('/assets/img/study/tesol.jpg');
 }
 </style>
<div class="breadcrub breadcrub-style-3 section allcourse-title">
    <div class="bg-img d-flex flex-column justify-content-center">
<div class="overlay"></div>
        <div class="container">
            <div class="heading">
                <h1 class="page-heading">TESOL CERTIFICATION (150 HRS)</h1>
            </div>
        </div>
    </div>
</div>

<div class="course-detail section" id="single-coursepage">
    <div class="container">
        <div class="row">
            	<div class="col-12 col-lg-9 content">
            		<h2>About this course</h2>
					<p>
					    The TESOL Course will discuss the key skills and knowledge related to Teaching English to Speakers of Other Languages. This course will introduce new topics and continue to build on important aspects related to Teaching English to
					    Speakers of Other Languages. Ideal for those wanting to learn the skills and confidence to support those learning English as a foreign language, with the use of practical examples.
					</p>
					<h3>Learning Outcomes of TESOL (TEFL) Trainer Certification:</h3>
					<ul class="list">
					    <li class="main-li check-arrow">Gain the fundamental knowledge of the principles of teaching English as a foreign language</li>
					    <li class="main-li check-arrow">Discover a wide range of approaches and methods for teaching foreign students</li>
					    <li class="main-li check-arrow">Develop the practical skills to deliver engaging, informative and inclusive lessons</li>
					    <li class="main-li check-arrow">Explore the role and responsibilities of a TESOL teacher &amp; understand the challenges teacher and students may face</li>
					    <li class="main-li check-arrow">Receive an internationally recognized teacher education qualification on successful course completion</li>
					    <li class="main-li check-arrow">Learn effective methods for teaching the 4 language skills: reading, writing, speaking and listening</li>
					    <li class="main-li check-arrow">Understand the basics of classroom management to create dynamic lesson plans</li>
					    <li class="main-li check-arrow">Get expert career guidance and understand your job opportunities &amp; requirements for teaching roles</li>
					</ul>
					<div class="divider"></div>
					<h3>TESOL Certificate Course Details</h3>
					<div class="tabs tabs-vertical" id="courses-tab">
					    <div class="row">
					        <div class="col-md-4">
					            <ul class="nav flex-column nav-tabs" id="course-faq" role="tablist" aria-orientation="vertical">
					                <li class="nav-item">
					                    <a class="nav-link active" href="#coursecontent" data-toggle="tab">Course Content</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#procedure" data-toggle="tab">Procedure</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#practicum" data-toggle="tab">Practicum</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#assessment" data-toggle="tab">Assessment</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#admissionrequirements" data-toggle="tab">Admission requirements</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#qualification" data-toggle="tab">Qualification</a>
					                </li>
					            </ul>
					        </div>
					        <div class="col-md-8">
					            <div class="tab-content">
					                <div class="tab-pane active" id="coursecontent">
					                    <ul class="list">
										    <li class="main-li check-arrow">Principles of Teaching English to Speakers of Other Languages</li>
										    <li class="main-li check-arrow">Understanding English Grammar</li>
										    <li class="main-li check-arrow">Teaching English Grammar</li>
										    <li class="main-li check-arrow">Teaching English Vocabulary</li>
										    <li class="main-li check-arrow">Teaching Productive Skills</li>
										    <li class="main-li check-arrow">Teaching Receptive Skills</li>
										    <li class="main-li check-arrow">Materials and Aids for Teaching English</li>
										    <li class="main-li check-arrow">Teaching Pronunciation of English</li>
										    <li class="main-li check-arrow">Lesson Planning for Teaching English</li>
										    <li class="main-li check-arrow">Teaching English to Young Learners</li>
										    <li class="main-li check-arrow">Using Resources Effectively When Teaching English to Speakers of Other Languages</li>
										</ul>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="procedure">
					                    <p>You are assigned a personal tutor as soon as you enrol, who will give you help and guidance through the course as well as mark your assignments and give you personal feedback.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="practicum">
					                    <p>Practicum is an essential component to a teacher’s training process, as it provides valuable insight and experience prior to obtaining independent teaching positions. Atheneum Global Teacher Training College requires that all students accumulate a minimum of 6 hours for TEFL/TESOL certification. These hours can be achieved through observation, tutoring, student teaching or a combination of 2 or 3 of these in an ESL/EFL setting in which non-native speakers are learning new English language skills.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="assessment">
					                    <p>
										    Our three learning blocks enable us to mark you fairly, using your reflective diaries (6% of your final grade), your assignments (44% of your grade), and your teaching practice (50% of your grade). So regardless of your learning style,
										    you will be able to obtain a high score through hard work
										</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="admissionrequirements">
					                    <p>Completion of high school.</p>
										<p>One of the following if you are not an English native speaker</p>
										<ul class="list">
										    <li class="main-li check-arrow">High Intermediate level of English or</li>
										    <li class="main-li check-arrow">IELTS 6.5</li>
										</ul>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="qualification">
					                    <p>On completion of your course, you will receive a certificate in Teaching English to Speakers of Other Languages from Atheneum Global Teacher Training College UK.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					            </div>
					        </div>
					    </div>
					</div>
            	</div>

            	<div class="col-12 col-lg-3 hide-md-and-down" id="single-course-sidebar">
                    <h5>Related Courses</h5>
                    <ul class="list">
                    	<li class="sub-li">
                        	<div class="post-image">
                                <div class="d-block img-thumbnail img-thumbnail-no-borders">
                                    <img src="/assets/img/course/1.jpg" width="50" height="50" alt="">
                                </div>
                            </div>
                            <div class="post-info">
                                <a href="/tefl">TEFL Level 5 OFQUAL Program</a>
                            </div>
                        </li>
                        <!-- <li class="sub-li">
                        	<div class="post-image">
                                <div class="d-block img-thumbnail img-thumbnail-no-borders">
                                    <img src="/assets/img/course/1.jpg" width="50" height="50" alt="">
                                </div>
                            </div>
                            <div class="post-info">
                                <a href="/tesol-certification">TESOL CERTIFICATION</a>
                            </div>
                        </li> -->
                        <li class="sub-li">
                        	<div class="post-image">
                                <div class="d-block img-thumbnail img-thumbnail-no-borders">
                                    <img src="/assets/img/course/3.jpg" width="50" height="50" alt="">
                                </div>
                            </div>
                            <div class="post-info">
                                <a href="/tesol-masters">TESOL MASTERS </a>
                            </div>
                        </li>
                    </ul>
                    <?php include("_common-sidebar.php");?>
                </div>
        </div>
    </div>
</div>
