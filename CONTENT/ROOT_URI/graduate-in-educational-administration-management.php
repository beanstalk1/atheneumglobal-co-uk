<title>Graduate Diploma in Educational Administration and management</title>
<meta name="keywords" content="Educational Administration and management">
<meta name="description" content="This Graduate Diploma leadership program was developed to meet the demand for skilled manpower in administrative and management roles in UK and beyond.">
<?php include("_menu.php");?>
<link rel="canonical" href="<?php echo $url; ?>" async/>
<style>
/* .allcourse-title .page-heading {
  font-size: 30px !important;
  padding-bottom: 14px;
} */
.page-heading~p {
    font-size: 16px;
}
p,li{
  font-size: 15px !important;
}
h4{
  color:#777;
}
.breadcrub-style-3 .bg-img{
  background-image: url('/assets/img/study/school_graduate.jpg');
}

</style>
<!-- Breadcrumb -->
  <div class="breadcrub breadcrub-style-3 section allcourse-title">
    <div class="bg-img d-flex flex-column justify-content-center">
<div class="overlay"></div>
      <div class="container">
        <div class="heading">
          <h1 class="page-heading">Graduate in School Organization</h1>
        </div>
      </div>
      <!-- <div class="overlay"></div> -->
    </div>
  </div>

<!-- Course Detail -->
<div class="course-detail section" id="single-coursepage">
    <div class="container">
        <div class="row">
            	<div class="col-12 col-lg-9 content">
                <h2 class="h4 pb-2">Graduate in School Organization</h2>
               <p>With a booming education industry on the verge of becoming underserved by high-quality teachers, there is an even more desperate need for well-trained and skilled managers who are prepared to operate institutions that care for children. The Graduate Diploma in School Organization was created to meet the demand for skilled personnel in management and management positions in different education systems and organizations.
The Diploma serves as an administrative capacity for students considering a career in an early childhood environment. The course helps early childhood teachers and aspirants to pursue an administrative career as a teacher, while not compromising on the current occupation or life choices that require them to obtain a global degree in the chosen sector.</p>
					<div class="divider"></div>
					<h3 class="text-center pt-1 pb-2">School Organization Graduate Course Details</h3>
					<div class="tabs tabs-vertical" id="courses-tab">
					    <div class="row">
					        <div class="col-md-4">
					            <ul class="nav flex-column nav-tabs" id="course-faq" role="tablist" aria-orientation="vertical">
					                <li class="nav-item">
					                    <a class="nav-link active" href="#courseduartion" data-toggle="tab">COURSE DURATION</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#eligibility" data-toggle="tab">ELIGIBILITY</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#coursefee" data-toggle="tab">COURSE FEE</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#curriculum" data-toggle="tab">CURRICULUM</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#diploma" data-toggle="tab">DIPLOMA</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#enrollment" data-toggle="tab">ENROLLMENT PROCESS</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#academicsupport" data-toggle="tab">ACADEMIC SUPPORT</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#placements" data-toggle="tab">PLACEMENTS</a>
					                </li>
					            </ul>
					        </div>
					        <div class="col-md-8">
					            <div class="tab-content">
					                <div class="tab-pane active" id="courseduartion">
					                    <ul class="list">
					                        <li class="main-li check-arrow">Maximum duration of the course is 9 months..</li>
					                        <li class="main-li check-arrow">It is a flexible program that can be pursued from any corner of the globe.</li>
					                        <li class="main-li check-arrow">Self-paced.</li>
                                  <li class="main-li check-arrow">Fast track mode permits early completion.</li>
					                    </ul>
                              <p>Average course completion time varies between 6-9 months depending on the number of hours per week that you devote to this course. The time spent though, is well accounted for as it equips teachers with the essential knowledge base to further his or her career in this field with confidence.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="eligibility">
					                    <ul class="list">
					                        <li class="main-li check-arrow">A Bachelors degree is the minimum requirement for this course.</li>
                                  <li class="main-li check-arrow">Aspiring and existing teachers are free to apply.</li>
					                    </ul>
                              <p>The minimum eligibility requirement for the Graduate Diploma in School Organization is a bachelor’s degree from any recognized institute. After you’ve enrolled yourself into the course, you’d have to submit your documentation for our records.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="coursefee">
                              <ul class="list">
                                <li class="main-li check-arrow">Program is available in online mode..</li>
                                <li class="main-li check-arrow">Very reasonably priced with installment options available.</li>
                                <li class="main-li check-arrow">Scholarships available for meritorious students.</li>
                              </ul>
                              <p>The Graduate Diploma in School Organization is priced very affordably at <strong>$250 </strong>for students. The course is administered completely online and hence extremely conveniently administered for students pursuing existing careers or those who require the flexibility of self paced online education. With a Graduation from Atheneum Global Teacher Training College, be best placed for a global teaching career with a very affordable investment.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="curriculum">
                              <p>The online Diploma in Educational Administration and Management is suitable for academics, administrative officials, heads of educational organizations such as principal and HODs.</p>
                              <p>Gain detailed information from the extensive curriculum and learn Academic Leadership and Training Center Management strategies to prepare for administrative responsibilities.</p>
                              <p class="pt-1 pb-1"><strong>Theory: 5 modules.</strong></p>
					                    <p><strong>THEORY</strong></p>
					                    <ul class="list">
					                        <li class="main-li check-arrow">Introduction to Early Childhood Education.</li>
					                        <li class="main-li check-arrow">Principles, Objectives of Early Childhood Education and Historical Approaches.</li>
                                  <li class="main-li check-arrow">Early Childhood Education curriculum.</li>
                                  <li class="main-li check-arrow">Early Childhood Education Organization.</li>
                                  <li class="main-li check-arrow">Early Childhood Education Management.</li>
					                    </ul>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="diploma">
					                    <p>
After the completion of the Graduate Diploma course, candidates are awarded a Diploma by Atheneum Global Teacher Training College. The diploma awarded has an added advantage as the word ‘Online’ is not mentioned in the certificates. Shipment of certificates within India is done free of cost but the candidates residing outside India are required to pay an stndard fee of <strong>US $ 35</strong> as certificate dispatch fee. Generally after submission of all the assignments and completion of the course, you have to pay the dispatch fee. After that we take your shipping address and contact number and courier the certificate to the same. It takes a maximum of 3 weeks for the certificates to reach the candidate’s address.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="enrollment">
					                    <ul class="list">
					                        <li class="main-li check-arrow">Simple and easy enrollment process.</li>
					                        <li class="main-li check-arrow">Application and payment online.</li>
					                    </ul>
                              <p>All our course requirements are clarified on our website, FAQs and Blog posts. You can also give us a call to help you with the onboarding process or to help clarify some aspects of the course.</p>
                              <p>Once you’ve made the online payment, you have to wait between 24-48 hours before we can enable your student account and get you started on your journey to academic success.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="academicsupport">
					                    <ul class="list">
					                        <li class="main-li check-arrow">Personalized tutor support.</li>
					                        <li class="main-li check-arrow">Tons of reference material and videos.</li>
					                        <li class="main-li check-arrow">Self Paced Program at a mouse click away from you.</li>
					                    </ul>
					                    <p>
                               Trainees enrolled into the program would be personally tutored by our experienced faculties at Atheneum Global Teacher Training College. Regular doubt clearing sessions and other academic support is extended as part of this personalized tutoring. The learning is at your own pace and the support provided is completely personalized and carries high level of expertise.
                             </p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="placements">
					                    <p>Atheneum Global Teacher Training College’s industry linkages and international credentials lends credibility to your candidature. Every candidate is issued a personalized letter to help him or her land up with an internship of their choice. This internship is an important step towards finishing your course as well as to start your journey as an educator.</p>
                              <p>In addition to this, candidates are also given privileged access to Jobs For Teachers, a national portal for teachers for all kinds of teaching assignments. In essence, what we wish to communicate is that Atheneum is committed to offer you the very best education and the very best access to teaching opportunities.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					            </div>
					        </div>
					    </div>
					</div>
            	</div>

            	<div class="col-12 col-lg-3 hide-md-and-down" id="single-course-sidebar">
                    <h5>Related Courses</h5>
                    <ul class="list">
                        <li class="sub-li">
                        	<div class="post-image">
                                <div class="d-block img-thumbnail img-thumbnail-no-borders">
                                    <img src="/assets/img/course/1.jpg" width="50" height="50" alt="">
                                </div>
                            </div>
                            <div class="post-info">
                                <a href="/certificate-in-educational-administration-management">Certificate in School Organization : Administration and Management</a>
                            </div>
                        </li>
                        <li class="sub-li">
                        	<div class="post-image">
                                <div class="d-block img-thumbnail img-thumbnail-no-borders">
                                    <img src="/assets/img/course/3.jpg" width="50" height="50" alt="">
                                </div>
                            </div>
                            <div class="post-info">
                                <a href="/global-qualification-in-educational-administration-management">Post Graduate in School Organization : Administration and Management</a>
                            </div>
                        </li>
                    </ul>
                    <?php include("_common-sidebar.php");?>
                </div>
        </div>
    </div>
</div>
