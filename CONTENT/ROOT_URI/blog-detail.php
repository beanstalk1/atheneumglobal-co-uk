<title>Atheneum Global Teacher Training College UK</title>
<meta name="description" content="">
<?php include("_menu.php");?>
<link rel="canonical" href="<?php echo $url; ?>" async/>

<!-- Breadcrumb -->
<div class="breadcrub breadcrub-style-3 section allcourse-title">
    <div class="bg-img">
        <div class="container">
            <div class="heading">
                <h1 class="page-heading">Single Blog Post</h1>
            </div>
        </div>
        <!-- <div class="overlay"></div> -->
    </div>
</div>

<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-9 content">
            	<div class="media-grid">
      				<div class="row">
        				<div class="col-12 img">
        					<img src="assets/img/University/Detailed_blog/bay-beach-birds-eye-view.png" alt="blog cover" class="img-fluid">
        				</div>
        			</div>
        		</div>
            	<div class="heading text-center">
			      <h2>My passion for photography</h2>
			      <h6 class="date">4 JAN, 2019</h6>
			    </div>
			    <div class="blog-contents mt-5">
			    	<div class="container">
        				<div class="blog-text">
        					<p>Lorem ipsum, dolor sit Lorem ipsum dolor sit, amet consectetur adipisicing elit. Vitae modi, consequatur
				            velit in a nostrum laborum iusto quam ut voluptatum! amet consectetur adipisicing elit. Cumque fugiat, quod
				            aspernatur eum aliquid laborum, tempora minus, vero illo sapiente modi optio nulla odio doloremque
				            architecto. Dolor neque expedita sed.</p>
				          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Asperiores, officia quia! Corrupti mollitia at
				            aspernatur ab deserunt explicabo praesentium odit, voluptate autem, atque fugit voluptatum incidunt
				            repudiandae reiciendis sint nemo. Nobis minus doloribus ut reprehenderit optio doloremque expedita non vel?
				            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Et iste cupiditate vitae dolore provident ducimus
				            ex harum modi quibusdam, accusantium ipsum rerum voluptatibus eaque qui voluptate facilis similique at rem!
				          </p>
				          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quo deserunt consequuntur dicta possimus sit
				            laborum, perspiciatis cupiditate consequatur. Exercitationem, repellendus! Aliquam, harum amet in, suscipit
				            asperiores tempore fugit nulla inventore, expedita nisi enim aspernatur possimus! Ratione corporis minus eum
				            quisquam vitae facilis ad ipsa nihil. Odio provident soluta facere, deserunt ex tenetur necessitatibus illum
				            blanditiis eligendi. Unde enim corrupti provident ad? Hic porro tenetur maxime sit, ducimus a veritatis enim
				            nulla labore voluptatum voluptate omnis! Repudiandae iste officia quod doloremque excepturi aliquid,
				            nesciunt dolor quia maiores modi veritatis cumque temporibus esse earum obcaecati deserunt fugit illum
				            numquam inventore velit molestiae?</p>
        				</div>
        			</div>
        		</div>
			</div>
            <div class="col-12 col-lg-3 hide-md-and-down" id="single-course-sidebar">
                    <ul>
                        <li class="d-block align-items-center search-input-group">
                            <input type="text" placeholder="Type something to search" />
                            <i class="flaticon-search"></i>            
                        </li>
                    </ul>
                    <h5>Our Courses</h5>
                     <ul class="list">
                     	<li class="sub-li">
                            <a href="/tefl" class="point-right-arrow">TEFL Level 5 Program</a>
                        </li>
                        <li class="sub-li">
                            <a href="/tesol-certification" class="point-right-arrow">TESOL Certification</a>
                        </li>
                        <li class="sub-li">
                            <a href="/tesol-masters" class="point-right-arrow">TESOL Masters</a>
                        </li>
                        <li class="sub-li">
                            <a href="/montessori-teacher-training" class="point-right-arrow">Montessori Teacher Training</a>
                        </li>
                        <li class="sub-li">
                            <a href="/pre-primary-teacher-training" class="point-right-arrow">Nursery Teacher Training</a>
                        </li>
                        <li class="sub-li">
                            <a href="/early-childhood-education-and-care" class="point-right-arrow">Pre & Primary Teacher Training</a>
                        </li>
                        <li class="sub-li">
                            <a href="/nursery-teacher-training" class="point-right-arrow">Early Childhood Care and Education</a>
                        </li>
                        <li class="sub-li">
                            <a href="/education-management" class="point-right-arrow">School Organization : Administration & Management</a>
                        </li>
                        <li class="sub-li">
                            <a href="/child-development-associate" class="point-right-arrow">Child Development Associate</a>
                        </li>
                    </ul>
                    <?php include("_common-sidebar.php");?>
            </div>
        </div>
    </div>
</div>