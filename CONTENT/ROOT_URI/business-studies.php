<title>Learn business administration and management | Atheneum</title>
<meta name="keywords" content="Business Administration, business management">
<meta name="description" content="Atheneum offers globally recognized TEFL and TESOL courses to the aspiring candidates. Our courses are Ofqual regulated making it a genuine UK qualification.">
<?php include("_menu.php");?>
<link rel="canonical" href="<?php echo $url; ?>" async/>
<style>
 .breadcrub-style-3 .bg-img{
   background-image: url('/assets/img/study/business-study.jpg');
 }
 </style>

<!-- Breadcrumb -->
  <div class="breadcrub breadcrub-style-3 section allcourse-title">
    <div class="bg-img d-flex flex-column justify-content-center">
<div class="overlay"></div>
      <div class="container">
        <div class="heading">
          <h1 class="page-heading">Business Studies</h1>
        </div>
      </div>
      <!-- <div class="overlay"></div> -->
    </div>
  </div>

<!-- Course Detail -->
<div class="course-detail section" id="main-coursepage">
    <div class="container">
        <div class="col-12 col-xl-12">
            <div class="row">
                <div class="col-12 col-lg-9 content">
                	<h3>Our different Business Administration Programs</h3>
                    <div class="row" id="different-certificate">
                      <div class="col-12 col-md-4">
                        <div class="course-card-style-2">
                          <div class="card">
                            <img class="card-img-top" src="/assets/img/course/2.jpg" alt="Ofqual Level 3 Diploma in Business Administration">
                            <div class="card-img-overlay">
                              <div class="card-body">
                                <h5>Ofqual Level 3 Diploma in Business Administration</h5>
                                <!-- <p class="details">Certificate course in Montessori Teacher Training</p> -->
                                <div class="more">
                                  <a href="/ofqual-level-3-diploma-in-business-administration/" class="small-btn">View Course</a>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-md-4">
                        <div class="course-card-style-2">
                          <div class="card">
                            <img class="card-img-top" src="assets/img/course/4.jpg" alt="Ofqual Level 5 Diploma in Management and Leadership">
                            <div class="card-img-overlay">
                              <div class="card-body">
                                <h5>Ofqual Level 5 Diploma in Management and Leadership</h5>
                                <!-- <p class="details">Graduate Course in Montessori Teacher Training</p> -->
                                <div class="more">
                                  <a href="/ofqual-level-5-diploma-in-management-and-leadership" class="small-btn">View Course</a>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-md-4">
                        <div class="course-card-style-2">
                          <div class="card">
                            <img class="card-img-top" src="assets/img/course/8.jpg" alt="Diploma in Business Administration">
                            <div class="card-img-overlay">
                              <div class="card-body">
                                <h5>Diploma in Business Administration</h5>
                                <!-- <p class="details">Graduate Course in Montessori Teacher Training</p> -->
                                <div class="more">
                                  <a href="/diploma-in-business-administration" class="small-btn">View Course</a>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                  	</div>
                  	<div class="row" id="">
                      <div class="col-12 col-md-4">
                        <div class="course-card-style-2">
                          <div class="card">
                            <img class="card-img-top" src="/assets/img/course/7.jpg" alt="Tourism Management">
                            <div class="card-img-overlay">
                              <div class="card-body">
                                <h5>Tourism Management</h5>
                                <!-- <p class="details">Certificate course in Montessori Teacher Training</p> -->
                                <div class="more">
                                  <a href="/tourism-management" class="small-btn">View Course</a>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
                <div class="col-12 col-lg-3 hide-md-and-down" id="all-sidebar">
                    <?php include("_common-sidebar.php");?>
                </div>
            </div>
        </div>
    </div>
</div>
