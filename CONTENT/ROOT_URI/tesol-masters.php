<title>TESOL MastersProgram from Atheneum</title>
<meta name="keywords" content="tesol certification online, tesol certification online accredited">
<meta name="description" content="Our 450 hrs of TESOL Masters Program provides an in-depth ESL curriculum for committed learners looking for a global qualification for an international teaching career.">
<?php include("_menu.php");?>
<link rel="canonical" href="<?php echo $url; ?>" async/>
<style>
 .breadcrub-style-3 .bg-img{
   background-image: url('/assets/img/study/masters.jpg');
 }
 </style>
<div class="breadcrub breadcrub-style-3 section allcourse-title">
    <div class="bg-img d-flex flex-column justify-content-center">
<div class="overlay"></div>
        <div class="container">
            <div class="heading">
                <h1 class="page-heading">TESOL MASTERS (450 HRS)</h1>
            </div>
        </div>
    </div>
</div>

<div class="course-detail section" id="single-coursepage">
    <div class="container">
        <div class="row">
            	<div class="col-12 col-lg-9 content">
            		<p>A TESOL certification allows learners to gain a sufficient understanding of language systems and commonly used teaching approaches to embark on a career as an educator. The qualification will enable learners to become independent teachers with the tools to plan appropriate lessons and courses for their students without support if necessary.</p>
            		<h2>Progression Pathways</h2>
            		<p>Upon completing this certificate program, learners can progress along a pathway to study for a higher level of qualification in teaching and/or linguistics, should they choose to in the future.</p>
            		<h2>About this course</h2>
					<p>Atheneum Global Teacher Training College is a leading provider of courses in teaching English to speakers of other languages (TESOL). Our courses are delivered 100% online through Atheneum’s e-learning platform. Our teacher-training qualifications are among the most widely recognized and respected in their field.</p>
					<p>This course is suitable for experienced teachers of ESOL hoping to work in an area or organization that requires a an advanced course, eg more senior teaching positions, academic management, teacher training or work within the higher education sector. The 450 hour course in TESOL is ideal for those who want to further their skills and knowledge both for their own personal development and so that they can support and guide less experienced colleagues.</p>

					<div class="divider"></div>
					<h3>TESOL Masters Course Details</h3>
					<div class="tabs tabs-vertical" id="courses-tab">
					    <div class="row">
					        <div class="col-md-4">
					            <ul class="nav flex-column nav-tabs" id="course-faq" role="tablist" aria-orientation="vertical">
					                <li class="nav-item">
					                    <a class="nav-link active" href="#coursecontent" data-toggle="tab">Course Content</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#practicum" data-toggle="tab">Practicum</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#assessment" data-toggle="tab">Assessment</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#admissionrequirements" data-toggle="tab">Admission requirements</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#qualification" data-toggle="tab">Qualification</a>
					                </li>
					            </ul>
					        </div>
					        <div class="col-md-8">
					            <div class="tab-content">
					                <div class="tab-pane active" id="coursecontent">
					                    <ul class="list">
										    <li class="main-li check-arrow">Teachers and Learners</li>
										    <li class="main-li check-arrow">Teaching Methodology</li>
										    <li class="main-li check-arrow">Teaching Vocabulary</li>
										    <li class="main-li check-arrow">Teaching Grammar</li>
										    <li class="main-li check-arrow">Teaching Reading &amp; Listening</li>
										    <li class="main-li check-arrow">Teaching Writing &amp; Speaking</li>
										    <li class="main-li check-arrow">Teaching Pronunciation</li>
										    <li class="main-li check-arrow">Teaching Large Classes</li>
										    <li class="main-li check-arrow">Using Technology in the Classroom</li>
										    <li class="main-li check-arrow">Classroom Management</li>
										    <li class="main-li check-arrow">Corrective Feedback</li>
										    <li class="main-li check-arrow">Lesson Planning</li>
										    <li class="main-li check-arrow">Assessment</li>
										    <li class="main-li check-arrow">Teaching Young Learners</li>
										    <li class="main-li check-arrow">Learning English in the Secondary School</li>
										    <li class="main-li check-arrow">Teaching English to Adults</li>
										    <li class="main-li check-arrow">The English Department</li>
										    <li class="main-li check-arrow">Linguistics</li>
										    <li class="main-li check-arrow">ESP</li>
										    <li class="main-li check-arrow">Language Teacher Education</li>
										    <li class="main-li check-arrow">Materials Development &amp; Resources</li>
										    <li class="main-li check-arrow">Programme Design</li>
										</ul>

					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>

					                <div class="tab-pane" id="practicum">
					                    <p>Practicum is an essential component to a teacher’s training process, as it provides valuable insight and experience prior to obtaining independent teaching positions. Atheneum Global Teacher Training College requires that all students accumulate a minimum of 6 hours for TEFL/TESOL certification. These hours can be achieved through observation, tutoring, student teaching or a combination of 2 or 3 of these in an ESL/EFL setting in which non-native speakers are learning new English language skills.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="assessment">
					                    <p>Our three learning blocks enable us to mark you fairly, using your reflective diaries (10% of your final grade), your assignments (40% of your grade), and your teaching practice (50% of your grade). So regardless of your learning style you will be able to obtain a high score through hard work.
										</p>

					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="admissionrequirements">
					                    <p>Completion of Bachelor degree.</p>
										<p>One of the following if you are not an English native speaker</p>
										<ul class="list">
										    <li class="main-li check-arrow">Advanced level of English or</li>
										    <li class="main-li check-arrow">IELTS 6.5</li>
										</ul>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="qualification">
					                    <p>On completion of your course, you will receive a certificate in Teaching English to Speakers of Other Languages  fromAtheneum Global Teacher Training College UK.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					            </div>
					        </div>
					    </div>
					</div>
            	</div>

            	<div class="col-12 col-lg-3 hide-md-and-down" id="single-course-sidebar">
                    <h5>Related Courses</h5>
                    <ul class="list">
                    	<li class="sub-li">
                        	<div class="post-image">
                                <div class="d-block img-thumbnail img-thumbnail-no-borders">
                                    <img src="/assets/img/course/1.jpg" width="50" height="50" alt="">
                                </div>
                            </div>
                            <div class="post-info">
                                <a href="/tefl">TEFL Level 5 OFQUAL Program</a>
                            </div>
                        </li>
                        <li class="sub-li">
                        	<div class="post-image">
                                <div class="d-block img-thumbnail img-thumbnail-no-borders">
                                    <img src="/assets/img/course/1.jpg" width="50" height="50" alt="">
                                </div>
                            </div>
                            <div class="post-info">
                                <a href="/tesol-certification">TESOL CERTIFICATION</a>
                            </div>
                        </li>
                        <!-- <li class="sub-li">
                        	<div class="post-image">
                                <div class="d-block img-thumbnail img-thumbnail-no-borders">
                                    <img src="/assets/img/course/3.jpg" width="50" height="50" alt="">
                                </div>
                            </div>
                            <div class="post-info">
                                <a href="/tesol-masters">TESOL MASTERS </a>
                            </div>
                        </li> -->
                    </ul>
                    <?php include("_common-sidebar.php");?>
                </div>
        </div>
    </div>
</div>
