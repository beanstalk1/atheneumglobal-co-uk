<title>Apply nowfor bestteacher training coursesin UK</title>
<meta name="description" content=" Become a global educator with UK regulated teaching qualifications that deliver quality learning and adds value your resume. Begin your learning journey today.
">
<?php include("_menu.php");?>
<link rel="canonical" href="<?php echo $url; ?>" async/>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<style>
.breadcrub-style-3 .bg-img{
  background-image: url('/assets/img/study/apply_now.jpg');
}
.form-style-2 .form-control{
  -webkit-appearance: none;
    -webkit-border-radius: 0px;
}
.mainForm {
    max-width: 700px;
    margin: auto;
    width: 100%;
    box-shadow: 0 3px 1px -2px rgba(0,0,0,.2), 0 2px 2px 0 rgba(0,0,0,.14), 0 1px 5px 0 rgba(0,0,0,.12);
    padding:30px 40px;
    border-radius: 8px;
    margin-top: 50px;
}
.form_heading h2 {
    font-size: 27px;
    border-bottom: 1px solid #8080804f;
    padding-bottom: 27px;
}
.mainForm .main-btn {
    height: 50px;
    width: 180px;
    border-radius: 10px;
}
</style>
<!-- Breadcrumb -->
<div class="breadcrub breadcrub-style-3 section allcourse-title">
    <div class="bg-img d-flex flex-column justify-content-center">
<div class="overlay"></div>
        <div class="container">
            <div class="heading">
                <h1 class="page-heading">Apply Now</h1>
            </div>
        </div>
        <!-- <div class="overlay"></div> -->
    </div>
</div>
<section>
  <div class="container">
    <div class="row">
      <div class="col-12  message-from text-center">
        <div class="mainForm">
        <div class="form_heading">
          <h2>Apply Now</h2>
        </div>
          <form class="text-left form-style-2">
            <div class="row">
              <div class="col-12 col-md-6">
                <div class="form-group required">
                    <label for="username">Name</label>
                    <input type="text" id="username" class="form-control" name="username" placeholder="Enter Full Name" />
                </div>
              </div>
              <div class="col-12 col-md-6">
                <div class="form-group required">
                    <label for="email">Email address</label>
                    <input type="email" id="email" class="form-control" name="email" placeholder="Put an valid Email ID" />
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <div class="form-group required">
                    <label for="phone">Phone Number</label>
                    <input type="number" id="phone" class="form-control" name="phone" placeholder="Put an valid phone number" />
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-12 col-md-6">
                <div class="form-group required">
                    <label for="sel">Program Type</label>
                    <select class="form-control" id="sel">
                     <option>Certification Course</option>
                     <option>Graduate Course</option>
                     <option>Post Graduate Course</option>
                   </select>
                </div>
              </div>
              <div class="col-12 col-md-6">
                <div class="form-group required">
                    <label for="sel1">Program Name</label>
                    <select class="form-control" id="sel1">
                     <option>Montessori Studies</option>
                     <option>Pre & Primary Education</option>
                     <option>Early Childhood Education & Care</option>
                     <option>Nursery Teacher Training</option>
                     <option>School Organization : Administration and Management</option>
                    </select>
                  </div>
              </div>
            </div>
            <div class="row">
              <div class="col-12 col-md-6">
                <div class="form-group required">
                    <label for="state">State</label>
                    <select class="form-control" id="state">
                     <option>Punjab</option>
                     <option>UP</option>
                     <option>Chandiagrh</option>
                   </select>
                </div>
              </div>
              <div class="col-12 col-md-6">
                <div class="form-group required">
                    <label for="country">Country</label>
                    <select class="form-control" id="country">
                     <option>India</option>
                     <option>America</option>
                     <option>Pakistan</option>
                     <option>China</option>
                     <option>Japan</option>
                    </select>
                  </div>
              </div>
            </div>
            <div class="row">
              <div class="col-12 col-md-6">
                <div class="form-group required">
                    <label for="city">City</label>
                    <input type="number" id="postalcode" class="form-control" name="city" placeholder="Put an valid city"/>
                </div>
              </div>
              <div class="col-12 col-md-6">
                <div class="form-group required">
                    <label for="postalcode">Postal Code</label>
                    <input type="number" id="postalcode" class="form-control" name="postalcode" placeholder="Put an valid postal code"/>
                </div>
              </div>
            </div>
          </form>
          <div class="main-btn contunue2">Submit</div>
        </div>
      </div>
      </div>
    </div>
</section>
