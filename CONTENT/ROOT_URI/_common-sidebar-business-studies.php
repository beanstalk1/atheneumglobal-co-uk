<ul class="list">
    <li class="sub-li mt-2">
      <div class="post-image">
            <div class="d-block img-thumbnail img-thumbnail-no-borders">
                <img src="/assets/img/course/1.jpg" width="50" height="50" alt="">
            </div>
        </div>
        <div class="post-info">
            <a href="/ofqual-level-3-diploma-in-business-administration">Ofqual Level 3 Diploma in Business Administration</a>
        </div>
    </li>
    <li class="sub-li">
      <div class="post-image">
            <div class="d-block img-thumbnail img-thumbnail-no-borders">
                <img src="/assets/img/course/3.jpg" width="50" height="50" alt="">
            </div>
        </div>
        <div class="post-info">
            <a href="/ofqual-level-5-diploma-in-management-and-leadership">Ofqual Level 5 Diploma in Management and Leadership</a>
        </div>
    </li>
    <li class="sub-li">
      <div class="post-image">
            <div class="d-block img-thumbnail img-thumbnail-no-borders">
                <img src="/assets/img/course/3.jpg" width="50" height="50" alt="">
            </div>
        </div>
        <div class="post-info">
            <a href="/diploma-in-business-administration">Diploma in Business Administration </a>
        </div>
    </li>
    <li class="sub-li">
      <div class="post-image">
            <div class="d-block img-thumbnail img-thumbnail-no-borders">
                <img src="/assets/img/course/3.jpg" width="50" height="50" alt="">
            </div>
        </div>
        <div class="post-info">
            <a href="/tourism-management">Tourism Management</a>
        </div>
    </li>
</ul>
