<title>About Atheneum Global Teacher Training College</title>
<meta name="keywords" content="Online Teacher Training Course">
<meta name="description" content="Global exposure of our training courses for teachers. Apply to begin your teaching career with the role of a dream teacher in UK.">
<?php include("_menu.php");?>
<link rel="canonical" href="<?php echo $url; ?>" async/>
<style>
 .breadcrub-style-3 .bg-img{
   background-image: url('/assets/img/slider/slider5.jpg');
 }
 .degree span {
     font-size: 40px;
     color: #fff;
 }
 .degree .btn-primary {
     background-color: #811b18 !important;
     border: transparent;
 }
 .degree {
     width: 100%;
     background: url(./assets/img/study/level3_final.jpeg);
     background-size: cover;
     background-position: bottom center;
     position: relative;
     background-attachment: fixed;
     padding: 60px 0;
 }
 .degree strong{
   color:#fff;
 }
</style>
<!-- Breadcrumb -->
<div class="breadcrub breadcrub-style-3 section allcourse-title">
    <div class="bg-img d-flex flex-column justify-content-center">
<div class="overlay"></div>
        <div class="container">
            <div class="heading">
                <h1 class="page-heading">FAQ's</h1>
            </div>
        </div>
        <!-- <div class="overlay"></div> -->
    </div>
</div>
<div class="container pb-5">
  <h2 class="pt-2 pb-4">General FAQ's for TEFL course</h2>
  <div class="row">
    <div class="col-12">
          <div class="accordion-wrapper">
              <div id="accordion">
                  <div class="card">
                      <div class="card-header" id="headingOne">
                          <h5 class="mb-0">
                              <a role="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                  What is a level 5 qualification?
                              </a>
                          </h5>
                      </div>
                      <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                          <div class="card-body">
                              <p>What is a level 5 qualification?<br />When you hear of "levels" this relates to the qualifications system of the UK government. This system helps learners studying governed qualifications from the UK government to understand where their qualification sits compared to other qualifications. A regulated qualification level 5 means Ofqual (Government of the United Kingdom) has accepted the qualification to be at a recognized standard of achievement. Our Level 5 TEFL Certificate (168hrs) is the same standard as the following certificates:</p>
                              <ul>
                              <li>A higher education diploma (DipHE),</li>
                              <li>A&nbsp;foundation degree, and</li>
                              <li>Certificates CELTA and Trinity CertTESOL.</li>
                              </ul>
                          </div>
                      </div>
                  </div>
                  <div class="card">
                      <div class="card-header" id="headingTwo">
                          <h5 class="mb-0">
                              <a class="collapsed" role="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                  What do you mean by ‘regulated qualification’?
                              </a>
                          </h5>
                      </div>
                      <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                          <div class="card-body">
                              <p>A regulated qualification means that it is officially recognised by government and sits on the Ofqual Register of Regulated Qualifications. You can view our qualification on the Ofqual register.</p>
                              <p><a href="https://register.ofqual.gov.uk/Detail/Index/42412?category=qualifications">https://register.ofqual.gov.uk/Detail/Index/42412?category=qualifications</a></p>
                          </div>
                      </div>
                  </div>
                  <div class="card">
                      <div class="card-header" id="headingThree">
                          <h5 class="mb-0">
                              <a class="collapsed" role="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                  Why does other course providers provide so many course options and ‘levels’?
                              </a>
                          </h5>
                      </div>
                      <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                          <div class="card-body">
                              <p>The response is simple: we just don't know! Recruiters who deliver the best prospects with the most affordable packages are now eligible for <strong>TEFL certification at level 5</strong>. All regulated level 5 courses have a length of 1<strong>50hrs +</strong>. We have never seen a single employer request TEFL training for more than <strong>168hrs</strong>.</p>
                          </div>
                      </div>
                  </div>
                  <div class="card">
                      <div class="card-header" id="headingFour">
                          <h5 class="mb-0">
                              <a class="collapsed" role="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                  Why choose a regulated qualification rather than a cheaper groupon option?
                              </a>
                          </h5>
                      </div>
                      <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                          <div class="card-body">
                              <p>Choosing a regulated credential to study is your quality guarantee. Course providers providing regulated certificates must comply with very strict rules and procedures about quality assurance. This accounts for:</p>
                              <ul>
                              <li>The staff who support and assist students are adequately trained</li>
                              <li>The course provider has the means to deliver the qualification effectively</li>
                              <li>The course itself teaches and tests the qualifications aims effectively</li>
                              <li>The course contents were written and drawn up by industry experts and peer-reviewed</li>
                              <li>Learner feedback and grades are decent, moderate and meet modern academic expectations</li>
                              <li>The qualification will also be accepted globally by schools, employers, foreign embassies, and governments of most other countries, officially controlled by the UK government.<br />&nbsp;</li>
                              </ul>
                          </div>
                      </div>
                  </div>
                  <div class="card">
                      <div class="card-header" id="headingFive">
                          <h5 class="mb-0">
                              <a class="collapsed" role="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                  How to determine if a qualification is regulated?
                              </a>
                          </h5>
                      </div>
                      <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                          <div class="card-body">
                              <p>Any qualification that is officially recognized by the UK government will sit on the Ofqual Register of Regulated Qualifications. Make sure to ask your course provider to provide a link to the register for their qualification.</p>
                              <p>You can view our qualification on the Ofqual Register of Regulated Qualifications here.</p>
                              <p><a href="https://register.ofqual.gov.uk/Detail/Index/42412?category=qualifications">https://register.ofqual.gov.uk/Detail/Index/42412?category=qualifications</a></p>
                          </div>
                      </div>
                  </div>
                  <div class="card">
                      <div class="card-header" id="headingSix">
                          <h5 class="mb-0">
                              <a class="collapsed" role="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                  Is there any difference between TEFL and TESOL?
                              </a>
                          </h5>
                      </div>
                      <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
                          <div class="card-body">
                              <p>TEFL and TESOL are both acronyms for teaching English to non-native English speakers. Both acronyms can be used to describe qualifications that certify you to teach English as a foreign language. To a large extent, "TEFL" and "TESOL" are used interchangeably. However, employers may have certain internal policies favoring one qualification over the other. It is always good to inquire about specific qualification requirements of employers before enrolling in either of the courses.</p>
                              <p><strong>TEFL -</strong> Teaching English as a Foreign Language</p>
                              <p><strong>TESOL -</strong> Teaching English to Speakers of Other Languages</p>
                          </div>
                      </div>
                  </div>
                  <div class="card">
                      <div class="card-header" id="headingSeven">
                          <h5 class="mb-0">
                              <a class="collapsed" role="button" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                  WHow to determine if a qualification is regulated?
                              </a>
                          </h5>
                      </div>
                      <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
                          <div class="card-body">
                              <p>Isn&rsquo;t it better for learners to just enroll in a Groupon or Amazon course for TEFL?<br />It doesn't come cheap to offer a high-quality TEFL course that provides qualified tutors to students and leads to a government-regulated qualification! Course providers who sell on regular sale platforms have to massively discount their course fees and then split what they make 50/50 with the sale marketplace like Groupon, Wowcher&amp; Amazon.</p>
                              <p>It is unlikely for TEFL course providers to sell on regular deal websites with such low revenues to:</p>
                              <ul>
                              <li>Provide continuous assistance from experienced &amp; trained tutors</li>
                              <li>Have fair and moderated marking/classification systems</li>
                              <li>Create and upgrade quality course content &amp; services on a continuous basis</li>
                              <li>Offer a course leading to regulated government qualification</li>
                              <li>Offer career assistance and continuing support upon completion of the course</li>
                              </ul>
                              <p>TEFL courses offered on day-to-day deal websites are unregulated, unaccredited, lack academic legitimacy and employers do not value them.</p>
                              <p>We know it's really frustrating to navigate your way through the TEFL courses page. And we give just one course, exactly what you need to get the best jobs in the world!</p>
                          </div>
                      </div>
                  </div>
                  <div class="card">
                      <div class="card-header" id="headingEight">
                          <h5 class="mb-0">
                              <a class="collapsed" role="button" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                  Is a Level 5 course always a level 5 course?
                              </a>
                          </h5>
                      </div>
                      <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
                          <div class="card-body">
                              <p>&nbsp;You can see online advertised cheap TEFL courses claiming to be a "level," such as "Level 5 140-Hour TEFL Course" (often advertised on daily deal websites), these courses are sadly not the level they claim to be and are highly deceptive.</p>
                              <p>A course-level informs learners where the qualification falls on the Regulated Qualifications System (RQF) for the UK. If a course is not listed in the Regulated Qualifications Registry, then it is not really a level at all. Our certification can be found on the List here.</p>
                              <p>Make sure you ask every TEFL training provider that claims to have a link to the Register of Regulated Qualifications to provide a "degree" of the TEFL course. If they are unable to do this then the credential is not the standard they claim to be international embassies and employers will not recognize it.</p>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
        </div>
      </div>
    </div>
    <section class="degree">
       <div class="container">
         <div class="row">
           <div class="col-auto mx-auto text-center my-auto">
              <span class="pb-4" style="display:block;">Get <strong>Yourself</strong> <strong>Enrolled</strong></span>
              <a href="/apply-now" class="btn btn-primary btn-lg" style="margin:2rem 0px;">Apply Now</a>
            </div>
         </div>
       </div>
    </section>
