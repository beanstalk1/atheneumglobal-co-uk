<title>Certificate in Nursery Teacher Training Course in UK</title>
<meta name="keywords" content="Certificate in ntt course, nursery teacher training course online">
<meta name="description" content="The NTT course certificate provides aspiring teachers global qualifications to begin their teaching careers.">
<?php include("_menu.php");?>
<link rel="canonical" href="<?php echo $url; ?>" async/>
<style>
/* .allcourse-title .page-heading {
  font-size: 30px !important;
  padding-bottom: 14px;
} */
.page-heading~p {
    font-size: 16px;
}
p,li{
  font-size: 15px !important;
}
h4{
  color:#777;
}
.breadcrub-style-3 .bg-img{
  background-image: url('/assets/img/study/certificate_ntt.jpg');
}
</style>
<!-- Breadcrumb -->
  <div class="breadcrub breadcrub-style-3 section allcourse-title">
    <div class="bg-img d-flex flex-column justify-content-center">
<div class="overlay"></div>
      <div class="container">
        <div class="heading">
          <h1 class="page-heading">Certificate in Nursery Teacher Training</h1>
        </div>
      </div>
      <!-- <div class="overlay"></div> -->
    </div>
  </div>

<!-- Course Detail -->
<div class="course-detail section" id="single-coursepage">
    <div class="container">
        <div class="row">
            	<div class="col-12 col-lg-9 content">
                <h2 class="h4 pb-2">Certificate in Nursery Teacher Training</h2>
               <p>Atheneum Global’s Certificate in Nursery Teacher Training emphasizes the holistic approaches and methods to teaching young children which involve children's physical, emotional and social developments while emphasizing on their cognitive learning as well. Teaching young children is considered challenging and the focus is on learning through play by providing an interactive learning environment. The certificate course acquaints the future nursery teachers with the methodologies of teaching learners at the nursery level with ease and competence.</p>
					<div class="divider"></div>
					<h3 class="text-center pt-1 pb-2">NTT Certificate Course Details</h3>
					<div class="tabs tabs-vertical" id="courses-tab">
					    <div class="row">
					        <div class="col-md-4">
					            <ul class="nav flex-column nav-tabs" id="course-faq" role="tablist" aria-orientation="vertical">
					                <li class="nav-item">
					                    <a class="nav-link active" href="#courseduartion" data-toggle="tab">COURSE DURATION</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#eligibility" data-toggle="tab">ELIGIBILITY</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#coursefee" data-toggle="tab">COURSE FEE</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#curriculum" data-toggle="tab">CURRICULUM</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#diploma" data-toggle="tab">DIPLOMA</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#enrollment" data-toggle="tab">ENROLLMENT PROCESS</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#placements" data-toggle="tab">PLACEMENTS</a>
					                </li>
					            </ul>
					        </div>
					        <div class="col-md-8">
					            <div class="tab-content">
					                <div class="tab-pane active" id="courseduartion">
					                    <ul class="list">
					                        <li class="main-li check-arrow">Maximum duration of the course is 4 months.</li>
					                        <li class="main-li check-arrow">It is a flexible program that can be pursued from any corner of the globe.</li>
					                        <li class="main-li check-arrow">Self-paced.</li>
					                        <li class="main-li check-arrow">Fast track mode permits early completion</li>
					                    </ul>
                              <p>Average course completion time varies between 2-3 months depending on the number of hours per week that you devote to this course. The time spent though, is well accounted for as it equips teachers with the essential knowledge base to further his or her career in this field with confidence.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="eligibility">
					                    <ul class="list">
					                        <li class="main-li check-arrow">A High School Degree is the minimum requirement for this course.</li>
					                        <li class="main-li check-arrow">Aspiring and existing teachers are free to apply.</li>
					                    </ul>
                              <p>The minimum eligibility requirement for the Certificate course in Nursery Teacher Training is a high school certificate. After you’ve enrolled yourself into the course, you’d have to submit your documentation for our records.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="coursefee">
					                    <ul class="list">
                                <li class="main-li check-arrow">Program is available in online mode</li>
                                <li class="main-li check-arrow">Very reasonably priced with installment options available</li>
                                <li class="main-li check-arrow">Scholarships available for meritorious students</li>
                              </ul>
                              <!-- <p>The Certificate in Nursery Education is priced very affordably at Rs 9,500 for Indian citizens and $ 150 for international students. The course is administered completely online and hence extremely conveniently administered for students pursuing existing careers or those who require the flexibility of self paced online education. With a Graduation from Atheneum Global Teacher Training College, be best placed for a global teaching career with a very affordable investment. -->
                              The Certificate in Nursery Education is priced very affordably at $150. The course is administered completely online and hence extremely conveniently administered for students pursuing existing careers or those who require the flexibility of self paced online education. With a Graduation from Atheneum Global Teacher Training College, be best placed for a global teaching career with a very affordable investment.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="curriculum">
					                    <p>We offer a concise Certificate Program in Nursery Teacher Training for those wishing to work with, or be in charge of Nursery house of children in pre-primary education space. The course comprises of core Nursery concepts. The Certificate is awarded on satisfactory completion of the course.</p>
                              <p><strong>Theory : 4 modules</strong></p>
                              <p>The break up of these modules is as below –</p>
                              <p><strong>THEORY</strong></p>
					                    <ul class="list">
					                        <li class="main-li check-arrow">Life History Of Maria Nursery</li>
					                        <li class="main-li check-arrow">NURSERY METHOD AND HISTORY</li>
					                        <li class="main-li check-arrow">Exercises of Practical Life</li>
					                        <li class="main-li check-arrow">Sensory Education in Nursery</li>
					                    </ul>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="diploma">
                            <!-- <p>After the completion of the Post Graduate Diploma course, candidates are awarded a Certificate by Atheneum Global Teacher Training College. The Certificate awarded has an added advantage as the word ‘Online’ is not mentioned in the certificates. Shipment of certificates within India is done free of cost but the candidates residing outside India are required to pay an additional <strong>US $ 25 (South East Asian & Middle Eastern Countries) and US $ 35</strong> (rest of the world) as certificate dispatch fee. Generally after submission of all the assignments and completion of the course, you have to pay the dispatch fee. After that we take your shipping address and contact number and courier the certificate to the same. It takes a maximum of 3 weeks for the certificates to reach the candidate’s address.</p> -->
                            <p>After the completion of the Post Graduate Diploma course, candidates are awarded a Certificate by Atheneum Global Teacher Training College. The Certificate awarded has an added advantage as the word ‘Online’ is not mentioned in the certificates. Shipment of certificates within India is done free of cost but the candidates residing outside India are required to pay an standard fee of <strong>US$35</strong> (rest of the world) as certificate dispatch fee. Generally after submission of all the assignments and completion of the course, you have to pay the dispatch fee. After that we take your shipping address and contact number and courier the certificate to the same. It takes a maximum of 3 weeks for the certificates to reach the candidate’s address.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="enrollment">
					                    <ul class="list">
					                        <li class="main-li check-arrow">Simplest enrollment process.</li>
					                        <li class="main-li check-arrow">Application and payment online.</li>
					                    </ul>
					                    <p>All our course requirements are clarified on our website, FAQs and Blog posts. You can also give us a call to help you with the onboarding process or to help clarify some aspects of the course.</p>
                              <p>Once you’ve made the online payment, you have to wait between 24-48 hours before we can enable your student account and get you started on your journey to academic success.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
                          <div class="tab-pane" id="academicsupport">
					                    <ul>
					                        <li>Personalized tutor support.</li>
					                        <li>Tons of reference material and videos</li>
					                        <li>Self-Paced Program at a mouse click away from you.</li>
					                    </ul>
					                    <p>
Trainees enrolled into the program would be personally tutored by our experienced faculties at Atheneum Global Teacher Training College. Regular doubt clearing sessions and other academic support is extended as part of this personalized tutoring. The learning is at your own pace and the support provided is completely personalized and carries high level of expertise.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="placements">
					                    <p>
					                       Atheneum Global Teacher Training College’s industry linkages and international credentials lends credibility to your candidature. Every candidate is issued a personalized letter to help him or her land up with an internship of their choice. This internship is an important step towards finishing your course as well as to start your journey as an educator.
					                    </p>
                              <p>In addition to this, candidates are also given privileged access to Jobs For Teachers, a national portal for teachers for all kinds of teaching assignments. In essence, what we wish to communicate is that Atheneum is committed to offer you the very best education and the very best access to teaching opportunities.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					            </div>
					        </div>
					    </div>
					</div>
            	</div>

            	<div class="col-12 col-lg-3 hide-md-and-down" id="single-course-sidebar">
                    <h5 class="pt-4">Related Courses</h5>
                    <ul class="list">
                        <li class="sub-li">
                        	<div class="post-image">
                                <div class="d-block img-thumbnail img-thumbnail-no-borders">
                                    <img src="/assets/img/course/1.jpg" width="50" height="50" alt="">
                                </div>
                            </div>
                            <div class="post-info">
                                <a href="/graduate-in-nursery-teacher-training">Graduate in Nursery Teacher training</a>
                            </div>
                        </li>
                        <li class="sub-li">
                        	<div class="post-image">
                                <div class="d-block img-thumbnail img-thumbnail-no-borders">
                                    <img src="/assets/img/course/3.jpg" width="50" height="50" alt="">
                                </div>
                            </div>
                            <div class="post-info">
                                <a href="/global-qualification-in-nursery-teacher-training">Post Graduate in Nursery Teacher training</a>
                            </div>
                        </li>
                    </ul>
                    <?php include("_common-sidebar.php");?>
                </div>
        </div>
    </div>
</div>
