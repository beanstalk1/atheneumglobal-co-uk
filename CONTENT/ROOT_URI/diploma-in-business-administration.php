<title>Diploma in Business Administration by Atheneum</title>
<meta name="keywords" content="Diploma in Business Administration">
<meta name="description" content="Atheneum offers internationally recognized diploma certificate in business administration for those who want to further their career in a management position.">
<?php include("_menu.php");?>
<link rel="canonical" href="<?php echo $url; ?>" async/>
<style>
.qualificationlist {
    display: flex;
    flex-wrap: wrap;
}
.qualificationlist li {
   width: 25%;
   padding: 12px 5px;
}
section{
  padding: 50px 0px;
}
.breadcrub-style-3 .bg-img{
  background-image: url('/assets/img/study/business_studies.jpg');
}
</style>

<!-- Breadcrumb -->
<div class="breadcrub breadcrub-style-3 section allcourse-title" style="margin-bottom:0px !important;">
    <div class="bg-img d-flex flex-column justify-content-center">
<div class="overlay"></div>
        <div class="container">
            <div class="heading">
                <h1 class="page-heading">Diploma in Business Administration</h1>
            </div>
        </div>
        <!-- <div class="overlay"></div> -->
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-12 col-lg-9 content">
<section>
  <div class="container">
    <div class="row">
      <div class="col-12">
        <h2 class="text-center pb-3">Diploma in Business Administration</h2>
        <p>Our comprehensive business management course can be done in three ways: interactive, variational or studying in classrooms. This course offers you intensive preparation for management studying with realistic knowledge of the largest organizations as well as online units that can differentiate the c.v.
Built specifically for the global online learner, the MBA draws on insights gained from more than a thousand global conversations held in ten regions of the world through the Business Education Jam. Each of the leading employers and academics engaged in informing a unique curriculum centered on the themes that drive business in the 21st century. The program also benefits from input and content from high-profile business alumni and partners worldwide.</p>
<p>Benefit from our outstanding reputation for thought leadership and be challenged by a host of different experiences. Enjoy a curriculum that can be tailored specifically to support your career path and choose whether to finish the program in 15, 18 or 21 months. Immerse yourself in collaborative group work, international exchanges, internships and a wide variety of options. Above all, learn to thrive in today's complex business environment and emerge as a well-rounded global leader, someone capable of acting and succeeding anywhere in the world.</p>
      </div>
    </div>
  </div>
<section>
<section style="background-color:#f7f7f7">
  <div class="container">
    <h2 class="text-center h2 pb-3">Course content for Diploma in Business Administration</h3>
    <div class="row">
      <div class="col-12">
                  <div class="accordion-wrapper">
                      <div id="accordion">
                          <div class="card">
                              <div class="card-header" id="headingOne">
                                  <h5 class="mb-0">
                                      <a role="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                          Theories:
                                      </a>
                                  </h5>
                              </div>
                              <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                  <div class="card-body">
                                      <ul>
                                      <li>Organising communication commercial letters</li>
                                      <li>Essential communications and strategic positions for meetings</li>
                                      <li>Incoterms and commercial usages</li>
                                      <li>Organising proforma invoice</li>
                                      <li>Currency supply and payments</li>
                                      <li>Enterprising an international business</li>
                                      <li></li>
                                      </ul>
                                  </div>
                              </div>
                          </div>
                          <div class="card">
                              <div class="card-header" id="headingTwo">
                                  <h5 class="mb-0">
                                      <a class="collapsed" role="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Practical:
                                      </a>
                                  </h5>
                              </div>
                              <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                  <div class="card-body">
                                    <p class="h6">Part 1:</p>
                                    <ul>
                                    <li>Determining strategy and business plan</li>
                                    <li>Designing SWOT</li>
                                    <li>Core competencies</li>
                                    <li>Business strategy simulation games</li>
                                    <li>Management consulting</li>
                                    <li>BCG growth matrics</li>
                                    </ul>
                                    <p class="h6">Part 2 :assignments</p>
                                    <p>This part contains the tasks for the theoretical components. In this segment, all participants are divided into 5 different classes, who become a company leader and employee based on their management experience and language level.</p>
                                    <ul>
                                    <li>Make an strategy for a company</li>
                                    <li>Have some negotiations</li>
                                    <li>Provide SWOT</li>
                                    <li>Make a plan</li>
                                    <li>Do international negotiations based on letters and telephone conversations</li>
                                    <li>Make an agreement</li>
                                    </ul>
                                  </div>
                              </div>
                          </div>
                          <div class="card">
                              <div class="card-header" id="headingThree">
                                  <h5 class="mb-0">
                                      <a class="collapsed" role="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                       Assessment:
                                      </a>
                                  </h5>
                              </div>
                              <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                  <div class="card-body">
                                    <ul>
                                    <li>50% participation</li>
                                    <li>50% presenting a practical project</li>
                                    </ul>
                              </div>
                          </div>
                        </div>
                          <div class="card">
                              <div class="card-header" id="headingFour">
                                  <h5 class="mb-0">
                                      <a class="collapsed" role="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                         Admission:
                                       </a>
                                  </h5>
                              </div>
                              <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                                  <div class="card-body">
                                      <ul>
                                      <li>Completion of high school</li>
                                      <li>High intermediate level of English</li>
                                      </ul>
                                  </div>
                              </div>
                          </div>
                          <div class="card">
                              <div class="card-header" id="headingFive">
                                  <h5 class="mb-0">
                                      <a class="collapsed" role="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                          Qualification:
                                      </a>
                                  </h5>
                              </div>
                              <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                                  <div class="card-body">
                                      <p>On completion of your course, you will receive a certificate in Business Administration  from Atheneum Global Teacher Training College.</p>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
      </div>
    </div>
</section>
</div>
<div class="col-12 col-lg-3 hide-md-and-down" id="single-course-sidebar">
      <h5 class="pt-4">Related Courses</h5>
      <?php include("_common-sidebar-business-studies.php")                ?>
      <?php include("_common-sidebar.php");?>
  </div>
</div>
</div>
<!-- </div> -->
