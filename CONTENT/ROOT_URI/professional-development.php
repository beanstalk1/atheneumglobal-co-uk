<title>Career Studies courses offered by Atheneum</title>
<meta name="keywords" content="Career Studies courses offered by Atheneum">
<meta name="description" content="Get ahead professionally in early childhood education with these carefully curated and internationally accredited online and in-class teacher training courses. ">
<?php include("_menu.php");?>
<link rel="canonical" href="<?php echo $url; ?>" async/>
<style>
 .breadcrub-style-3 .bg-img{
   background-image: url('/assets/img/study/professional.jpg');
 }
 </style>
<!-- Breadcrumb -->
  <div class="breadcrub breadcrub-style-3 section allcourse-title">
    <div class="bg-img d-flex flex-column justify-content-center">
<div class="overlay"></div>
      <div class="container">
        <div class="heading">
          <h1 class="page-heading">Career Studies</h1>
        </div>
      </div>
      <!-- <div class="overlay"></div> -->
    </div>
  </div>

<!-- Course Detail -->
<div class="course-detail section" id="main-coursepage">
    <div class="container">
        <div class="col-12 col-xl-12">
            <div class="row">
                <div class="col-12 col-lg-9 content">
                	<h3>Our different Career Studies Programs</h3>
                    <div class="row" id="different-certificate">
                      <div class="col-12 col-md-4">
                        <div class="course-card-style-2">
                          <div class="card">
                            <img class="card-img-top" src="/assets/img/course/2.jpg" alt="Montessori Teacher Training">
                            <div class="card-img-overlay">
                              <div class="card-body">
                                <h5>Montessori Teacher Training</h5>
                                <!-- <p class="details">Certificate course in Montessori Teacher Training</p> -->
                                <div class="more">
                                  <a href="/montessori-teacher-training" class="small-btn">View Course</a>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-md-4">
                        <div class="course-card-style-2">
                          <div class="card">
                            <img class="card-img-top" src="assets/img/course/4.jpg" alt="Graduate course in Montessori Teacher Training">
                            <div class="card-img-overlay">
                              <div class="card-body">
                                <h5>Pre-Primary Teacher Training</h5>
                                <!-- <p class="details">Graduate Course in Montessori Teacher Training</p> -->
                                <div class="more">
                                  <a href="/pre-primary-teacher-training" class="small-btn">View Course</a>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-md-4">
                        <div class="course-card-style-2">
                          <div class="card">
                            <img class="card-img-top" src="assets/img/course/8.jpg" alt="arly Childhood Education and Care">
                            <div class="card-img-overlay">
                              <div class="card-body">
                                <h5>Early Childhood Education and Care</h5>
                                <!-- <p class="details">Graduate Course in Montessori Teacher Training</p> -->
                                <div class="more">
                                  <a href="/early-childhood-education-and-care" class="small-btn">View Course</a>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                  	</div>
                  	<div class="row" id="">
                      <div class="col-12 col-md-4">
                        <div class="course-card-style-2">
                          <div class="card">
                            <img class="card-img-top" src="/assets/img/course/7.jpg" alt="Nursery Teacher Training">
                            <div class="card-img-overlay">
                              <div class="card-body">
                                <h5>Nursery Teacher Training</h5>
                                <!-- <p class="details">Certificate course in Montessori Teacher Training</p> -->
                                <div class="more">
                                  <a href="/nursery-teacher-training" class="small-btn">View Course</a>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-md-4">
                        <div class="course-card-style-2">
                          <div class="card">
                            <img class="card-img-top" src="/assets/img/course/11.jpg" alt="Education Management">
                            <div class="card-img-overlay">
                              <div class="card-body">
                                <h5>Education Management</h5>
                                <!-- <p class="details">Certificate course in Montessori Teacher Training</p> -->
                                <div class="more">
                                  <a href="/education-management" class="small-btn">View Course</a>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-12 col-md-4">
                        <div class="course-card-style-2">
                          <div class="card">
                            <img class="card-img-top" src="/assets/img/course/6.jpg" alt="Child Development Associate">
                            <div class="card-img-overlay">
                              <div class="card-body">
                                <h5>Child Development Associate</h5>
                                <!-- <p class="details">Certificate course in Montessori Teacher Training</p> -->
                                <div class="more">
                                  <a href="/child-development-associate" class="small-btn">View Course</a>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
                <div class="col-12 col-lg-3 hide-md-and-down" id="all-sidebar">
                    <?php include("_common-sidebar.php");?>
                </div>
            </div>
        </div>
    </div>
</div>
