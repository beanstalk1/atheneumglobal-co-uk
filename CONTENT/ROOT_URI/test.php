<title>Atheneum Global Teacher Training College UK</title>
<meta name="description" content="">
<?php include("_menu.php");?>
<link rel="canonical" href="<?php echo $url; ?>" async/>
<style type="text/css">
	#courses-tab .nav-item a.active, #courses-tab .nav-item a.nav-link:active, #courses-tab .nav-item a.nav-link:focus{
		background: var(--light-brown-1) !important;
		border-right: 2px solid var(--dark-brown-1);
	}
</style>


<section class="mt-5">
	<div class="container">
		<div class="tabs tabs-vertical" id="courses-tab">
			<div class="row">
				<div class="col-md-3">
					<ul class="nav flex-column nav-tabs" id="course-faq" role="tablist" aria-orientation="vertical">
						<li class="nav-item">
							<a class="nav-link active" href="#courseduartion" data-toggle="tab">COURSE DURATION</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#eligibility" data-toggle="tab">ELIGIBILITY</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#coursefee" data-toggle="tab">COURSE FEE</a>
						</li>
						<li class="nav-item">
						    <a class="nav-link" href="#curriculum" data-toggle="tab">CURRICULUM</a>
						</li>
						<li class="nav-item">
						    <a class="nav-link" href="#diploma" data-toggle="tab">DIPLOMA</a>
						</li>
						<li class="nav-item">
						    <a class="nav-link" href="#enrollment" data-toggle="tab">ENROLLMENT PROCESS</a>
						</li>
						<li class="nav-item">
						    <a class="nav-link" href="#academicsupport" data-toggle="tab">ACADEMIC SUPPORT</a>
						</li>
						<li class="nav-item">
						    <a class="nav-link" href="#placements" data-toggle="tab">PLACEMENTS</a>
						</li>
					</ul>
				</div>
				<div class="col-md-9">
					<div class="tab-content">
						<div class="tab-pane active" id="courseduartion">
							<ul>
							    <li>Maximum duration of the course is 4 months.</li>
							    <li>It is a flexible program that can be pursued from any corner of the globe.</li>
							    <li>Self-paced.</li>
							    <li>Fast track mode permits early completion</li>
							</ul>
							<p>
							    Average course completion time varies between 4-6 months depending on the number of hours per week that you devote to this course. The time spent though, is well accounted for as it equips teachers with the essential knowledge base to
							    further his or her career in this field with confidence.
							</p>
							<div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
						</div>
						<div class="tab-pane" id="eligibility">
						    <ul>
						        <li>A High School Degree is the minimum requirement for this course.</li>
						        <li>Aspiring and existing teachers are free to apply.</li>
						    </ul>
						    <p>
						        The minimum eligibility requirement for the Certificate in Montessori Education is a high school degree from any recognized institute. After you’ve enrolled yourself into the course, you’d have to submit your documentation for our
						        records.
						    </p>
						    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
						</div>
						<div class="tab-pane" id="coursefee">
						    <ul>
						        <li>Program is available in online mode</li>
						        <li>Very reasonably priced with installment options available</li>
						        <li>Scholarships available for meritorious students</li>
						    </ul>
						    <p>
						        The Certificate in Montessori Education is priced very affordably at Rs 9,500 for Indian citizens. The course is administered completely online and hence extremely conveniently administered for students pursuing existing careers or
						        those who require the flexibility of self-paced online education. With a Graduation from Atheneum Global Teacher Training College, be best placed for a global teaching career with a very affordable investment in Bangalore.
						    </p>
						    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
						</div>
						<div class="tab-pane" id="curriculum">
						    <p>
						        We offer a concise Certificate Program in Montessori Teacher Training in Bangalore for those wishing to work with or be in charge of the Montessori house of children in pre-primary education space. The course comprises of core
						        Montessori concepts. The Certificate is awarded on satisfactory completion of the course.
						    </p>
						    <p><strong>Theory: 4 modules</strong></p>
						    <p>The break up of these modules is as below &ndash;</p>
						    <ul>
						        <li>THEORY</li>
						        <li>Life History Of Maria Montessori</li>
						        <li>MONTESSORI METHOD AND HISTORY</li>
						        <li>Exercises of Practical Life</li>
						        <li>Sensory Education in Montessori</li>
						    </ul>
						    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
						</div>
						<div class="tab-pane" id="diploma">
						    <p>
						        After the completion of the Post Graduate Diploma course, candidates are awarded a Certificate by Atheneum Global Teacher Training College. The Certificate awarded has an added advantage as the word &lsquo;Online&rsquo; is not
						        mentioned in the certificates. Shipment of certificates <strong>within India is done free of cost</strong> but the candidates residing outside India are required to pay an additional
						        <strong>US $ 25 (South East Asian &amp; Middle Eastern Countries)</strong> and the<strong> US $ 35 (rest of the world)</strong> as certificate dispatch fee.
						    </p>
						    <p>
						        Generally after submission of all the assignments and completion of the course, you have to pay the dispatch fee. After that, we take your shipping address and contact number and courier the certificate to the same. It takes a
						        maximum of 3 weeks for the certificates to reach the candidate&rsquo;s address in Bangalore
						    </p>
						    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
						</div>
						<div class="tab-pane" id="enrollment">
						    <ul>
						        <li>Simple and easy enrollment process.</li>
						        <li>Application and payment online.</li>
						        <li>Go through our FAQs and Blogs and you would be ready to enroll yourself into the course.</li>
						        <li>You can also give us a call to help you with the onboarding process or to help clarify some aspects of the course.</li>
						    </ul>
						    <p>Once you&rsquo;ve made the online payment, you have to wait between 24-48 hours before we can enable your student account and get you started on your journey to academic success.</p>
						    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
						</div>
						<div class="tab-pane" id="academicsupport">
						    <ul>
						        <li>Personalized tutor support.</li>
						        <li>Tons of reference material and videos</li>
						        <li>Self Paced</li>
						    </ul>
						    <p>
						        Trainees enrolled in the program would be personally tutored by our experienced faculties at Atheneum Global Teacher Training College. Regular doubt clearing sessions and other academic support is extended as part of this
						        personalized tutoring. The learning is at your own pace and the support provided is completely personalized and carries a high level of expertise.
						    </p>
						    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
						</div>
						<div class="tab-pane" id="placements">
						    <p>
						        Atheneum Global Teacher Training College educates global professionals to pursue a teaching profession in an early childhood setting anywhere in the world. Our industry linkages and international credentials lend credibility to your
						        candidature. Every candidate is issued a personalized letter to help him or her land up with an internship of their choice. This internship is an important step towards finishing your course as well as to start your journey as an
						        educator.
						    </p>
						    <p>
						        Candidates are also given privileged access to Jobs For Teachers, a national portal for teachers for all kinds of teaching assignments. In essence, what we wish to communicate is that Atheneum is committed to offering you the very
						        best education and the very best access to teaching opportunities.
						    </p>
						    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
