<title>GraduateProgram in Nursery Teacher Training in UK</title>
<meta name="keywords" content="online ntt course, nursery teacher training course online">
<meta name="description" content="GraduateProgram in NTT enables aspiring teachers to receive a degree in nursery education and to pursue a national career in teaching anywhere in the world.">
<?php include("_menu.php");?>
<link rel="canonical" href="<?php echo $url; ?>" async/>
<style>
/* .allcourse-title .page-heading {
  font-size: 30px !important;
  padding-bottom: 14px;
} */
.page-heading~p {
    font-size: 16px;
}
p,li{
  font-size: 15px !important;
}
h4{
  color:#777;
}
.breadcrub-style-3 .bg-img{
  background-image: url('/assets/img/study/post_ntt.jpg');
}
</style>
<!-- Breadcrumb -->
  <div class="breadcrub breadcrub-style-3 section allcourse-title">
    <div class="bg-img d-flex flex-column justify-content-center">
<div class="overlay"></div>
      <div class="container">
        <div class="heading">
          <h1 class="page-heading">Graduate in Nursery Teachers Training</h1>
        </div>
      </div>
      <!-- <div class="overlay"></div> -->
    </div>
  </div>

<!-- Course Detail -->
<div class="course-detail section" id="single-coursepage">
    <div class="container">
        <div class="row">
            	<div class="col-12 col-lg-9 content">
                <h2 class="h4 pb-2">Graduate in Nursery Teachers Training</h2>
               <p>The course is the perfect curriculum for those seeking valuable insights into the fundamental principles of early childhood education that can be used to implement early childhood education successfully. This course is based on the guidelines established by the National Council for Education Research and Training.</p>
               <p>We recognize that the success of nursery education depends on the quality of teacher education in the country. This course helps nursery teachers and aspirants to pursue a career as nursery teachers while not sacrificing themselves to the current profession or life choices that require them to obtain a global postgraduate diploma in the field of their choice.</p>
					<div class="divider"></div>
					<h3 class="text-center pt-1 pb-2">NTT Graduate Course Details</h3>
					<div class="tabs tabs-vertical" id="courses-tab">
					    <div class="row">
					        <div class="col-md-4">
					            <ul class="nav flex-column nav-tabs" id="course-faq" role="tablist" aria-orientation="vertical">
					                <li class="nav-item">
					                    <a class="nav-link active" href="#courseduartion" data-toggle="tab">COURSE DURATION</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#eligibility" data-toggle="tab">ELIGIBILITY</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#coursefee" data-toggle="tab">COURSE FEE</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#curriculum" data-toggle="tab">CURRICULUM</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#diploma" data-toggle="tab">DIPLOMA</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#enrollment" data-toggle="tab">ENROLLMENT PROCESS</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#academicsupport" data-toggle="tab">ACADEMIC SUPPORT</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#placements" data-toggle="tab">PLACEMENTS</a>
					                </li>
					            </ul>
					        </div>
					        <div class="col-md-8">
					            <div class="tab-content">
					                <div class="tab-pane active" id="courseduartion">
					                    <ul class="list">
					                        <li class="main-li check-arrow">Maximum duration of the course is 9 months.</li>
					                        <li class="main-li check-arrow">It is a flexible program that can be pursued from any corner of the globe.</li>
					                        <li class="main-li check-arrow">Self-paced.</li>
                                  <li class="main-li check-arrow">Fast track mode permits early completion</li>
					                    </ul>
                              <p>Average course completion time varies between 4-6 months depending on the number of hours per week that you devote to this course. The time spent though, is well accounted for as it equips teachers with the essential knowledge base to further his or her career in this field with confidence.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="eligibility">
					                    <ul class="list">
					                        <li class="main-li check-arrow">A high school degree is the minimum requirement for this course.</li>
                                  <li class="main-li check-arrow">Aspiring and existing teachers are free to apply..</li>
					                    </ul>
                              <p>The minimum eligibility requirement for the Post Diploma in Nursery Teacher Training is a bachelor’s degree from any recognized institute. After you’ve enrolled yourself into the course, you’d have to submit your documentation for our records.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="coursefee">
                              <ul class="list">
                                <li class="main-li check-arrow">Program is available in online mode..</li>
                                <li class="main-li check-arrow">Very reasonably priced with installment options available.</li>
                                <li class="main-li check-arrow">Scholarships available for meritorious students.</li>
                              </ul>
                              <p>The Graduate Diploma in Nursery Teacher Training is priced very affordably at $250 for students. The course is administered completely online and hence extremely conveniently administered for students pursuing existing careers or those who require the flexibility of self-paced online education. With a Graduation from Atheneum Global Teacher Training College, be best placed for a global teaching career with a very affordable investment.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="curriculum">
                              <p>Nursery education is a program which provides a stimulating play environment for Physical, Intellectual, Language, Social and emotional development of the child. It prepares children for the primary and it focuses on the holistic development of the child. It lays the foundation for the development of reading, writing and number work. It is a program which encourages interaction with the environment, active participation in-group activities and enhances creativity and problem solving in children. It stresses on providing first-hand experiences to children in ways that would ensure, development of skills related to the process of learning. It also stresses on prior planning and scheduling but it at the same time flexible to children’s needs. Nursery education indirectly promotes self-control and thereby inner discipline in children.</p>
                              <p>This curriculum has been designed for three years of nursery education before class I, which highlights goals, key concepts/ skills, pedagogical processes and early learning outcomes for nursery I, II and III. It also suggests ways of planning a nursery education programme, classroom organizations and management, assessment and building partnership with parents and community.</p>
                              <p>Keeping the NCERT guidelines in mind, the post graduate course covers the following topics in depth in a course than spans 15 months.</p>
					                    <ul class="list">
					                        <li class="main-li check-arrow">Objectives and Principles of NTT</li>
					                        <li class="main-li check-arrow">Development of Young Learners</li>
                                  <li class="main-li check-arrow">Developmentally Appropriate Practices - Young Learners.</li>
                                  <li class="main-li check-arrow">Home, School and Community</li>
                                  <li class="main-li check-arrow">Education in Early Years</li>
                                  <li class="main-li check-arrow">Classroom Management Strategies</li>
                                  <li class="main-li check-arrow">Learning Plan</li>
                                  <li class="main-li check-arrow">Magic of Play</li>
                                  <li class="main-li check-arrow">School Organization and Management</li>
                                  <li class="main-li check-arrow">Assessment of Young Learners</li>

					                    </ul>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="diploma">
					                    <p>After the completion of the Graduate diploma course, candidates are awarded a diploma by Atheneum Global Teacher Training College. The diploma awarded have an added advantage as the word ‘Online’ is not mentioned in the certificates. Shipment of certificates within India is done free of cost but the candidates residing outside India are required to pay an standard fee of<strong>US $ 35</strong> as certificate dispatch fee. Generally after submission of all the assignments and completion of the course, you have to pay the dispatch fee. After that we take your shipping address and contact number and courier the certificate to the same. It takes a maximum of 3 weeks for the certificates to reach the candidate’s address.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="enrollment">
					                    <ul class="list">
					                        <li class="main-li check-arrow">Simple and easy enrollment process.</li>
					                        <li class="main-li check-arrow">Application and payment online.</li>
					                    </ul>
                              <p>All our course requirements are clarified on our website, FAQs and Blog posts. You can also give us a call to help you with the onboarding process or to help clarify some aspects of the course.</p>
                              <p>Once you’ve made the online payment, you have to wait between 24-48 hours before we can enable your student account and get you started on your journey to academic success.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="academicsupport">
					                    <ul class="list">
					                        <li class="main-li check-arrow">Personalized tutor support.</li>
					                        <li class="main-li check-arrow">Tons of reference material and videos</li>
					                        <li class="main-li check-arrow">Self Paced Program at a mouse click away from you.</li>
					                    </ul>
					                    <p>
                              Trainees enrolled into the program would be personally tutored by our experienced faculties at Atheneum Global Teacher Training College. Regular doubt clearing sessions and other academic support is extended as part of this personalized tutoring. The learning is at your own pace and the support provided is completely personalized and carries high level of expertise.
                             </p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="placements">
					                    <p>We appreciate your passion for a career in the field of early childhood education. Integral to your enrolment into this course, is the expectation of being placed in a preschool or school. Atheneum Global Teacher Training College’s industry linkages and international credentials lends credibility to your candidature. Every candidate is issued a personalized letter to help him or her land up with an internship of their choice. This internship is an important step towards finishing your course as well as to start your journey as an educator.</p>
                              <p>In addition to this, candidates are also given privileged access to Jobs For Teachers, a national portal for teachers for all kinds of teaching assignments. In essence, what we wish to communicate is that Atheneum is committed to offer you the very best education and the very best access to teaching opportunities.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					            </div>
					        </div>
					    </div>
					</div>
            	</div>

            	<div class="col-12 col-lg-3 hide-md-and-down" id="single-course-sidebar">
                    <h5>Related Courses</h5>
                    <ul class="list">
                        <li class="sub-li">
                        	<div class="post-image">
                                <div class="d-block img-thumbnail img-thumbnail-no-borders">
                                    <img src="/assets/img/course/1.jpg" width="50" height="50" alt="">
                                </div>
                            </div>
                            <div class="post-info">
                                <a href="/certificate-in-nursery-teacher-training">Certificate in Nursery Teacher training</a>
                            </div>
                        </li>
                        <li class="sub-li">
                        	<div class="post-image">
                                <div class="d-block img-thumbnail img-thumbnail-no-borders">
                                    <img src="/assets/img/course/3.jpg" width="50" height="50" alt="">
                                </div>
                            </div>
                            <div class="post-info">
                                <a href="/global-qualification-in-nursery-teacher-training">Post Graduate in Nursery Teacher training</a>
                            </div>
                        </li>
                    </ul>
                    <?php include("_common-sidebar.php");?>
                </div>
        </div>
    </div>
</div>
