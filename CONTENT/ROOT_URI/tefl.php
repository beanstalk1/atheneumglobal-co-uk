<title>TEFL Level 5 Ofqual regulated and UK recognized program</title>
<meta name="keywords" content="tefl course, tefl, tefl certification online, tefl course online">
<meta name="description" content="Get online and in-class certification of our TEFL Level 5 168 hour course. Atheneum provides globally recognized TEFL course for aspiring ESL candidates.">
<?php include("_menu.php");?>
<link rel="canonical" href="<?php echo $url; ?>" async/>
<style>
 .breadcrub-style-3 .bg-img{
   background-image: url('/assets/img/study/tefl.jpg');
 }
 .tab-content {
     padding: 30px;
 }
 .nav {
    justify-content: flex-start;
}
 .nav-pills .nav-link.active {
     color: #fff;
     background-color: #6c171b;
     position: relative;
 }
 .nav-link.active:before {
     position: absolute;
     content: "";
     border-top: 10px solid #6c171b;
     border-left: 10px solid transparent;
     border-right: 10px solid transparent;
     bottom: -10px;
     left: 43%;
 }
 li.nav-item .nav-link h4 {
     margin-bottom: 0px;
     font-size: 13px;
 }
 .nav-pills .nav-link {
    color: #000;
    border-radius: 10px;
    border: 1px solid #eacccd;
    padding: 12px 13px;
}
 .tab-content {
     padding: 26px 17px 1px;
     border: 1px solid #eacccd;
     border-radius: 7px;
 }
 .nav-pills .nav-link {
    margin: 22px 10px 8px;
     transition: 0.3s;
 }
 .nav-pills .nav-link:hover{
   background-color: #6C171C;
   color: #fff;
 }
 </style>
<!-- Breadcrumb -->
  <div class="breadcrub breadcrub-style-3 section allcourse-title">
    <div class="bg-img d-flex flex-column justify-content-center">
<div class="overlay"></div>
      <div class="container">
        <div class="heading">
          <h1 class="page-heading">LEVEL 5 TEFL COURSE | OFQUAL REGULATED</h1>
        </div>
      </div>
      <!-- <div class="overlay"></div> -->
    </div>
  </div>


<div class="course-detail section" id="single-coursepage">
    <div class="container">
        <div class="row">
            	<div class="col-12 col-lg-9 content">
            		<h2>Course Information</h2>
					<p>Our <strong>level 5 TEFL course (168 hours)</strong> can be completed 100% online.&nbsp;</p>
					<p>
					    This version is particularly suitable if you feel confident without any tuition in the classroom or maybe have previous teaching experience. This version of the course gives you the training from an EFL professional with many years of
					    experience. Internships that you take up after this course, will provide the opportunity to practice what you learned during the course.
					</p>
					<div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					<!-- <h3>Qualification Purpose</h3>
					<p>
					    The Ofqual regulated Level 5 Certificate in <strong>Teaching English as a Foreign Language</strong> (RQF) for those who wish to teach English to those who speak other languages. The qualification aims to develop knowledge and skills in
					    a specialist area of teaching, allowing learners to teach English as a foreign language. The Focus Awards Level 5 Certificate in Teaching English as a Foreign Language (RQF) is aimed at those learners wishing to teach English as a
					    Foreign language but have limited or no experience.
					</p>
					<h3>Entry Requirements</h3>
					<ul>
					    <li>This qualification has been designed to be accessible without artificial barriers that restrict access and progression. There are no entrance requirements.</li>
					    <li>In the case of applicants whose first language is not English, then IELTS 6.5 (or equivalent) is recommended.</li>
					</ul>
					<p><strong>Age Ranges</strong> Entry is at the discretion of Atheneum Global Teacher Training College; however, learners should be aged 16 or above to undertake this qualification.</p>
					<h3>Learning Outcomes</h3>
					<ol class="list">
					    <li class="main-li no-bullet">Understand the rules of the English language and how to convey these in a foreign language classroom.</li>
					    <li class="main-li no-bullet">Understand the nature of foreign language skills and how to develop them.</li>
					    <li class="main-li no-bullet">Understand and apply basic standard teaching procedures appropriately.</li>
					    <li class="main-li no-bullet">Select, design, and review their own teaching materials and lesson plans appropriate to specific teaching situations.</li>
					    <li class="main-li no-bullet">Demonstrate their ability to work independently.</li>
					    <li class="main-li no-bullet">Improve their own employability through developing their awareness of the relationship between learner needs and the practical application of teaching methodology in the language classroom.</li>
					</ol>
					<h3>Assessment Methods</h3>
					<p>Each participant is required to create a portfolio of evidence that demonstrates the achievement of all the learning outcomes and the assessment criteria associated with each unit.</p>
					<p>The main pieces of evidence for the portfolio could include:</p>
					<ul class="list">
					    <li class="main-li check-arrow">Assessor observation &ndash; completed observational checklists on related action plans</li>
					    <li class="main-li check-arrow">Witness testimony</li>
					    <li class="main-li check-arrow">Learner product</li>
					    <li class="main-li check-arrow">Worksheets</li>
					    <li class="main-li check-arrow">Assignments / projects / reports</li>
					    <li class="main-li check-arrow">Record of oral and written questioning</li>
					    <li class="main-li check-arrow">Learner and peer reports</li>
					    <li class="main-li check-arrow">Recognition of prior learning (RPL)</li>
					</ul>
                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div> -->

                    <section class='pils pt-4 pb-4'>
                    <div class="container">
                      <!-- <h2 class="text-center pb-3">Our Departments</h2> -->
                      <div class="row">
                        <div class="col-12">
                          <!-- Nav pills -->
                      <ul class="nav nav-pills">
                        <li class="nav-item">
                          <a class="nav-link active" data-toggle="pill" href="#home"><h4>Qualification Purpose</h4></a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" data-toggle="pill" href="#menu1"><h4>Entry Requirements</h4></a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" data-toggle="pill" href="#menu2"><h4>Learning Outcomes</h4></a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" data-toggle="pill" href="#menu3"><h4>Assessment Methods</h4></a>
                        </li>
                      </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                      <div class="tab-pane container active" id="home">
                        <p>
              					    The Ofqual regulated Level 5 Certificate in <strong>Teaching English as a Foreign Language</strong> (RQF) for those who wish to teach English to those who speak other languages. The qualification aims to develop knowledge and skills in
              					    a specialist area of teaching, allowing learners to teach English as a foreign language. The Focus Awards Level 5 Certificate in Teaching English as a Foreign Language (RQF) is aimed at those learners wishing to teach English as a
              					    Foreign language but have limited or no experience.
              					</p>
              					<div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
                      </div>
                      <div class="tab-pane container fade" id="menu1">
                        <ul>
              					    <li>This qualification has been designed to be accessible without artificial barriers that restrict access and progression. There are no entrance requirements.</li>
              					    <li>In the case of applicants whose first language is not English, then IELTS 6.5 (or equivalent) is recommended.</li>
              					</ul>
              					<p><strong>Age Ranges</strong> Entry is at the discretion of Atheneum Global Teacher Training College; however, learners should be aged 16 or above to undertake this qualification.</p>
                        <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
                      </div>
                      <div class="tab-pane container fade" id="menu2">
                        <ol class="list">
              					    <li class="main-li no-bullet">Understand the rules of the English language and how to convey these in a foreign language classroom.</li>
              					    <li class="main-li no-bullet">Understand the nature of foreign language skills and how to develop them.</li>
              					    <li class="main-li no-bullet">Understand and apply basic standard teaching procedures appropriately.</li>
              					    <li class="main-li no-bullet">Select, design, and review their own teaching materials and lesson plans appropriate to specific teaching situations.</li>
              					    <li class="main-li no-bullet">Demonstrate their ability to work independently.</li>
              					    <li class="main-li no-bullet">Improve their own employability through developing their awareness of the relationship between learner needs and the practical application of teaching methodology in the language classroom.</li>
              					</ol>
                        <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
                      </div>
                      <div class="tab-pane container fade" id="menu3">
                        <p>Each participant is required to create a portfolio of evidence that demonstrates the achievement of all the learning outcomes and the assessment criteria associated with each unit.</p>
              					<p>The main pieces of evidence for the portfolio could include:</p>
              					<ul class="list">
              					    <li class="main-li check-arrow">Assessor observation &ndash; completed observational checklists on related action plans</li>
              					    <li class="main-li check-arrow">Witness testimony</li>
              					    <li class="main-li check-arrow">Learner product</li>
              					    <li class="main-li check-arrow">Worksheets</li>
              					    <li class="main-li check-arrow">Assignments / projects / reports</li>
              					    <li class="main-li check-arrow">Record of oral and written questioning</li>
              					    <li class="main-li check-arrow">Learner and peer reports</li>
              					    <li class="main-li check-arrow">Recognition of prior learning (RPL)</li>
              					</ul>
                          <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
                      </div>
                    </div>
                    </div>
                    </div>
                    </div>
                    </section>
					<div class="divider"></div>
					<!-- <h3>General FAQs for TEFL course</h3>

                    <div class="accordion-wrapper">
                        <div id="accordion">
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <h5 class="mb-0">
                                        <a role="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            What is a level 5 qualification?
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>What is a level 5 qualification?<br />When you hear of "levels" this relates to the qualifications system of the UK government. This system helps learners studying governed qualifications from the UK government to understand where their qualification sits compared to other qualifications. A regulated qualification level 5 means Ofqual (Government of the United Kingdom) has accepted the qualification to be at a recognized standard of achievement. Our Level 5 TEFL Certificate (168hrs) is the same standard as the following certificates:</p>
                                        <ul>
                                        <li>A higher education diploma (DipHE),</li>
                                        <li>A&nbsp;foundation degree, and</li>
                                        <li>Certificates CELTA and Trinity CertTESOL.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingTwo">
                                    <h5 class="mb-0">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            What do you mean by ‘regulated qualification’?
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>A regulated qualification means that it is officially recognised by government and sits on the Ofqual Register of Regulated Qualifications. You can view our qualification on the Ofqual register.</p>
                                        <p><a href="https://register.ofqual.gov.uk/Detail/Index/42412?category=qualifications">https://register.ofqual.gov.uk/Detail/Index/42412?category=qualifications</a></p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingThree">
                                    <h5 class="mb-0">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            Why does other course providers provide so many course options and ‘levels’?
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>The response is simple: we just don't know! Recruiters who deliver the best prospects with the most affordable packages are now eligible for <strong>TEFL certification at level 5</strong>. All regulated level 5 courses have a length of 1<strong>50hrs +</strong>. We have never seen a single employer request TEFL training for more than <strong>168hrs</strong>.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingFour">
                                    <h5 class="mb-0">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                            Why choose a regulated qualification rather than a cheaper groupon option?
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>Choosing a regulated credential to study is your quality guarantee. Course providers providing regulated certificates must comply with very strict rules and procedures about quality assurance. This accounts for:</p>
                                        <ul>
                                        <li>The staff who support and assist students are adequately trained</li>
                                        <li>The course provider has the means to deliver the qualification effectively</li>
                                        <li>The course itself teaches and tests the qualifications aims effectively</li>
                                        <li>The course contents were written and drawn up by industry experts and peer-reviewed</li>
                                        <li>Learner feedback and grades are decent, moderate and meet modern academic expectations</li>
                                        <li>The qualification will also be accepted globally by schools, employers, foreign embassies, and governments of most other countries, officially controlled by the UK government.<br />&nbsp;</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingFive">
                                    <h5 class="mb-0">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                            How to determine if a qualification is regulated?
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>Any qualification that is officially recognized by the UK government will sit on the Ofqual Register of Regulated Qualifications. Make sure to ask your course provider to provide a link to the register for their qualification.</p>
                                        <p>You can view our qualification on the Ofqual Register of Regulated Qualifications here.</p>
                                        <p><a href="https://register.ofqual.gov.uk/Detail/Index/42412?category=qualifications">https://register.ofqual.gov.uk/Detail/Index/42412?category=qualifications</a></p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingSix">
                                    <h5 class="mb-0">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                            Is there any difference between TEFL and TESOL?
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>TEFL and TESOL are both acronyms for teaching English to non-native English speakers. Both acronyms can be used to describe qualifications that certify you to teach English as a foreign language. To a large extent, "TEFL" and "TESOL" are used interchangeably. However, employers may have certain internal policies favoring one qualification over the other. It is always good to inquire about specific qualification requirements of employers before enrolling in either of the courses.</p>
                                        <p><strong>TEFL -</strong> Teaching English as a Foreign Language</p>
                                        <p><strong>TESOL -</strong> Teaching English to Speakers of Other Languages</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingSeven">
                                    <h5 class="mb-0">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                            WHow to determine if a qualification is regulated?
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapseSeven" class="collapse" aria-labelledby="headingSeven" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>Isn&rsquo;t it better for learners to just enroll in a Groupon or Amazon course for TEFL?<br />It doesn't come cheap to offer a high-quality TEFL course that provides qualified tutors to students and leads to a government-regulated qualification! Course providers who sell on regular sale platforms have to massively discount their course fees and then split what they make 50/50 with the sale marketplace like Groupon, Wowcher&amp; Amazon.</p>
                                        <p>It is unlikely for TEFL course providers to sell on regular deal websites with such low revenues to:</p>
                                        <ul>
                                        <li>Provide continuous assistance from experienced &amp; trained tutors</li>
                                        <li>Have fair and moderated marking/classification systems</li>
                                        <li>Create and upgrade quality course content &amp; services on a continuous basis</li>
                                        <li>Offer a course leading to regulated government qualification</li>
                                        <li>Offer career assistance and continuing support upon completion of the course</li>
                                        </ul>
                                        <p>TEFL courses offered on day-to-day deal websites are unregulated, unaccredited, lack academic legitimacy and employers do not value them.</p>
                                        <p>We know it's really frustrating to navigate your way through the TEFL courses page. And we give just one course, exactly what you need to get the best jobs in the world!</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingEight">
                                    <h5 class="mb-0">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-target="#collapseEight" aria-expanded="false" aria-controls="collapseEight">
                                            Is a Level 5 course always a level 5 course?
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapseEight" class="collapse" aria-labelledby="headingEight" data-parent="#accordion">
                                    <div class="card-body">
                                        <p>&nbsp;You can see online advertised cheap TEFL courses claiming to be a "level," such as "Level 5 140-Hour TEFL Course" (often advertised on daily deal websites), these courses are sadly not the level they claim to be and are highly deceptive.</p>
                                        <p>A course-level informs learners where the qualification falls on the Regulated Qualifications System (RQF) for the UK. If a course is not listed in the Regulated Qualifications Registry, then it is not really a level at all. Our certification can be found on the List here.</p>
                                        <p>Make sure you ask every TEFL training provider that claims to have a link to the Register of Regulated Qualifications to provide a "degree" of the TEFL course. If they are unable to do this then the credential is not the standard they claim to be international embassies and employers will not recognize it.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->
            	</div>

            	<div class="col-12 col-lg-3 hide-md-and-down" id="single-course-sidebar">
                    <h5>Related Courses</h5>
                    <ul class="list">
                    	<!-- <li class="sub-li">
                        	<div class="post-image">
                                <div class="d-block img-thumbnail img-thumbnail-no-borders">
                                    <img src="/assets/img/course/1.jpg" width="50" height="50" alt="">
                                </div>
                            </div>
                            <div class="post-info">
                                <a href="/tefl">TEFL Level 5 OFQUAL Program</a>
                            </div>
                        </li> -->
                        <li class="sub-li">
                        	<div class="post-image">
                                <div class="d-block img-thumbnail img-thumbnail-no-borders">
                                    <img src="/assets/img/course/1.jpg" width="50" height="50" alt="">
                                </div>
                            </div>
                            <div class="post-info">
                                <a href="/tesol-certification">TESOL CERTIFICATION</a>
                            </div>
                        </li>
                        <li class="sub-li">
                        	<div class="post-image">
                                <div class="d-block img-thumbnail img-thumbnail-no-borders">
                                    <img src="/assets/img/course/3.jpg" width="50" height="50" alt="">
                                </div>
                            </div>
                            <div class="post-info">
                                <a href="/tesol-masters">TESOL MASTERS </a>
                            </div>
                        </li>
                    </ul>
                    <?php include("_common-sidebar.php");?>
                </div>
        </div>
    </div>
</div>
