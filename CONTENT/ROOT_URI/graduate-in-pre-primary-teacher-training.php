<title>Level 5 Graduation in Pre and Primary TeacherTraining in UK</title>
<meta name="keywords" content="pre primary teacher training course">
<meta name="description" content="OurLevel 5 Global Qualification course in pre and primary teacher training helps develop the requisite knowledge and expertise required in pre and primary institutes globally.">
<?php include("_menu.php");?>
<link rel="canonical" href="<?php echo $url; ?>" async/>
<style>
/* .allcourse-title .page-heading {
  font-size: 30px !important;
  padding-bottom: 14px;
} */
.page-heading~p {
    font-size: 16px;
}
p,li{
  font-size: 15px !important;
}
h4{
  color:#777;
}
.breadcrub-style-3 .bg-img{
  background-image: url('/assets/img/study/graduate_pre.jpg');
}
#more {display: none;}
</style>
<!-- Breadcrumb -->
  <div class="breadcrub breadcrub-style-3 section allcourse-title">
    <div class="bg-img d-flex flex-column justify-content-center">
<div class="overlay"></div>
      <div class="container">
        <div class="heading">
          <h1 class="page-heading">Graduate in Pre-Primary Teacher Training Course</h1>
          <p>‘Those who know, do. Those that understand, teach.’ <br>–&nbsp;<strong>Aristotle</strong></br></p>
        </div>
      </div>
      <!-- <div class="overlay"></div> -->
    </div>
  </div>

<!-- Course Detail -->
<div class="course-detail section" id="single-coursepage">
    <div class="container">
        <div class="row">
            	<div class="col-12 col-lg-9 content">
                <h2 class="h4 pb-2">Graduate in Pre Primary Teacher Training Course</h2>
               <p>The Graduate Diploma in Pre-and Primary Teacher Training (PPTT) is an appropriate course available to students seeking to meet the requirements of pre-and primary-teachers. The course adopts the best practices of the National Curriculum Framework guidance provided by the Ministry of Women and Child Development and equips the participants of the course with the knowledge and confidence to be expert practitioners in the schools and preschools of their choice.<span id="dots">...</span><span id="more"><br><br>
               The first 6 years of life is also important as a foundation for the inculcation of social values and personal habits that are known to last a lifetime. What follows logically is the crucial importance of investing in these early years to ensure an enabling environment for every child and, therefore, a sound foundation for life, which is not only the right of every child but will also have a long-term impact on the quality of human capital available to a country. Pre-and primary education and teacher training to equip trainees to have an impact in this age group derives its importance from this rationale.<br><br>
               The course is perfect for those students who seek to fulfill the role of educator in a pre-and primary setting without compromising on existing career or life choices that require them to pursue a global post-graduate diploma in the field of their choice.</p>
               <a class="small-btn" onclick="myFunction()" id="myBtn" style="color:#fff; margin:2rem 0 !important;">Read More</a>
					<div class="divider"></div>
					<h3 class="text-center pt-1 pb-2">Pre-Primary Graduate Course Details</h3>
					<div class="tabs tabs-vertical" id="courses-tab">
					    <div class="row">
					        <div class="col-md-4">
					            <ul class="nav flex-column nav-tabs" id="course-faq" role="tablist" aria-orientation="vertical">
					                <li class="nav-item">
					                    <a class="nav-link active" href="#courseduartion" data-toggle="tab">COURSE DURATION</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#eligibility" data-toggle="tab">ELIGIBILITY</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#coursefee" data-toggle="tab">COURSE FEE</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#curriculum" data-toggle="tab">CURRICULUM</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#diploma" data-toggle="tab">DIPLOMA</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#enrollment" data-toggle="tab">ENROLLMENT PROCESS</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#academicsupport" data-toggle="tab">ACADEMIC SUPPORT</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" href="#placements" data-toggle="tab">PLACEMENTS</a>
					                </li>
					            </ul>
					        </div>
					        <div class="col-md-8">
					            <div class="tab-content">
					                <div class="tab-pane active" id="courseduartion">
					                    <ul class="list">
					                        <li class="main-li check-arrow">Maximum of 9 months.</li>
					                        <li class="main-li check-arrow">Can be accessed round the sphere.</li>
					                        <li class="main-li check-arrow">Option of early completion.</li>
					                    </ul>
                              <p></p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="eligibility">
					                    <ul class="list">
					                        <li class="main-li check-arrow">High School Degree from a recognized institution.</li>
                                  <li class="main-li check-arrow">Any Profession can apply.</li>
                                  <li class="main-li check-arrow">Current Teachers can also avail of the course.</li>
					                    </ul>
                              <p>The minimum degree required for joining the Graduate Diploma in Pre-Primary Teacher Teaching is a High School Degree from an authorized institution. After enrolment, submission of documents is required.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="coursefee">
                              <ul class="list">
                                <li class="main-li check-arrow">Online Platform.</li>
                                <li class="main-li check-arrow">Modestly priced, installment option available.</li>
                                <li class="main-li check-arrow">Scholarships for meritorious students.</li>
                              </ul>
                              <!-- <p>The Course is priced at Rs.15,500 for National Students and $250 for International Students. The course is designed online and hence is very flexible.</p> -->
                              <p>The Course is priced at $250. The course is designed online and hence is very flexible.</p>
                              <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="curriculum">
					                    <p>This course is intended for those who dream of becoming educators and administrators of Montessori. The training includes theory and practice.</p>
                              <p>Upon completion of the diploma:-</p>
                              <p>Pragmatic Papers, Archives containing language and cultural instructional resources. Published course work.
The diploma consists of theory, instruction, album production and viva-vocabulary. Montessori Education.</p>
					                    <p class="pt-1 pb-1"><strong>Theory: 8 modules.</strong><br>
                                                    <strong>Practicals: 4 modules</strong><br>
                                                    <strong>Albums: 5 modules.</strong></p>
					                    <p><strong>Theory :</strong></p>
					                    <ul class="list">
					                        <li class="main-li check-arrow">Life History of Maria Montessori.</li>
					                        <li class="main-li check-arrow">Montessori Methods And History.</li>
                                  <li class="main-li check-arrow">Early Childhood Education.</li>
                                  <li class="main-li check-arrow">Montessori School Administration.</li>
                                  <li class="main-li check-arrow">Sensory Education in Montessori.</li>
                                  <li class="main-li check-arrow">Early Childhood Language.</li>
                                  <li class="main-li check-arrow">Methods of Teaching, Writing, and Reading.</li>
					                    </ul>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="diploma">
					                    <p>
                                 After successfully finishing the course, the certificate will be handed over, where the word 'Online' is not mentioned anywhere.
					                    </p>
                              <p>
                                The Certificate of Indian Candidates is dispatched free of cost. After the submission of all the documents, we ask for the shipping address and contact number.
                              </p>
                              <p>The courier takes a maximum of 3 weeks for the certificates to reach the candidate's address.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="enrollment">
					                    <ul class="list">
					                        <li class="main-li check-arrow">Easy enrollment process.</li>
					                        <li class="main-li check-arrow">Payment and Application online.</li>
					                    </ul>
                              <p>On our blogs, FAQs and blog posts our course is illustrated. During the boarding process we are right there with you.</p>
                              <p>Please wait 24-48 hours after payment before we activate the student's account and start to succeed.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="academicsupport">
					                    <ul class="list">
					                        <li class="main-li check-arrow">Tutor support in case of academic queries.</li>
					                        <li class="main-li check-arrow">Unlimited references, videos.</li>
					                        <li class="main-li check-arrow">Self-paced</li>
					                    </ul>
					                    <p>
                               Candidates enrolled would be personally guided by our experienced faculties.
                             </p>
                             <p>Regular Doubt Clearing Session is an integral part of our program. Learning is at your own pace.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					                <div class="tab-pane" id="placements">
					                    <p>Atheneum Global Teacher Training College issues a personalized letter to help the student with an internship of their choice, which helps in career success.
We also give privileged access to Jobs for Teachers, a national portal for teachers of all kinds of teaching assignments.</p>
					                    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
					                </div>
					            </div>
					        </div>
					    </div>
					</div>
            	</div>

            	<div class="col-12 col-lg-3 hide-md-and-down" id="single-course-sidebar">
                    <h5>Related Courses</h5>
                    <ul class="list">
                        <li class="sub-li">
                        	<div class="post-image">
                                <div class="d-block img-thumbnail img-thumbnail-no-borders">
                                    <img src="/assets/img/course/1.jpg" width="50" height="50" alt="">
                                </div>
                            </div>
                            <div class="post-info">
                                <a href="/certificate-in-pre-primary-teacher-training">Certificate in Pre-Primary teacher training course</a>
                            </div>
                        </li>
                        <li class="sub-li">
                        	<div class="post-image">
                                <div class="d-block img-thumbnail img-thumbnail-no-borders">
                                    <img src="/assets/img/course/3.jpg" width="50" height="50" alt="">
                                </div>
                            </div>
                            <div class="post-info">
                                <a href="/global-qualification-in-pre-primary-teacher-training">Post Graduate in Pre-Primary Teacher Training</a>
                            </div>
                        </li>
                    </ul>
                    <?php include("_common-sidebar.php");?>
                </div>
        </div>
    </div>
</div>
<script>
function myFunction() {
  var dots = document.getElementById("dots");
  var moreText = document.getElementById("more");
  var btnText = document.getElementById("myBtn");

  if (dots.style.display === "none") {
    dots.style.display = "inline";
    btnText.innerHTML = "Read more";
    moreText.style.display = "none";
  } else {
    dots.style.display = "none";
    btnText.innerHTML = "Read less";
    moreText.style.display = "inline";
  }
}
</script>
