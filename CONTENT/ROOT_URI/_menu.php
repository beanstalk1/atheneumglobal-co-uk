
    <!-- Fab Icon -->
    <link rel="shortcut icon" type="image/png" href="/assets/img/logos/fav_icon.png"/>
    <!-- Fonts Awesome Icons -->
    <link rel="stylesheet" href="/assets/fonts/fontawesome/css/all.min.css">
    <!-- Flaticon -->
    <link rel="stylesheet" href="/assets/css/vander/flaticon.css">
    <!-- Bootstrap Link -->
    <link rel="stylesheet" href="/assets/css/vander/bootstrap.min.css">
    <!-- Animate Css -->
    <link rel="stylesheet" href="/assets/css/vander/animate.css">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="/assets/css/vander/owl.carousel.min.css">
    <!-- Slick Slider -->
    <link rel="stylesheet" href="/assets/css/vander/slick.css">
    <!-- Flaticon -->
    <link rel="stylesheet" href="/assets/css/vander/flaticon.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="/assets/css/style.css">
    <link rel="stylesheet" href="/assets/css/custom.css">

</head>

<body>
  <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W3LKFGT"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- Navbar Section -->
    <nav class="navbar-section container-fluid d-none d-sm-block">
        <div class="row">
            <div class="col-12 nav-container">
                <!-- Main Nav Start -->
                <div class="main-nav">
                    <nav class="navbar navbar-expand-lg">
                        <a class="navbar-brand" href="/">
                            <img src="/assets/img/logos/atheneum.png" alt="Atheneum Global">
                        </a>

                        <!-- For Tablet Screen -> Start -->
                        <div class="menu-toggler d-block d-xl-none">
                            <div class="hamburger-menu">
                                <a href="#">
                                    <span>Menu</span>
                                </a>
                            </div>
                        </div>
                        <div class="nav-menu-items">
                            <ul class="nav-items">
                                <li><a href="#">Login/Sign up</a></li>
                                <li class="menu-item-has-children"><a href="/" class="current-menu-item">Home</a>
                                </li>
                                <li><a href="/about">About</a></li>
                                <li class="menu-item-has-children"><a href="#">Departments</a>
                                    <ul class="sub-menu">
                                        <li class="sub-menu-item-has-children"><a href="/english-studies">English Studies</a>
                                            <ul class="sub-menu">
                                                <li><a href="/tefl">TEFL LEVEL 5</a></li>
                                                <li><a href="/tesol-certification">TESOL CERTIFICATION </a></li>
                                                <li><a href="/tesol-masters">TESOL MASTERS </a></li>
                                                <!-- <li><a href=/global-teaching-opportunities>GLOBAL TEACHING OPPRTUNITIES</a></li>
                                                <li><a href=/faq>FAQ</a></li> -->
                                            </ul>
                                        </li>
                                        <li class="sub-menu-item-has-children"><a href="/professional-development">Career Studies</a>
                                            <ul class="sub-menu">
                                                <li><a href="/montessori-teacher-training">Montessori Teacher Training</a></li>
                                                <li><a href="/pre-primary-teacher-training">Pre-Primary Teacher Training</a></li>
                                                <li><a href="/early-childhood-education-and-care">Early Childhood Education and Care</a></li>
                                                <li><a href="/nursery-teacher-training">Nursery Teacher Training</a></li>
                                                <li><a href="/education-management">Education Management</a></li>
                                                <li><a href="/child-development-associate">Child Development Associate</a></li>
                                            </ul>
                                        </li>
                                        <li class="sub-menu-item-has-children"><a href="/business-studies">Business Studies</a>
                                            <ul class="sub-menu">
                                                <li><a href="/ofqual-level-3-diploma-in-business-administration">Ofqual Level 3 Diploma in Business Administration</a></li>
                                                <li><a href="/ofqual-level-5-diploma-in-management-and-leadership">Ofqual Level 5 Diploma in Management and Leadership</a></li>
                                                <li><a href="/diploma-in-business-administration">Diploma in Business Administration</a></li>
                                                <li><a href="/tourism-management">Tourism Management</a></li>
                                                <!-- <li><a href="/tourism-management">Tourism Management</a></li> -->
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children"><a href="/faq">FAQ</a>
                                </li>
                                <!-- <li class="menu-item-has-children"><a href="/blog">Blog</a> -->
                                </li>
                                <li class="menu-item-has-no-child"><a href="/contact">Contact</a></li>
                                <li class="menu-item-has-no-child"><a href=“/apply-now”>Apply Now</a></li>
                            </ul>
                        </div>
                        <!-- For Tablet Screen -> End -->

                        <div class="collapse navbar-collapse" id="navbarNav">
                            <ul class="navbar-nav">
                                <li class="menu-item-no-children">
                                    <a href="/" class="current-menu-item">Home</a>
                                </li>
                                <li class="menu-item-has-no-children">
                                    <a href="/about">About</a>
                                </li>
                                <li class="menu-item-has-children"><a href="#">Departments</a>
                                    <ul class="sub-menu">
                                        <li class="sub-menu-item-has-children"><a href="/english-studies">English Studies</a>
                                            <ul class="sub-menu">
                                                <li><a href="/tefl">TEFL LEVEL 5</a></li>
                                                <li><a href="/tesol-certification">TESOL CERTIFICATION </a></li>
                                                <li><a href="/tesol-masters">TESOL MASTERS </a></li>
                                               <!-- <li><a href=/global-teaching-opportunities>GLOBAL TEACHING OPPRTUNITIES</a></li>
                                              <li><a href=/faq>FAQ</a></li> -->
                                            </ul>
                                        </li>
                                        <li class="sub-menu-item-has-children"><a href="/professional-development">Career Studies</a>
                                            <ul class="sub-menu">
                                                <li><a href="/montessori-teacher-training">Montessori Teacher Training</a></li>
                                                <li><a href="/pre-primary-teacher-training">Pre-Primary Teacher Training</a></li>
                                                <li><a href="/early-childhood-education-and-care">Early Childhood Education and Care</a></li>
                                                <li><a href="/nursery-teacher-training">Nursery Teacher Training</a></li>
                                                <li><a href="/education-management">Education Management</a></li>
                                                <li><a href="/child-development-associate">Child Development Associate</a></li>
                                            </ul>
                                        </li>
                                        <li class="sub-menu-item-has-children"><a href="/business-studies">Business Studies</a>
                                            <ul class="sub-menu">
                                                <li><a href="/ofqual-level-3-diploma-in-business-administration">Ofqual Level 3 Diploma in Business Administration</a></li>
                                                <li><a href="/ofqual-level-5-diploma-in-management-and-leadership">Ofqual Level 5 Diploma in Management and Leadership</a></li>
                                                <li><a href="/diploma-in-business-administration">Diploma in Business Administration</a></li>
                                                <li><a href="/tourism-management">Tourism Management</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href=/faq>FAQ</a></li>
                                <!-- <li class="menu-item-no-children">
                                    <a href="/blog">Blog</a>
                                </li> -->
                                <li class="menu-item-has-no-children">
                                    <a href="/contact">Contact</a>
                                </li>
                                <li class="menu-item-has-no-children">
                                    <a href="/apply-now">Apply Now</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>

                <!-- Side Nav Start -->
                <div class="side-nav d-flex align-items-center">
                    <a class="auth" href="#">Sign In</a>

                    <div class="search search-active">
                        <i class="flaticon-search"></i>
                    </div>
                </div>
            </div>

            <!-- search start -->
            <div class="search-content-wrap main-search-active">
                <div class="search-content">
                  <div class="search-container mt-3">
                                  <form action="/action_page.php">
                                   <input type="text" placeholder="Search.." name="search">
                                   <button type="submit"><i class="flaticon-search"></i></button>
                                  </form>
                                </div>
                </div>
            </div>
        </div>
    </nav>

    <!-- Navbar for mobile screen -->
    <div class="navbar-mobile sticky-bar d-block d-sm-none">
        <div class="container-fluid">
            <div class="menu-toggler">
                <div class="hamburger-menu">
                    <a href="#">
                        <span>Menu</span>
                    </a>
                </div>
            </div>
            <div class="divider"></div>
            <div class="logo">
                <a href="/"><img src="/assets/img/logos/atheneum.png" alt="Atheneum Global"></a>
            </div>
            <div class="divider"></div>
            <div class="search">
                <i class="flaticon-search"></i>
            </div>
        </div>

        <div class="nav-menu-items">
                <ul class="nav-items">
                    <li><a href="#">Login/Sign up</a></li>
                    <li class="menu-item-no-children"><a href="/" class="current-menu-item">Home</a>
                    </li>
                    <li><a href="/about">About</a></li>
                    <li class="menu-item-has-children">
                        <a href="/english-studies">English Studies</a>
                        <ul class="sub-menu">
                            <li><a href="/tefl">TEFL LEVEL 5</a></li>
                            <li><a href="/tesol-certification">TESOL CERTIFICATION </a></li>
                            <li><a href="/tesol-masters">TESOL MASTERS </a></li>
                            <!-- <li><a href=/global-teaching-opportunities>GLOBAL TEACHING OPPRTUNITIES</a></li>
                            <li><a href=/faq>FAQ</a></li> -->
                        </ul>
                    </li>
                    <li class="menu-item-has-children">
                        <a href="/professional-development">Career Studies</a>
                        <ul class="sub-menu">
                            <li><a href="/montessori-teacher-training">Montessori Teacher Training</a></li>
                            <li><a href="/pre-primary-teacher-training">Pre-Primary Teacher Training</a></li>
                            <li><a href="/early-childhood-education-and-care">Early Childhood Education and Care</a></li>
                            <li><a href="/nursery-teacher-training">Nursery Teacher Training</a></li>
                            <li><a href="/education-management">Education Management</a></li>
                            <li><a href="/child-development-associate">Child Development Associate</a></li>
                        </ul>
                    </li>
                    <li class="sub-menu-item-has-children"><a href="/business-studies">Business Studies</a>
                                            <ul class="sub-menu">
                                                <li><a href="/ofqual-level-3-diploma-in-business-administration">Ofqual Level 3 Diploma in Business Administration</a></li>
                                                <li><a href="/ofqual-level-5-diploma-in-management-and-leadership">Ofqual Level 5 Diploma in Management and Leadership</a></li>
                                                <li><a href="/diploma-in-business-administration">Diploma in Business Administration</a></li>
                                                <li><a href="/tourism-management">Tourism Management</a></li>
                                            </ul>
                                        </li>
                     <li><a href=/faq>FAQ</a></li>
                    <!-- <li class="menu-item-no-children"><a href="/blog">Blog</a>
                    </li> -->
                    <li class="menu-item-has-no-child"><a href="/contact">Contact</a></li>
                    <li class="menu-item-has-no-child"><a href=“/apply-now”>Apply Now</a></li>
                </ul>
            </div>

        <div class="mobile-search-wrapper">
            <form>
                <input type="text" placeholder="Type something to start">
            </form>
        </div>
    </div>
