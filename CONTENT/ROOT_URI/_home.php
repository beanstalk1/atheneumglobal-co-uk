<title>Teacher Training Course in UK | Atheneum Global</title>
<meta name="keywords" content="teacher training course in UK">
<meta name="description" content=" We offer the best online and in-class teacher training programs in the UK. Ofqual regulated and UK Certification programs offered at the most competitive prices.">
<link rel="canonical" href="<?php echo $url;?>" async/>


    <!-- Fab Icon -->
    <link rel="shortcut icon" type="image/png" href="/assets/img/logos/fav_icon.png"/>
    <!-- Fonts Awesome Icons -->
    <link rel="stylesheet" href="/assets/fonts/fontawesome/css/all.min.css">
    <!-- Flaticon -->
    <link rel="stylesheet" href="/assets/css/vander/flaticon.css">
    <!-- Bootstrap Link -->
    <link rel="stylesheet" href="/assets/css/vander/bootstrap.min.css">
    <!-- Animate Css -->
    <link rel="stylesheet" href="/assets/css/vander/animate.css">
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="/assets/css/vander/owl.carousel.min.css">
    <!-- Slick Slider -->
    <link rel="stylesheet" href="/assets/css/vander/slick.css">
    <!-- Flaticon -->
    <link rel="stylesheet" href="/assets/css/vander/flaticon.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="/assets/css/style.css">
    <link rel="stylesheet" href="/assets/css/custom.css">
<style>
</style>
</head>
<body>
  <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W3LKFGT"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- Navbar Section -->
    <nav class="navbar-style-2 container-fluid d-none d-sm-block">
        <div class="row">
            <div class="col-12 nav-container">
                <!-- Main Nav Start -->
                <div class="main-nav">
                    <nav class="navbar navbar-expand-lg">
                        <a class="navbar-brand" href="/">
                            <img src="/assets/img/logos/atheneum-logo-white.png" alt="Atheneum Global">
                        </a>

                        <!-- For Tablet Screen -> Start -->
                        <div class="menu-toggler d-block d-xl-none">
                            <div class="hamburger-menu">
                                <a href="#">
                                    <span>Menu</span>
                                </a>
                            </div>
                        </div>
                        <div class="nav-menu-items">
                            <ul class="nav-items">
                                <li><a href="#">Sign In</a></li>
                                <li class="menu-item-has-children"><a href="/" class="current-menu-item">Home</a>
                                </li>
                                <li><a href="/about">About</a></li>
                                <li class="menu-item-has-children"><a href="#">Departments</a>
                                    <ul class="sub-menu">
                                        <li class="sub-menu-item-has-children"><a href="/english-studies">English Studies</a>
                                            <ul class="sub-menu">
                                                <li><a href="/tefl">TEFL LEVEL 5</a></li>
                                                <li><a href="/tesol-certification">TESOL CERTIFICATION </a></li>
                                                <li><a href="/tesol-masters">TESOL MASTERS </a></li>
                                                <!-- <li><a href=/global-teaching-opportunities>GLOBAL TEACHING OPPRTUNITIES</a></li>
                                                <li><a href=/faq>FAQ</a></li> -->
                                            </ul>
                                        </li>
                                        <li class="sub-menu-item-has-children"><a href="/professional-development">Career Studies</a>
                                            <ul class="sub-menu">
                                                <li><a href="/montessori-teacher-training">Montessori Teacher Training</a></li>
                                                <li><a href="/pre-primary-teacher-training">Pre-Primary Teacher Training</a></li>
                                                <li><a href="/early-childhood-education-and-care">Early Childhood Education and Care</a></li>
                                                <li><a href="/nursery-teacher-training">Nursery Teacher Training</a></li>
                                                <li><a href="/education-management">Education Management</a></li>
                                                <li><a href="/child-development-associate">Child Development Associate</a></li>
                                            </ul>
                                        </li>
                                        <li class="sub-menu-item-has-children"><a href="/business-studies">Business Studies</a>
                                            <ul class="sub-menu">
                                                <li><a href="/ofqual-level-3-diploma-in-business-administration">Ofqual Level 3 Diploma in Business Administration</a></li>
                                                <li><a href="/ofqual-level-5-diploma-in-management-and-leadership">Ofqual Level 5 Diploma in Management and Leadership</a></li>
                                                <li><a href="/diploma-in-business-administration">Diploma in Business Administration</a></li>
                                                <li><a href="/tourism-management">Tourism Management</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children"><a href="/faq">FAQ</a>
                                </li>
                                <!-- <li class="menu-item-has-children"><a href="/blog">Blog</a> -->
                                </li>
                                <li class="menu-item-has-no-child"><a href="/contact">Contact</a></li>
                                <li class="menu-item-has-no-child"><a href=“/apply-now”>Apply Now</a></li>
                            </ul>
                        </div>
                        <!-- For Tablet Screen -> End -->

                        <div class="collapse navbar-collapse" id="navbarNav">
                            <ul class="navbar-nav">
                                <li class="menu-item-no-children">
                                    <a href="/" class="current-menu-item">Home</a>
                                </li>
                                <li class="menu-item-has-no-children">
                                    <a href="/about">About</a>
                                </li>
                                <li class="menu-item-has-children"><a href="#">Departments</a>
                                    <ul class="sub-menu">
                                        <li class="sub-menu-item-has-children"><a href="/english-studies">English Studies</a>
                                            <ul class="sub-menu">
                                                <li><a href="/tefl">TEFL LEVEL 5</a></li>
                                                <li><a href="/tesol-certification">TESOL CERTIFICATION </a></li>
                                                <li><a href="/tesol-masters">TESOL MASTERS </a></li>
                                                <!-- <li><a href=/global-teaching-opportunities>GLOBAL TEACHING OPPRTUNITIES</a></li>
                                                <li><a href=/faq>FAQ</a></li> -->
                                            </ul>
                                        </li>
                                        <li class="sub-menu-item-has-children"><a href="/professional-development">Career Studies</a>
                                            <ul class="sub-menu">
                                                <li><a href="/montessori-teacher-training">Montessori Teacher Training</a></li>
                                                <li><a href="/pre-primary-teacher-training">Pre-Primary Teacher Training</a></li>
                                                <li><a href="/early-childhood-education-and-care">Early Childhood Education and Care</a></li>
                                                <li><a href="/nursery-teacher-training">Nursery Teacher Training</a></li>
                                                <li><a href="/education-management">Education Management</a></li>
                                                <li><a href="/child-development-associate">Child Development Associate</a></li>
                                            </ul>
                                        </li>
                                        <li class="sub-menu-item-has-children"><a href="/business-studies">Business Studies</a>
                                            <ul class="sub-menu">
                                                <li><a href="/ofqual-level-3-diploma-in-business-administration">Ofqual Level 3 Diploma in Business Administration</a></li>
                                                <li><a href="/ofqual-level-5-diploma-in-management-and-leadership">Ofqual Level 5 Diploma in Management and Leadership</a></li>
                                                <li><a href="/diploma-in-business-administration">Diploma in Business Administration</a></li>
                                                <li><a href="/tourism-management">Tourism Management</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                  <a href=/faq>FAQ</a>
                                </li>
                                <!-- <li class="menu-item-no-children">
                                    <a href="/blog">Blog</a>
                                </li> -->
                                <li class="menu-item-has-no-children">
                                    <a href="/contact">Contact</a>
                                </li>
                                <li class="menu-item-has-no-children">
                                    <a href="/apply-now">Apply Now</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>

                <!-- Side Nav Start -->
                <div class="side-nav d-flex align-items-center">
                    <a class="auth" href="#">Sign In</a>

                    <div class="search search-active">
                        <i class="flaticon-search"></i>
                    </div>
                </div>
            </div>

            <!-- search start -->
            <div class="search-content-wrap main-search-active">
                <div class="search-content">
                  <div class="search-container mt-3">
                                  <form action="/action_page.php">
                                   <input type="text" placeholder="Search.." name="search">
                                   <button type="submit"><i class="flaticon-search"></i></button>
                                  </form>
                                </div>

                    <a class="search-close d-flex justify-content-center align-items-center">
                        <i class="fas fa-times icon"></i>
                    </a>
                </div>
            </div>
        </div>
    </nav>

    <!-- Navbar for mobile screen -->
    <div class="navbar-mobile-2 sticky-bar d-block d-sm-none">
        <div class="container-fluid">
            <div class="menu-toggler">
                <div class="hamburger-menu">
                    <a href="#">
                        <span>Menu</span>
                    </a>
                </div>
            </div>
            <div class="divider"></div>
            <div class="logo">
                <a href="/"><img src="/assets/img/logos/atheneum-logo-white.png" alt="Atheneum Global"></a>
            </div>
            <div class="divider"></div>
            <div class="search">
                <i class="flaticon-search"></i>
            </div>
        </div>

        <div class="nav-menu-items">
                <ul class="nav-items">
                    <li><a href="#">Sign In</a></li>
                    <li class="menu-item-no-children"><a href="/" class="current-menu-item">Home</a>
                    </li>
                    <li><a href="/about">About</a></li>
                    <li class="menu-item-has-children">
                        <a href="/english-studies">English Studies</a>
                        <ul class="sub-menu">
                            <li><a href="/tefl">TEFL LEVEL 5</a></li>
                            <li><a href="/tesol-certification">TESOL CERTIFICATION </a></li>
                            <li><a href="/tesol-masters">TESOL MASTERS </a></li>
                            <!-- <li><a href=/global-teaching-opportunities>GLOBAL TEACHING OPPRTUNITIES</a></li>
                            <li><a href=/faq>FAQ</a></li> -->
                        </ul>
                    </li>
                    <li class="menu-item-has-children">
                        <a href="/professional-development">Career Studies</a>
                        <ul class="sub-menu">
                            <li><a href="/montessori-teacher-training">Montessori Teacher Training</a></li>
                            <li><a href="/pre-primary-teacher-training">Pre-Primary Teacher Training</a></li>
                            <li><a href="/early-childhood-education-and-care">Early Childhood Education and Care</a></li>
                            <li><a href="/nursery-teacher-training">Nursery Teacher Training</a></li>
                            <li><a href="/education-management">Education Management</a></li>
                            <li><a href="/child-development-associate">Child Development Associate</a></li>
                        </ul>
                    </li>
                    <li class="sub-menu-item-has-children"><a href="/business-studies">Business Studies</a>
                                            <ul class="sub-menu">
                                                <li><a href="/ofqual-level-3-diploma-in-business-administration">Ofqual Level 3 Diploma in Business Administration</a></li>
                                                <li><a href="/ofqual-level-5-diploma-in-management-and-leadership">Ofqual Level 5 Diploma in Management and Leadership</a></li>
                                                <li><a href="/diploma-in-business-administration">Diploma in Business Administration</a></li>
                                                <li><a href="/tourism-management">Tourism Management</a></li>
                                            </ul>
                                        </li>
                    <li><a href=/faq>FAQ</a></li>
                    <!-- <li class="menu-item-has-children"><a href="/blog">Blog</a> -->
                    </li>
                    <li class="menu-item-has-no-child"><a href="/contact">Contact</a></li>
                    <li class="menu-item-has-no-child"><a href=“/apply-now”>Apply Now</a></li>
                </ul>
            </div>

        <div class="mobile-search-wrapper">
            <form>
                <input type="text" placeholder="Type something to start">
            </form>
        </div>
    </div>

<!-- SLider Section -->
<section class="showcase-style-2">
    <div class="container-fluid">
        <div class="slider-wrapper">
            <div class="slider-item" style="background-image: url(/assets/img/slider/slider1.jpg);">
                <div class="content">
                    <div class="main-content">
                        <div class="heading">
                            <h1>Education that is Affordable, Flexible and Accredited.</h1>
                            <p class="pt-3" style="color:#fff;">Acquire professional teaching qualifications at your convenience for in-demand careers</p>
                        </div>
                    </div>
                </div>
                <div class="overlay"></div>
            </div>
            <div class="slider-item" style="background-image: url(/assets/img/slider/new-slider-images/AtheneumPictures.png);">
                <div class="content">
                    <div class="main-content">
                        <div class="heading">
                            <h1>Ofqual Regulated Qualifications for Global Careers</h1>
                            <p>We offer UK Qualifications for students worldwide to shape their careers as global educators</p>
                        </div>
                    </div>
                </div>
                <div class="overlay"></div>
            </div>
            <div class="slider-item" style="background-image: url(/assets/img/slider/new-slider-images/AtheneumPictures7.png);">
                <div class="content">
                    <div class="main-content">
                        <div class="heading">
                            <h1>Study Overseas with a UK Qualification</h1>
                            <p>Become an international educator with fully accredited qualifications that are flexible and affordable</p>
                        </div>
                    </div>
                </div>
                <div class="overlay"></div>
            </div>
            <div class="slider-item" style="background-image: url(/assets/img/slider/new-slider-images/AtheneumPictures4.png);">
                <div class="content">
                    <div class="main-content">
                        <div class="heading">
                            <h1>Teach English as a Foreign Language with accredited UK Qualification</h1>
                            <p>Gain a Level 5 accredited Ofqual TEFL Qualification for teaching English to speakers of other languages</p>
                        </div>
                    </div>
                </div>
                <div class="overlay"></div>
            </div>
            <div class="slider-item" style="background-image: url(/assets/img/slider/new-slider-images/AtheneumPictures3.png);">
                <div class="content">
                    <div class="main-content">
                        <div class="heading">
                            <h1>Become a Montessori Directress with Atheneum Global</h1>
                            <p>Pursue a Level 5 National Qualification Framework (NQF) Award for Montessori Education for a global career as a Montessori Educator.</p>
                        </div>
                    </div>
                </div>
                <div class="overlay"></div>
            </div>
        </div>

        <div class="side-social-icons hide-md-and-down">
            <a target="_blank" href="https://www.facebook.com/AtheneumGlobalTeacherTrainingCollege/"><i class="fab fa-facebook-square"></i></a>
            <a target="_blank" href="https://www.instagram.com/atheneum.global/"><i class="fab fa-instagram"></i></a>
            <a target="_blank" href="https://twitter.com/atheneum_global"><i class="fab fa-twitter"></i></a>
        </div>

        <div class="scroll-next hide-md-and-down">
            <div class="chevrons">
                <div class="chevron"></div>
                <div class="chevron"></div>
                <div class="chevron"></div>
            </div>
        </div>
        <div class="indicator-style-2">
            <i class="flaticon-back"></i>
            <div class="nums">
                <div class="current">01</div>
                <div class="total"></div>
            </div>
            <i class="flaticon-next"></i>
        </div>
    </div>
</section>
<!-- About Section -->
<section class="blog-section section" id="homepage-about" style="margin:0px !important">
    <div class="container">

        <div class="row" style="align-items:center; justify-content:center;">
            <div class="col-12 col-lg-6 content">
                <h2>Why choose Atheneum Global?</h2>
                <p>
                    Atheneum Global Teacher Training College is one of UK’s leading teacher training organization. Based in the heart of London, our organization spans the three continents of Europe, Asia and Africa. Our programs have been curated of many years of research and development. Tutored by specialists in their respective fields, our students carry the banner of Atheneum proudly across many countries that they’re teaching in. With a clear pathway to University degrees from premier colleges in UK, our graduates have gone on to forge outstanding careers in the field of education and business management.
                </p>
                <div class="large-btn" onclick="window.location.href = '/about';" style="margin:2rem 0 !important;">Know More</div>
            </div>
            <div class="col-12 col-lg-6 media">
                <img src="assets/img/study/early_graduate.jpg" alt="feature image" class="img-fluid" />
            </div>
        </div>
    </div>
</section>
<!-- Courses Section -->
<section id="events" class="events-section section">
    <div class="container">
        <h2 class="margin-bottom-50">Our Departments</h2>
        <div class="row event-item-style-1 odd-event">
            <div class="col-7 col-md-6 col-lg-3 media">
                <img src="assets/img/course/mtt.jpg" alt="events image" class="img-fluid" />
            </div>
            <div class="col-12 col-lg-7 details">
                <h3 style="text-transform: capitalize;">ENGLISH STUDIES</h3>
                <p>
                  Atheneum Global is professionally managed and run by trained educators with global qualifications. We have very stringent norms for trainers associated with us. The Department of English Studies has trained teacher educators globally who have been assessed by our trainers who have had either a Celta or a Delta qualification. Our Internal Qualification Assurance is delivered by IQA’s with a minimum of Level 4 qualification in Quality Assurance.
                </p>
                <div class="d-flex align-items-center event-infos">
                    <div class="info">
                        <i class="fas fa-clock"></i>
                        <span><strong>900 Hours Of Training Content</strong></span>
                    </div>
                    <div class="info ml-5">
                        <i class="fas fa-book"></i>
                        <span><strong>60+ Training Modules</strong></span>
                    </div>
                    <div class="info ml-5">
                        <i class="fas fa-certificate"></i>
                        <span><strong>6 Core Academic Programs</strong></span>
                    </div>
                </div>
                <button class="main-btn" onclick="window.location.href = '/english-studies';">Learn more</button>
            </div>
        </div>
        <div class="row event-item-style-1 even-event">
            <!-- <div class="col-5 col-lg-2 date">
                    <h1>12</h1>
                    <h5>JAN 2019</h5>
                </div> -->
            <div class="col-7 col-md-6 col-lg-3 media">
                <img src="assets/img/course/pptt.jpg" alt="events image" class="img-fluid" />
            </div>
            <div class="col-12 col-lg-7 details">
                <h3 style="text-transform: capitalize;">career studies</h3>
                <p>
                    The Department of Professional Studies hosts several teacher education programs like Montessori Education, Nursery Education, Early Childhood Care, and Education, Pre and Primary Teacher Training and School Organization and Management. All our trainers have globally accomplished teacher educators with degrees in Early Childhood or equivalent and professional accomplishments vetted through references.
                </p>
                <div class="d-flex align-items-center event-infos">
                    <div class="info">
                        <i class="fas fa-clock"></i>
                      <span><strong>5000 Hours Of Training Content</strong></span>
                    </div>
                    <div class="info ml-5">
                        <i class="fas fa-book"></i>
                       <span><strong>120+ Training Modules</strong></span>
                    </div>
                    <div class="info ml-5">
                        <i class="fas fa-certificate"></i>
                        <span><strong>15 Core Academic Programs</strong></span>
                    </div>
                </div>
                <button class="main-btn" onclick="window.location.href = '/professional-development';">Learn more</button>
            </div>
        </div>
        <div class="row event-item-style-1 odd-event">
            <!-- <div class="col-5 col-lg-2 date">
                    <h1>12</h1>
                    <h5>JAN 2019</h5>
                </div> -->
            <div class="col-7 col-md-6 col-lg-3 media">
                <img src="assets/img/course/ntt.jpg" alt="events image" class="img-fluid" />
            </div>
            <div class="col-12 col-lg-7 details">
                <h3>BUSINESS STUDIES</h3>
                <p>
                   The Department of Business Studies offers professional programs for aspirants looking to secure their future as Business Managers across a diverse range of industries. Our tutors have professionally accomplished teacher trainers with professional accomplishments and a history of training business management trainees.
                </p>
                <div class="d-flex align-items-center event-infos">
                    <div class="info">
                        <i class="fas fa-clock"></i>
                        <span><strong>600 Hours Of Training Content</strong></span>
                    </div>
                    <div class="info ml-5">
                        <i class="fas fa-book"></i>
                        <span><strong>90+ Training Modules</strong></span>
                    </div>
                    <div class="info ml-5">
                        <i class="fas fa-certificate"></i>
                        <span><strong>4 Core Academic Programs</strong></span>
                    </div>
                </div>
                <button class="main-btn" onclick="window.location.href = '/business-studies';">Learn more</button>
            </div>
        </div>
    </div>
</section>

<!-- Course Slider Section -->
<section class="blog-section section" id="course-homepage">
    <div class="container">
        <h2 class="text-left">Our Courses</h2>
        <div class="blog-carousel-wrap">
            <div class="blog-carousel-controls hide-md-and-down">
                <div class="dots"></div>
            </div>
            <div class="blog-carousel-01">
                <div class="single-blog-grid-item-01">
                    <div class="thumb">
                        <img src="/assets/img/Cards/TEFL.png" alt="blog image" />
                        <div class="hover">
                            <a href="/tefl" class="readmore"><i class="fas fa-link"></i></a>
                        </div>
                    </div>
                    <div class="content">
                        <span class="time">ENGLISH STUDIES</span>
                        <h4 class="title">
                            <a href="/tefl">TEFL LEVEL 5</a>
                        </h4>
                    </div>
                </div>
                <div class="single-blog-grid-item-01">
                    <div class="thumb">
                        <img src="/assets/img/Cards/TESOLCERTIFICATE.png" alt="blog image" />
                        <div class="hover">
                            <a href="/tesol-certification" class="readmore"><i class="fas fa-link"></i></a>
                        </div>
                    </div>
                    <div class="content">
                        <span class="time">ENGLISH STUDIES</span>
                        <h4 class="title">
                            <a href="/tesol-certification">TESOL CERTIFICATION</a>
                        </h4>
                    </div>
                </div>
                <div class="single-blog-grid-item-01">
                    <div class="thumb">
                        <img src="/assets/img/Cards/TESOLMASTERS.png" alt="blog image" />
                        <div class="hover">
                            <a href="/tesol-masters" class="readmore"><i class="fas fa-link"></i></a>
                        </div>
                    </div>
                    <div class="content">
                        <span class="time">ENGLISH STUDIES</span>
                        <h4 class="title">
                            <a href="/tesol-masters">TESOL MASTERS</a>
                        </h4>
                    </div>
                </div>
                <div class="single-blog-grid-item-01">
                    <div class="thumb">
                        <img src="/assets/img/Cards/Montessori.png" alt="blog image" />
                        <div class="hover">
                            <a href="/montessori-teacher-training" class="readmore"><i class="fas fa-link"></i></a>
                        </div>
                    </div>
                    <div class="content">
                        <span class="time">Career Studies</span>
                        <h4 class="title">
                            <a href="/montessori-teacher-training">Montessori Teacher Training</a>
                        </h4>
                    </div>
                </div>
                <div class="single-blog-grid-item-01">
                    <div class="thumb">
                        <img src="/assets/img/Cards/PrePrimary.png" alt="blog image" />
                        <div class="hover">
                            <a href="/pre-primary-teacher-training" class="readmore"><i class="fas fa-link"></i></a>
                        </div>
                    </div>
                    <div class="content">
                        <span class="time">Career Studies</span>
                        <h4 class="title">
                            <a href="/pre-primary-teacher-training">Pre-primary Teacher Training</a>
                        </h4>
                    </div>
                </div>
                <div class="single-blog-grid-item-01">
                    <div class="thumb">
                        <img src="/assets/img/Cards/early.png" alt="blog image" />
                        <div class="hover">
                            <a href="/early-childhood-education-and-care" class="readmore"><i class="fas fa-link"></i></a>
                        </div>
                    </div>
                    <div class="content">
                        <span class="time">Career Studies</span>
                        <h4 class="title">
                            <a href="/early-childhood-education-and-care">Early Childhood Education and Care</a>
                        </h4>
                    </div>
                </div>
                <div class="single-blog-grid-item-01">
                    <div class="thumb">
                        <img src="/assets/img/Cards/Nursery.png" alt="blog image" />
                        <div class="hover">
                            <a href="/nursery-teacher-training" class="readmore"><i class="fas fa-link"></i></a>
                        </div>
                    </div>
                    <div class="content">
                        <span class="time">Career Studies</span>
                        <h4 class="title">
                            <a href="/nursery-teacher-training">Nursery Teacher Training</a>
                        </h4>
                    </div>
                </div>
                <div class="single-blog-grid-item-01">
                    <div class="thumb">
                        <img src="/assets/img/Cards/school.png" alt="blog image" />
                        <div class="hover">
                            <a href="/education-management" class="readmore"><i class="fas fa-link"></i></a>
                        </div>
                    </div>
                    <div class="content">
                        <span class="time">Career Studies</span>
                        <h4 class="title">
                            <a href="/education-management">Education Management</a>
                        </h4>
                    </div>
                </div>
                <div class="single-blog-grid-item-01">
                    <div class="thumb">
                        <img src="/assets/img/Cards/childdevelopmentassociate.png" alt="blog image" />
                        <div class="hover">
                            <a href="/nursery-teacher-training" class="readmore"><i class="fas fa-link"></i></a>
                        </div>
                    </div>
                    <div class="content">
                        <span class="time">Career Studies</span>
                        <h4 class="title">
                            <a href="/child-development-associate">Child Development Associate</a>
                        </h4>
                    </div>
                </div>
                <!-- <div class="single-blog-grid-item-01">
                    <div class="thumb">
                        <img src="assets/img/University/students/backpacks-college-students.png" alt="blog image" />
                        <div class="hover">
                            <a href="/ofqual-level-3-diploma-in-business-administration" class="readmore"><i class="fas fa-link"></i></a>
                        </div>
                    </div>
                    <div class="content">
                        <span class="time">BUSINESS STUDIES</span>
                        <h4 class="title"><a href="/ofqual-level-3-diploma-in-business-administration">TEFL LEVEL 3/TESOL</a></h4>
                    </div>
                </div>
                <div class="single-blog-grid-item-01">
                    <div class="thumb">
                        <img src="assets/img/University/students/backpacks-college-students.png" alt="blog image" />
                        <div class="hover">
                            <a href="/ofqual-level-5-diploma-in-management-and-leadership" class="readmore"><i class="fas fa-link"></i></a>
                        </div>
                    </div>
                    <div class="content">
                        <span class="time">BUSINESS STUDIES</span>
                        <h4 class="title"><a href="/ofqual-level-5-diploma-in-management-and-leadership">TEFL LEVEL 5/TESOL</a></h4>
                    </div>
                </div><div class="single-blog-grid-item-01">
                    <div class="thumb">
                        <img src="assets/img/University/students/backpacks-college-students.png" alt="blog image" />
                        <div class="hover">
                            <a href="/diploma-in-business-administration" class="readmore"><i class="fas fa-link"></i></a>
                        </div>
                    </div>
                    <div class="content">
                        <span class="time">BUSINESS STUDIES</span>
                        <h4 class="title"><a href="/diploma-in-business-administration">Diploma in Business Administration</a></h4>
                    </div>
                </div><div class="single-blog-grid-item-01">
                    <div class="thumb">
                        <img src="assets/img/University/students/backpacks-college-students.png" alt="blog image" />
                        <div class="hover">
                            <a href="/tourism-management" class="readmore"><i class="fas fa-link"></i></a>
                        </div>
                    </div>
                    <div class="content">
                        <span class="time">BUSINESS STUDIES</span>
                        <h4 class="title"><a href="/tourism-management">Tourism Management</a></h4>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
</section>


<!-- testimonial carousel area start -->
<section class="testimonial-carousel-area padding-45">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="testimonial-style-1 white-bg">
                    <div class="section-title with-icon">
                        <h2 class="title">What Our Students Says</h2>
                    </div>
                    <div class="testimonial-slider-arrows"></div>
                    <div class="testimonial-slider-dots hide-md-and-down"></div>
                    <div class="row justify-content-center">
                        <div class="col-lg-6 text-center">
                            <div class="testimonial-thumbnail-slider">
                                <div class="single-testimonial-thumb">
                                    <div class="img-wrap">
                                        <div class="img" style="background-image: url(/assets/img/testimonials/test1.jpeg);"></div>
                                    </div>
                                </div>
                                <div class="single-testimonial-thumb">
                                    <div class="img-wrap">
                                        <div class="img" style="background-image: url(/assets/img/testimonials/test2.jpeg);"></div>
                                    </div>
                                </div>
                                <div class="single-testimonial-thumb">
                                    <div class="img-wrap">
                                        <div class="img" style="background-image: url(/assets/img/testimonials/test3.jpeg);"></div>
                                    </div>
                                </div>
                                <div class="single-testimonial-thumb">
                                    <div class="img-wrap">
                                        <div class="img" style="background-image: url(/assets/img/testimonials/test4.jpeg);"></div>
                                    </div>
                                </div>
                                <div class="single-testimonial-thumb">
                                    <div class="img-wrap">
                                        <div class="img" style="background-image: url(/assets/img/testimonials/test5.jpeg);"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="testimonial-description-slider">
                            <div class="single-testimonial-item">
                                <p>1 Been trained as an early childhood educator at Atheneum Global. The global level 5 qualification program of Atheneum Global is very well organized and effectively delivered. What I loved most was the individual attention given to me. My tutor worked along with me to ensure assignments and quizzes became less of an ordeal.</p>
                                <div class="author-meta">
                                    <!-- <h4 class="title"><strong>Sophia Kibler</strong><span class="divider"> | </span><span class="designation">BA Hons Project
                                            Management</span></h4> -->
                                            <h4 class="title"><strong>Sophia Kibler</strong></h4>
                                </div>
                            </div>
                            <div class="single-testimonial-item">
                                <p>2 Fantastic experience in graduating as a Montessori educator from Atheneum Global. Highly qualified assessors and quality assurers made my course work well directed and my in-class teaching experience enormously rewarding. Highly recommend to students in the UK.</p>
                                <div class="author-meta">
                                    <!-- <h4 class="title"><strong>Olivia Perry</strong><span class="divider"> | </span><span class="designation">Computer Science</span></h4> -->
                                    <h4 class="title"><strong>Olivia Perry</strong></h4>

                                </div>
                            </div>
                            <div class="single-testimonial-item">
                                <p>3 Thanks to Atheneum Global, I’m now a confident tutor running my live-online classes for non-native English speakers. Highly recommend the TESOL Masters course. The global qualification with easily verifiable credentials helped me acquire learners easily.</p>
                                <div class="author-meta">
                                    <!-- <h4 class="title"><strong>Zoya Augustin</strong><span class="divider"> | </span><span class="designation">BA Hons English</span></h4> -->
                                    <h4 class="title"><strong>Zoya Augustin</strong></h4>

                                </div>
                            </div>
                            <div class="single-testimonial-item">
                                <p>4 I’m a Level 5 Global Qualification TEFL Graduate from Atheneum Global. At par with CELTA, this course has helped me tremendously to mature as an adult educator with a practical understanding of how to conduct effective classes for non-native English learners.</p>
                                <div class="author-meta">
                                    <!-- <h4 class="title"><strong>Sienna Miller</strong><span class="divider"> | </span><span class="designation">BA Hons Project
                                            Management</span></h4> -->
                                            <h4 class="title"><strong>Sienna Miller</strong></h4>
                                </div>
                            </div>
                            <div class="single-testimonial-item">
                                <p>5 The certificate course in Montessori Education was exactly what I needed as an entry-level course as part of my CPD plans as a qualified educator. I wish to now pursue the global level 5 qualification in Montessori Education. Highly recommend Atheneum Global!</p>
                                <div class="author-meta">
                                    <!-- <h4 class="title"><strong>Romi Smith</strong><span class="divider"> | </span><span class="designation">BA Hons Project
                                            Management</span></h4> -->
                                            <h4 class="title"><strong>Romi Smith</strong></h4>
                                </div>
                            </div>
                            <!-- <div class="single-testimonial-item">
                                <p>6 Lorem ipsum dolor sit amet, consectetur adipiscing elit. In rhoncus augue nibh, at
                                    ullamcorper orci ullamcorper ut. Integer vehicula iaculis risus, non consequat eros
                                    tincidunt ac. Morbi a aliquam tortor. Nam convallis vestibulum nisi, sit amet
                                    fermentum libero scelerisque id. Integer iaculis mollis justo, sed interdum ligula
                                    auctor in.</p>
                                <div class="author-meta">
                                    <h4 class="title"><strong>Michel Hamson </strong><span class="divider"> | </span><span class="designation">Masters in
                                            Economics</span></h4>
                                </div>
                            </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- accreditations Section -->
<section class="features-section section" id="homepage-accreditations">
    <div class="container">

        <div class="row d-flex justify-content-between">
            <div class="col-12 col-lg-7 content">
                <h2>Accreditations & Partnerships</h2>
                <ul class="list">
                    <li class="check-arrow">OFQUAL REGULATED COURSES</li>
                    <li class="check-arrow">Focus Awards</li>
                    <li class="check-arrow">UK REGISTER OF LEARNING PROVIDERS</li>
                    <li class="check-arrow">Westminster College UK</li>
                    <li class="check-arrow">International Montessori Council</li>
                    <li class="check-arrow">International Montessori Society</li>
                    <li class="check-arrow">IATEFL - International Association of Teachers of English as a Foreign Language</li>
                </ul>
                <!-- <div class="large-btn" onclick="window.location.href = '/accreditations';">Know More</div> -->
            </div>
            <div class="col-12 col-lg-5 d-flex flex-column justify-content-center align-items-center">
                <!-- <img src="assets/img/web/accreditations/all-accreditations.png" alt="Accreditations" class="img-fluid" /> -->
                <div class="owl-carousel-0 owl-theme">
                  <div class="item">
                     <img src="/assets/img/study/Chartered-College-Of-Teachers.png" class="img-fluid" alt="chartered college of teachers">
                  </div>
                  <div class="item">
                    <img src="/assets/img/study/ISO-Certificate.png" class="img-fluid" alt="ISO Certificate">
                  </div>
                  <!-- <div class="item">
                    <img src="/assets/img/study/London-Teacher-Training-College.png" class="img-fluid" alt="London Teacher Training College">
                  </div> -->
                  <div class="item">
                    <img src="/assets/img/study/IMC.png" class="img-fluid" alt="IMC">
                  </div>
                  <div class="item">
                    <img src="/assets/img/study/IMS.png" class="img-fluid" alt="IMS">
                  </div>
                </div>
            </div>
        </div>
    </div>
</section>



<!-- Achievements section -->
<div class="achievements-style-2 section">
    <div class="container">
        <div id="counters" class="row text-center">
            <div class="col-12 col-md-4">
                <h1><span class="students-counter count-number">2,322</span></h1>
                <h4>Hours of Lessons</h4>
            </div>
            <div class="col-12 col-md-4">
                <h1><span class="courses-counter count-number">1500</span><span>+</span></h1>
                <h4>Students</h4>
            </div>
            <div class="col-12 col-md-4">
                <h1><span class="reviews-counter count-number">30</span></h1>
                <h4>Teachers</h4>
            </div>
        </div>
    </div>
    <div class="overaly"></div>
</div>
