<ul>
                        <li class="d-block align-items-center search-input-group">
                          <div class="search-container mt-3">
                          <form action="/action_page.php">
                           <input type="text" placeholder="Search.." name="search">
                           <button type="submit"><i class="flaticon-search"></i></button>
                          </form>
                        </div>
                        </li>
                    </ul>
                    <h5>Other Courses</h5>
                    <ul class="list">
                        <li class="sub-li">
                            <a href="/montessori-teacher-training" class="point-right-arrow">Montessori Teacher Training</a>
                        </li>
                        <li class="sub-li">
                            <a href="/pre-primary-teacher-training" class="point-right-arrow">Pre & Primary Teacher Training</a>
                        </li>
                        <li class="sub-li">
                            <a href="/early-childhood-education-and-care" class="point-right-arrow">Early Childhood Care and Education</a>
                        </li>
                        <li class="sub-li">
                            <a href="/nursery-teacher-training" class="point-right-arrow">Nursery Teacher Training</a>
                        </li>
                        <li class="sub-li">
                            <a href="/education-management" class="point-right-arrow">School Organization : Administration & Management</a>
                        </li>
                        <li class="sub-li">
                            <a href="/child-development-associate" class="point-right-arrow">Child Development Associate</a>
                        </li>
                    </ul>
<div>
    <div class="large-btn" onclick="window.location.href = '/apply-now';">Apply Now</div>
</div>
<h5>Testimonials</h5>
<div class="row comment-item-style-2" id="sidebar-testimonials">
    <div class="col-3 profile-img">
        <img src="/assets/img/profiles/student_4.png" alt="profile img" class="img-fluid" />
    </div>
    <div class="col-9 comment-content">
        <p class="name">Martin Backham</p>
        <p class="time">Teacher</p>
    </div>
    <p class="post">I am proud to pass out from this global institute. The training material was easy to follow and very relevant. The overall course structure was very good.</p>
</div>
<div class="row comment-item-style-2" id="sidebar-testimonials">
    <div class="col-3 profile-img">
        <img src="/assets/img/profiles/student_5.png" alt="profile img" class="img-fluid" />
    </div>
    <div class="col-9 comment-content">
        <p class="name">Emrata Rosie</p>
        <p class="time">Teacher</p>
    </div>
    <p class="post">I rate the training program as perfect. Thanks for the systematic approach undertaken for the program delivery.</p>
</div>
